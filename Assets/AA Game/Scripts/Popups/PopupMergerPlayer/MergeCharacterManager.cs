using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Purchasing;

[CreateAssetMenu(fileName = "MergeCharacterManager", menuName = "Toan/Merge Character Manager")]
public class MergeCharacterManager : ScriptableObject
{
    private static MergeCharacterManager instance;
    public static MergeCharacterManager Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<MergeCharacterManager>("Scriptable/MergeCharacterManager");
            return instance;
        }
    }
    [SerializeField] private DataPlayerMerger dataHero;
    [SerializeField] private List<CharacterMergeOder> mergerOderListHero;
    [SerializeField] private DataPlayerMerger dataPet;
    [SerializeField] private List<CharacterMergeOder> mergerOderListPet;
    [SerializeField] private List<CharacterMergeOder> mergerOderListPet2;
    [SerializeField] private List<CharacterMergeOder> mergerOderListPet3;
    [SerializeField] private bool loadData;
    private void OnValidate()
    {
        if (loadData)
        {
            for (int i = 0; i < mergerOderListHero.Count; i++)
            {
                mergerOderListHero[i].mergeOrder = i;
            }
            for (int i = 0; i < mergerOderListPet.Count; i++)
            {
                mergerOderListPet[i].mergeOrder = i;
            }
            loadData = false;
        }
    }
    public ItemID GetIconHero()
    {
        return mergerOderListHero[0].characterID;
    }
    public ItemID GetIconPet(TypeBot typeBot)
    {
        ItemID idBot = ItemID.None;
        if(typeBot == TypeBot.Pistol)
        {
            idBot = mergerOderListPet[0].characterID;
        }
        else if (typeBot == TypeBot.Riffle)
        {
            idBot = mergerOderListPet2[0].characterID;
        }
        else if (typeBot == TypeBot.Bazoka)
        {
            idBot = mergerOderListPet3[0].characterID;
        }

        return idBot;
    }
    public CharacterMergeOder GetCharacter(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            return mergerOderListHero.Find(x => x.characterID == _idHero);
        }
        if (IsTypePet(_idHero))
        {
            return mergerOderListPet.Find(x => x.characterID == _idHero);
        }
        if (IsTypePet2(_idHero))
        {
            return mergerOderListPet2.Find(x => x.characterID == _idHero);
        }
        if (IsTypePet3(_idHero))
        {
            return mergerOderListPet3.Find(x => x.characterID == _idHero);
        }
        return null;
    }
    public bool IsHero(ItemID _id)
    {
        return mergerOderListHero.Contains(x => x.characterID == _id);
    }
    public bool IsTypePet(ItemID _id)
    {
        return mergerOderListPet.Contains(x => x.characterID == _id);
    }

    public bool IsTypePet2(ItemID _id)
    {
        return mergerOderListPet2.Contains(x => x.characterID == _id);
    }

    public bool IsTypePet3(ItemID _id)
    {
        return mergerOderListPet3.Contains(x => x.characterID == _id);
    }

    public bool CanMerge(ItemID _id)
    {
        if (IsHero(_id))
        {
            return mergerOderListHero.Last().characterID != _id;
        }
        if (IsTypePet(_id))
        {
            return mergerOderListPet.Last().characterID != _id;
        }
        if (IsTypePet2(_id))
        {
            return mergerOderListPet2.Last().characterID != _id;
        }
        if (IsTypePet3(_id))
        {
            return mergerOderListPet3.Last().characterID != _id;
        }
        return false;
    }
    public ItemID NextMerge(ItemID _id)
    {
        if (CanMerge(_id))
        {
            if (IsHero(_id))
            {
                CharacterMergeOder chaCurrent = mergerOderListHero.FirstOrDefault(x => x.characterID == _id);
                int indexNext = mergerOderListHero.IndexOf(chaCurrent) + 1;
                return mergerOderListHero[indexNext].characterID;
            }
            if (IsTypePet(_id))
            {
                CharacterMergeOder chaCurrent = mergerOderListPet.FirstOrDefault(x => x.characterID == _id);
                int indexNext = mergerOderListPet.IndexOf(chaCurrent) + 1;
                return mergerOderListPet[indexNext].characterID;
            }
            if (IsTypePet2(_id))
            {
                CharacterMergeOder chaCurrent = mergerOderListPet2.FirstOrDefault(x => x.characterID == _id);
                int indexNext = mergerOderListPet2.IndexOf(chaCurrent) + 1;
                return mergerOderListPet2[indexNext].characterID;
            }
            if (IsTypePet3(_id))
            {
                CharacterMergeOder chaCurrent = mergerOderListPet3.FirstOrDefault(x => x.characterID == _id);
                int indexNext = mergerOderListPet3.IndexOf(chaCurrent) + 1;
                return mergerOderListPet3[indexNext].characterID;
            }
        }
        return ItemID.None;
    }
    public CharacterMergeOder GetHeroHigh()
    {
        CharacterMergeOder temp = mergerOderListHero.LastOrDefault(x => User.Instance[x.characterID] >= 1);
        if (temp == null)
        {
            return mergerOderListHero[0];
        }
        else
        {
            return temp;
        }
    }
    public CharacterMergeOder GetPetHigh()
    {
        CharacterMergeOder temp = mergerOderListPet.LastOrDefault(x => User.Instance[x.characterID] >= 1);
        if (temp == null)
        {
            return mergerOderListPet[0];
        }
        else
        {
            return temp;
        }
    }
    public static ItemID GetNameWeapon(ItemID _idSkin)
    {
        ItemID idWeapon = ItemID.Rifle_1;
        int indexOder = Instance.GetCharacter(_idSkin).mergeOrder;
        if (Instance.IsHero(_idSkin))
        {
            //int value = Mathf.Clamp(indexOder, 0, WeaponFuseManager.Instance.listFuseRifle.Count - 1);
            //idWeapon = WeaponFuseManager.Instance.listFuseRifle[value].WeaponId;
        }
        else if (Instance.IsTypePet(_idSkin))
        {
            //int value = Mathf.Clamp(indexOder, 0, WeaponFuseManager.Instance.listFuseRocket.Count - 1);
            //idWeapon = WeaponFuseManager.Instance.listFuseRocket[value].WeaponId;
        }

        return idWeapon;
    }

    public int GetDamager(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            int order = GetHeroHigh().mergeOrder;
            return dataHero.GetDamager(order);
        }
        else if (IsTypePet(_idHero))
        {
            int order = GetPetHigh().mergeOrder;
            return dataPet.GetDamager(order);
        }
        return 100;
    }

    public int GetHP(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            int order = GetHeroHigh().mergeOrder;
            return dataHero.GetHP(order);
        }
        else if (IsTypePet(_idHero))
        {
            int order = GetPetHigh().mergeOrder;
            return dataPet.GetHP(order);
        }
        return 100;
    }
     public ItemID GetRandomHeroOrPet()
    {
        int random = Random.Range(0, 100);
        if(random %2 == 0)
        {
            return mergerOderListHero[0].characterID;
        }
        else
        {
            return mergerOderListPet[0].characterID;
        }
    }
}
[System.Serializable]
public class CharacterMergeOder
{
    public ItemID characterID;
    public int mergeOrder;
}
[System.Serializable]
public class DataPlayerMerger
{
    [SerializeField] private int baseHP;
    [SerializeField] private int baseDamager;
    public int GetDamager(int oder)
    {
        return (int)Mathf.Round(baseDamager * Mathf.Pow(2, oder));
    }
    public int GetHP(int oder)
    {
        return (int)Mathf.Round(baseHP * Mathf.Pow(2, oder)); ;
    }
}
