using AA_Game;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class ItemCharacterMerger : Item
{
    [SerializeField] private SkeletonGraphic skeGraphic;
    public SkeletonDataAsset[] skeletonDataAsset;
    [SerializeField] private Image iconHero;
    private ItemID idHero;
    public bool isCarMerge = false;

    public void Setup(ItemID _idHero)
    {
        idHero = _idHero;
        //iconHero.sprite = Resources.Load<Sprite>("IconBot/"+_idHero.ToString());
        //iconHero.preserveAspect = true;
        ChangeDataAsset();
        //skeGraphic.Initialize(false);
        //var customSkin = new Skin("Skin");
        //List<string> skins = skeGraphic.SkeletonDataAsset.GetSkeletonData(true).Skins.Items.Select(x => x.ToString()).ToList();
        //string skinCurrent = _idHero.ToString();
        //string currentFind = null;
        //currentFind = skins.Find(x => x.Contains(skinCurrent));
        //if (currentFind == null)
        //{
        //    currentFind = skins.Find(x => x.Contains(_idHero.ToString()));
        //}

        //var charSkin = skeGraphic.Skeleton.Data.FindSkin(currentFind);
        //customSkin.AddSkin(charSkin);
        //var weaponSkin = skeGraphic.Skeleton.Data.FindSkin(skins.Find(x => x.Contains(MergeCharacterManager.GetNameWeapon(_idHero).ToString())));
        //customSkin.AddSkin(weaponSkin);
        //skeGraphic.Skeleton.SetSkin(customSkin);
        //skeGraphic.Skeleton.SetSlotsToSetupPose();
        //skeGraphic.AnimationState.SetAnimation(0, "idle", true);
    }

    public void ChangeDataAsset()
    {
        if (isCarMerge)
        {
            if (ItemType.Cars.Contains(idHero))
            {
                skeGraphic.skeletonDataAsset = skeletonDataAsset[0];
            }
            skeGraphic.Skeleton.SetSkin(idHero.ToString()+"/phase_1");
        }
        else
        {
            if (ItemType.SkinsMain.Contains(idHero))
            {
                skeGraphic.skeletonDataAsset = skeletonDataAsset[0];
            }
            if (ItemType.SkinsBot1.Contains(idHero))
            {
                skeGraphic.skeletonDataAsset = skeletonDataAsset[1];
            }
            if (ItemType.SkinsBot2.Contains(idHero))
            {
                skeGraphic.skeletonDataAsset = skeletonDataAsset[2];
            }
            if (ItemType.SkinsBot3.Contains(idHero))
            {
                skeGraphic.skeletonDataAsset = skeletonDataAsset[3];
            }
            skeGraphic.Initialize(true);
            skeGraphic.Skeleton.SetSkin(idHero.ToString());
        }
    }
}
