using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "MergeCarManager", menuName = "Toan/Merge Car Manager")]
public class MergeCarManager : ScriptableObject
{
    private static MergeCarManager instance;
    public static MergeCarManager Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<MergeCarManager>("Scriptable/MergeCarManager");
            return instance;
        }
    }
    [SerializeField] private DataPlayerMerger dataHero;
    [SerializeField] private List<CharacterMergeOder> mergerOderListHero;
    [SerializeField] private bool loadData;
    private void OnValidate()
    {
        if (loadData)
        {
            for (int i = 0; i < mergerOderListHero.Count; i++)
            {
                mergerOderListHero[i].mergeOrder = i;
            }
            loadData = false;
        }
    }
    public ItemID GetIconHero()
    {
        return mergerOderListHero[0].characterID;
    }
    public ItemID GetIconPet(TypeBot typeBot)
    {
        ItemID idBot = ItemID.None;
        return idBot;
    }
    public CharacterMergeOder GetCharacter(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            return mergerOderListHero.Find(x => x.characterID == _idHero);
        }
        return null;
    }
    public bool IsHero(ItemID _id)
    {
        return mergerOderListHero.Contains(x => x.characterID == _id);
    }

    public bool CanMerge(ItemID _id)
    {
        if (IsHero(_id))
        {
            return mergerOderListHero.Last().characterID != _id;
        }
        return false;
    }
    public ItemID NextMerge(ItemID _id)
    {
        if (CanMerge(_id))
        {
            if (IsHero(_id))
            {
                CharacterMergeOder chaCurrent = mergerOderListHero.FirstOrDefault(x => x.characterID == _id);
                int indexNext = mergerOderListHero.IndexOf(chaCurrent) + 1;
                return mergerOderListHero[indexNext].characterID;
            }
        }
        return ItemID.None;
    }
    public CharacterMergeOder GetHeroHigh()
    {
        CharacterMergeOder temp = mergerOderListHero.LastOrDefault(x => User.Instance[x.characterID] >= 1);
        if (temp == null)
        {
            return mergerOderListHero[0];
        }
        else
        {
            return temp;
        }
    }
    //public CharacterMergeOder GetPetHigh()
    //{
    //    CharacterMergeOder temp = mergerOderListPet.LastOrDefault(x => User.Instance[x.characterID] >= 1);
    //    if (temp == null)
    //    {
    //        return mergerOderListPet[0];
    //    }
    //    else
    //    {
    //        return temp;
    //    }
    //}
    public static ItemID GetNameWeapon(ItemID _idSkin)
    {
        ItemID idWeapon = ItemID.Rifle_1;
        int indexOder = Instance.GetCharacter(_idSkin).mergeOrder;
        if (Instance.IsHero(_idSkin))
        {
            //int value = Mathf.Clamp(indexOder, 0, WeaponFuseManager.Instance.listFuseRifle.Count - 1);
            //idWeapon = WeaponFuseManager.Instance.listFuseRifle[value].WeaponId;
        }
        //else if (Instance.IsTypePet(_idSkin))
        //{
        //    //int value = Mathf.Clamp(indexOder, 0, WeaponFuseManager.Instance.listFuseRocket.Count - 1);
        //    //idWeapon = WeaponFuseManager.Instance.listFuseRocket[value].WeaponId;
        //}

        return idWeapon;
    }

    public int GetDamager(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            int order = GetHeroHigh().mergeOrder;
            return dataHero.GetDamager(order);
        }
        //else if (IsTypePet(_idHero))
        //{
        //    int order = GetPetHigh().mergeOrder;
        //    return dataPet.GetDamager(order);
        //}
        return 100;
    }

    public int GetHP(ItemID _idHero)
    {
        if (IsHero(_idHero))
        {
            int order = GetHeroHigh().mergeOrder;
            return dataHero.GetHP(order);
        }
        //else if (IsTypePet(_idHero))
        //{
        //    int order = GetPetHigh().mergeOrder;
        //    return dataPet.GetHP(order);
        //}
        return 100;
    }
    // public ItemID GetRandomHeroOrPet()
    //{
    //    int random = Random.Range(0, 100);
    //    if(random %2 == 0)
    //    {
    //        return mergerOderListHero[0].characterID;
    //    }
    //    else
    //    {
    //        return mergerOderListPet[0].characterID;
    //    }
    //}
}
//[System.Serializable]
//public class CharacterMergeOder
//{
//    public ItemID characterID;
//    public int mergeOrder;
//}
//[System.Serializable]
//public class DataPlayerMerger
//{
//    [SerializeField] private int baseHP;
//    [SerializeField] private int baseDamager;
//    public int GetDamager(int oder)
//    {
//        return (int)Mathf.Round(baseDamager * Mathf.Pow(2, oder));
//    }
//    public int GetHP(int oder)
//    {
//        return (int)Mathf.Round(baseHP * Mathf.Pow(2, oder)); ;
//    }
//}
