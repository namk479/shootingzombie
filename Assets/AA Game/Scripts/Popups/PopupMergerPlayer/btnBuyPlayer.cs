using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class btnBuyPlayer : MonoBehaviour
{
    private ItemID idHero;
    private Action<ItemID> actionClick;
    private bool isBuyADS;
    private bool isBuyFree;
    private int amountHero;
    private int priceHero;
    public int indexChar;
    public bool isCarMerge = false;

    [SerializeField] private Button btnClick;
    [SerializeField] private Image iconHero;
    [SerializeField] private TextMeshProUGUI txtPrice;
    [SerializeField] private Image iconType;
    [SerializeField] private TextMeshProUGUI txtCountHero;

    public void Setup(ItemID _idHero, Action<ItemID> _action)
    {
        idHero = _idHero;
        actionClick = _action;
        if (isCarMerge)
        {

        }
        else
        {
            iconHero.sprite = Resources.Load<Sprite>("IconBot/NewIcon/" + idHero.ToString()); //SpriteManager.Instance.GetSprite(idHero);
        }
        OnRefreshBtn();
        GameEvent.OnRefreshBtnPrice.RemoveListener(OnRefreshBtn);
        GameEvent.OnRefreshBtnPrice.AddListener(OnRefreshBtn);
    }
    private void OnEnable()
    {
        btnClick.onClick.RemoveAllListeners();
        btnClick.onClick.AddListener(OnClick);
        
    }

    public void OnClick()
    {
        if(indexChar == 0 || indexChar == 1)
        {
            Buy();
        }
        if(indexChar == 2)
        {
            if(User.Instance.Car.slot >= 2)
            {
                Buy();
            }
        }
        if (indexChar == 3)
        {
            if (User.Instance.Car.slot >= 3)
            {
                Buy();
            }
        }
    }


    public void Buy()
    {
        if (isBuyADS)
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 0) }, null, (success) =>
            {
                if (success)
                {
                    for (int i = 0; i < amountHero; i++)
                    {
                        actionClick?.Invoke(idHero);
                    }
                }
            }, AdLocation.BuyHero);
        }
        else
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Gold, priceHero) }, null, (success) =>
            {
                if (success)
                {
                    if (isBuyFree)
                    {
                        User.Instance.RemoveListMergeDu(idHero);
                    }
                    else
                    {
                        if (isCarMerge)
                        {
                            User.Instance[ItemID.PriceCar] += 500;
                        }
                        else
                        {
                            if (MergeCharacterManager.Instance.IsHero(idHero))
                            {
                                User.Instance[ItemID.PriceHero1] += 500;
                            }
                            else if (MergeCharacterManager.Instance.IsTypePet(idHero))
                            {
                                User.Instance[ItemID.PriceHero2] += 500;
                            }
                            else if (MergeCharacterManager.Instance.IsTypePet2(idHero))
                            {
                                User.Instance[ItemID.PriceHero3] += 500;
                            }
                            else if (MergeCharacterManager.Instance.IsTypePet3(idHero))
                            {
                                User.Instance[ItemID.PriceHero4] += 500;
                            }
                        }
                        
                    }
                    actionClick?.Invoke(idHero);
                }
            }, AdLocation.BuyHero);
        }
    }


    private void OnRefreshBtn()
    {
        isBuyFree = false;
        isBuyADS = false;

        if (isCarMerge)
        {
            priceHero = User.Instance[ItemID.PriceCar];
            if (User.Instance.GetListCarMergeDu(idHero))
            {
                priceHero = 0;
                isBuyFree = true;
            }
        }
        else
        {
            if (MergeCharacterManager.Instance.IsHero(idHero))
            {
                priceHero = User.Instance[ItemID.PriceHero1];
            }
            else if (MergeCharacterManager.Instance.IsTypePet(idHero))
            {
                priceHero = User.Instance[ItemID.PriceHero2];
            }
            else if (MergeCharacterManager.Instance.IsTypePet2(idHero))
            {
                priceHero = User.Instance[ItemID.PriceHero3];
            }
            else if (MergeCharacterManager.Instance.IsTypePet3(idHero))
            {
                priceHero = User.Instance[ItemID.PriceHero4];
            }
            if (User.Instance.GetListMergeDu(idHero))
            {
                priceHero = 0;
                isBuyFree = true;
            }
        }
        
        if (isBuyFree == false)
        {
            isBuyADS = User.Instance[ItemID.Gold] < priceHero;
        }
        if (isBuyADS)
        {
            iconType.sprite = SpriteManager.Instance.GetSprite(ItemID.ads_Icon);
            txtPrice.text = $"Free";
            amountHero = 2;
        }
        else
        {
            iconType.sprite = SpriteManager.Instance.GetSprite(ItemID.Gold);
            txtPrice.text = priceHero.ToKMB();
            amountHero = 1;
        }
        txtCountHero.text = $"+{amountHero}";
        //iconType.SetNativeSize();
    }
}
