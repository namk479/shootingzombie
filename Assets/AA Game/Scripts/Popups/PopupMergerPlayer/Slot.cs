using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Yurowm.GameCore;

public class Slot : MonoBehaviour
{
    private ItemID idHero;
    private SlotState state;
    private bool canMerger;
    [SerializeField] private GameObject gLock;
    [SerializeField] private Image gIconLock;
    [SerializeField] private GameObject highlight;
    [SerializeField] private GameObject highlightMerge;
    [SerializeField] private Sprite sAds, sLock;
    [SerializeField] private Image imgBG;
    [SerializeField] private Sprite sNon, sType1, sType2,sType3,sType4;
    public ItemCharacterMerger characterMerger;
    private bool animMergePlaying;
    private Transform layerTop;

    public bool isMergeCar = false;
    public ItemID IDHero
    {
        get { return idHero; }
        set
        {
            idHero = value;
        }
    }
    public SlotState State
    {
        get => state;
        set
        {
            state = value;
            switch (state)
            {
                case SlotState.Non:
                    idHero = ItemID.None;
                    imgBG.sprite = sNon;
                    gLock.SetActive(false);
                    break;
                case SlotState.HasCharacter:
                    if (isMergeCar)
                    {
                        characterMerger = ContentPoolable.Emit(ItemID.CarMergeUI) as ItemCharacterMerger;
                    }
                    else
                    {
                        characterMerger = ContentPoolable.Emit(ItemID.CharacterMergeUI) as ItemCharacterMerger;
                    }
                    characterMerger.transform.SetParent(transform);
                    characterMerger.transform.localPosition = Vector3.zero;
                    if (isMergeCar)
                    {
                        characterMerger.transform.localScale = Vector3.one * 0.2f;
                    }
                    else
                    {
                        characterMerger.transform.localScale = Vector3.one * 0.6f;
                    }
                    characterMerger.Setup(idHero);
                    if (MergeCharacterManager.Instance.IsHero(idHero))
                    {
                        imgBG.sprite = sType1;
                    }
                    else if (MergeCharacterManager.Instance.IsTypePet(idHero))
                    {
                        imgBG.sprite = sType2;
                    }
                    else if (MergeCharacterManager.Instance.IsTypePet2(idHero))
                    {
                        imgBG.sprite = sType3;
                    }
                    else if (MergeCharacterManager.Instance.IsTypePet3(idHero))
                    {
                        imgBG.sprite = sType4;
                    }
                    break;
                case SlotState.LockADS:
                    gLock.SetActive(true);
                    gIconLock.sprite = sAds;
                    break;
                case SlotState.Lock:
                    gLock.SetActive(true);
                    gIconLock.sprite = sLock;
                    break;
            }
            //gIconLock.SetNativeSize();
        }
    }

    public ItemID IdHero => idHero;
    public void SetupSlot(ItemID _idHero, byte _index, Transform _layerTop)
    {
        layerTop = _layerTop;
        idHero = _idHero;
        gLock.SetActive(false);
        highlight.SetActive(false);
        highlightMerge.SetActive(false);
        if (User.Instance[ItemID.TotalSlot] < 5)
        {
            User.Instance[ItemID.TotalSlot] = 5;
        }
        if (characterMerger != null)
        {
            characterMerger.Kill();
            characterMerger = null;
        }
        if (_index > User.Instance[ItemID.TotalSlot])
        {
            if (_index == User.Instance[ItemID.TotalSlot] + 1)
            {
                State = SlotState.LockADS;
            }
            else
            {
                State = SlotState.Lock;
            }
        }
        else
        {
            if (idHero != ItemID.None)
            {
                State = SlotState.HasCharacter;
            }
            else
            {
                State = SlotState.Non;
            }
        }
    }
    public void OnSelect()
    {
        characterMerger.transform.SetParent(layerTop);
        highlight.gameObject.SetActive(true);
    }
    public void OnSelectDone()
    {
        if (characterMerger != null)
        {
            characterMerger.transform.DOMove(transform.position, 0.2f).OnComplete(() =>
            {
                characterMerger.transform.SetParent(transform);
                highlight.SetActive(false);
            });
        }
        else
        {
            highlight.SetActive(false);
        }
    }
    public void KillCharacter()
    {
        characterMerger.Kill();
        characterMerger = null;
        idHero = ItemID.None;
        State = SlotState.Non;
        highlight.SetActive(false);
    }
    public void MergeSuccess()
    {
        if (isMergeCar)
        {
            User.Instance.MergerCarSuccess(idHero);
        }
        else
        {
            User.Instance.MergerSuccess(idHero);
        }
        AnimMerge(idHero);
        StartCoroutine(IE_MergeSuccess());
        if (isMergeCar)
        {
            idHero = MergeCarManager.Instance.NextMerge(IdHero);
        }
        else
        {
            idHero = MergeCharacterManager.Instance.NextMerge(IdHero);
        }
        characterMerger.gameObject.SetActive(false);
    }
    IEnumerator IE_MergeSuccess()
    {
        yield return new WaitUntil(() => animMergePlaying == false);
        item1Anim.Kill();
        item2Anim.Kill();
        item1Anim = null;
        item2Anim = null;

        if (isMergeCar)
        {
            characterMerger.transform.DOScale(0.2f, 0.1f).From(0.1f).SetEase(Ease.OutBack);
        }
        else
        {
            characterMerger.transform.DOScale(0.6f, 0.1f).From(0.1f).SetEase(Ease.OutBack);
        }
        
        if (User.Instance[idHero] <= 0)
        {
            PopupManager.Instance.OpenPopup<PopupUnlockSkin>(PopupID.PopupUnlockSkin, (pop) =>
            {
                pop.SetData(idHero,isMergeCar);
                GameEvent.OnCarLevelUp.Invoke();
            });
        }
        else
        {
            FxItem fxMergeSuccess = ContentPoolable.Emit(ItemID.fxMergeSuccess) as FxItem;
            fxMergeSuccess.transform.SetParent(transform);
            fxMergeSuccess.transform.localPosition = Vector3.zero;
            fxMergeSuccess.transform.localScale = new Vector3(60, 30, 1);



            //fxMergeSuccess.transform.SetAsFirstSibling();
        }
        characterMerger.Setup(idHero);

    }
    ItemCharacterMerger item1Anim;
    ItemCharacterMerger item2Anim;
    private void AnimMerge(ItemID _idHero)
    {
        animMergePlaying = true;
        if (isMergeCar)
        {
            item1Anim = ContentPoolable.Emit(ItemID.CarMergeUI) as ItemCharacterMerger;
        }
        else
        {
            item1Anim = ContentPoolable.Emit(ItemID.CharacterMergeUI) as ItemCharacterMerger;
        }
        item1Anim.Setup(_idHero);
        item1Anim.transform.SetParent(layerTop);
        item1Anim.transform.position = transform.position + Vector3.right * 0.8f;

        if (isMergeCar)
        {
            item1Anim.transform.localScale = Vector3.one * 0.2f;
        }
        else
        {
            item1Anim.transform.localScale = Vector3.one * 0.6f;
        }

        if (isMergeCar)
        {
            item2Anim = ContentPoolable.Emit(ItemID.CarMergeUI) as ItemCharacterMerger;
        }
        else
        {
            item2Anim = ContentPoolable.Emit(ItemID.CharacterMergeUI) as ItemCharacterMerger;
        }
        item2Anim.Setup(_idHero);
        item2Anim.transform.SetParent(layerTop);
        item2Anim.transform.position = transform.position + Vector3.left * 0.8f;
        if (isMergeCar)
        {
            item2Anim.transform.localScale = Vector3.one * 0.2f;
        }
        else
        {
            item2Anim.transform.localScale = Vector3.one * 0.6f;
        }

        item1Anim.transform.DOMove(transform.position, 0.2f).SetEase(Ease.InBack);
        item2Anim.transform.DOMove(transform.position, 0.2f).SetEase(Ease.InBack).OnComplete(() =>
        {
            animMergePlaying = false;
            characterMerger.gameObject.SetActive(true);
        });

    }
    public bool CanMerger
    {
        set
        {
            if (canMerger != value)
            {
                highlightMerge.SetActive(value);
                canMerger = value;
            }
        }
    }
}
public enum SlotState : byte
{
    Non,
    HasCharacter,
    LockADS,
    Lock,

}