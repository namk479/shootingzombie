using Spine.Unity;
using System.Collections.Generic;
using System.Linq;
using Thanh.Core;
using UnityEngine;
using UnityEngine.UI;
using Yurowm.GameCore;

public class PopupMerge : Popup
{
    private Camera camUI;
    private bool isClickMerge;
    private Slot prevSlot;
    private Slot hightLightSlot;
    public CarUI carUI;

    //Btn
    [SerializeField] private btnBuyPlayer btnBuyPlayer1;
    [SerializeField] private btnBuyPlayer btnBuyPlayer2;
    [SerializeField] private btnBuyPlayer btnBuyPlayer3;
    [SerializeField] private btnBuyPlayer btnBuyPlayer4;

    [SerializeField] private Button btnPlayGame;

    private Slot HightSlot
    {
        set
        {
            if (value != prevSlot)
            {
                if (hightLightSlot == null)
                {
                    hightLightSlot = value;
                }
                else
                {
                    if (hightLightSlot != value)
                    {
                        hightLightSlot.CanMerger = false;
                        hightLightSlot = value;
                    }
                }
            }
            if (hightLightSlot != null)
            {
                hightLightSlot.CanMerger = true;
            }
        }
        get { return hightLightSlot; }
    }
    private Vector3 point;
    [SerializeField] private List<Slot> slots;
    [SerializeField] private LayerMask layerSlot;
    [SerializeField] private Transform layerTop;
    [SerializeField] private SkeletonGraphic animCharacter;
    [SerializeField] private SkeletonGraphic animPet;

    private void OnValidate()
    {
        popupID = PopupID.PopupMergerCharacter;
        slots = new List<Slot>();
        slots = GetComponentsInChildren<Slot>().ToList();
    }
    public override void OnShow()
    {
        base.OnShow();

        camUI = GameScene.main.UICamera;
        isClickMerge = false;
        for (int i = 0; i < slots.Count; i++)
        {
            if (i < User.Instance.GetListSkinMerge().Count)
            {
                slots[i].SetupSlot(User.Instance.GetListSkinMerge()[i], (byte)i, layerTop);
            }
            else
            {
                slots[i].SetupSlot(ItemID.None, (byte)i, layerTop);
            }
        }
        
        ItemID idHero1 = MergeCharacterManager.Instance.GetIconHero();
        ItemID idHero2 = MergeCharacterManager.Instance.GetIconPet(TypeBot.Pistol);
        ItemID idHero3 = MergeCharacterManager.Instance.GetIconPet(TypeBot.Riffle);
        ItemID idHero4 = MergeCharacterManager.Instance.GetIconPet(TypeBot.Bazoka);
        btnBuyPlayer1.Setup(idHero1, OnClaimPlayer);
        btnBuyPlayer2.Setup(idHero2, OnClaimPlayer);
        btnBuyPlayer3.Setup(idHero3, OnClaimPlayer);
        btnBuyPlayer4.Setup(idHero4, OnClaimPlayer);
        Refresh();
        ShowHeroGraphic();
        //GameEvent.OnChangeUsingSkin.RemoveListener(ShowHeroGraphic);
        //GameEvent.OnChangeUsingSkin.AddListener(ShowHeroGraphic);

        ShowPetGraphic();
        //GameEvent.OnChangeUsingPet.RemoveListener(ShowPetGraphic);
        //GameEvent.OnChangeUsingPet.AddListener(ShowPetGraphic);

        btnPlayGame.onClick.RemoveAllListeners();
        btnPlayGame.onClick.AddListener(() =>
        {
            //Loader.Instance.LoadScene(SceneName.GameScene.ToString());
            AudioManager.instance.Play("BtnClick");
            PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode);
        });
        carUI.SpawnBot();

        GameEvent.OnMoveToPlay.RemoveListener(Close);
        GameEvent.OnMoveToPlay.AddListener(Close);
    }

    public override void OnShowDone()
    {
        base.OnShowDone();
        isClickMerge = true;
    }
    private void Update()
    {
        if (isClickMerge == false)
        {
            return;
        }
        if (PopupManager.Instance.showingPopup == PopupID.PopupUnlockSkin) { return; }
        point = camUI.ScreenToWorldPoint(Input.mousePosition);
        point.z = 0;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D targer = Physics2D.CircleCast(point, 0.2f, Vector2.zero, layerSlot);
            if (targer.collider != null)
            {
                Slot targetSlot = targer.collider.GetComponent<Slot>();
                if (targetSlot != null)
                {
                    switch (targetSlot.State)
                    {
                        case SlotState.HasCharacter:
                            prevSlot = targetSlot;
                            prevSlot.OnSelect();
                            break;
                        case SlotState.LockADS:
                            Debug.Log("Unock");


                            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, (isSuccess) =>
                            {
                                if (isSuccess)
                                {
                                    User.Instance[ItemID.TotalSlot]++;
                                    RefreshSlot(targetSlot, (byte)User.Instance[ItemID.TotalSlot]);
                                    Refresh();
                                }
                                else
                                {
                                    PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice,
                                        (pop) => { pop.SetData("RETRY", "Video Ads not available"); });
                                }
                            }, AdLocation.BuySlotMerge);
                            
                            break;
                    }
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (prevSlot != null)
            {
                prevSlot.characterMerger.transform.position = point;
                RaycastHit2D targer = Physics2D.CircleCast(point, 0.2f, Vector2.zero, layerSlot);
                if (targer.collider != null)
                {
                    Slot targetSlot = targer.collider.GetComponent<Slot>();
                    if (targetSlot != null)
                    {
                        if (targetSlot.IDHero == prevSlot.IDHero)
                        {
                            HightSlot = targetSlot;
                        }
                        else
                        {
                            if (hightLightSlot != null)
                            {
                                hightLightSlot.CanMerger = false;
                            }
                        }
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (prevSlot != null)
            {
                prevSlot.characterMerger.transform.position = point;
                RaycastHit2D targer = Physics2D.CircleCast(point, 0.2f, Vector2.zero, layerSlot);
                if (targer.collider != null)
                {
                    Slot targetSlot = targer.collider.GetComponent<Slot>();
                    if (targetSlot != null)
                    {
                        if (targetSlot != prevSlot)
                        {
                            switch (targetSlot.State)
                            {
                                case SlotState.Non:
                                    targetSlot.IDHero = prevSlot.IDHero;
                                    targetSlot.characterMerger = prevSlot.characterMerger;
                                    targetSlot.State = SlotState.HasCharacter;
                                    targetSlot.OnSelectDone();
                                    prevSlot.KillCharacter();
                                    break;
                                case SlotState.HasCharacter:
                                    if (targetSlot.IDHero == prevSlot.IDHero && MergeCharacterManager.Instance.CanMerge(prevSlot.IDHero))
                                    {
                                        Debug.Log("Merger Success!!!");
                                        targetSlot.MergeSuccess();
                                        prevSlot.KillCharacter();
                                    }
                                    break;
                            }
                            Refresh();

                        }
                    }
                }
            }
            if (prevSlot != null)
            {
                prevSlot.OnSelectDone();
            }
            if (hightLightSlot != null)
            {
                hightLightSlot.CanMerger = false;
            }
            hightLightSlot = null;
            prevSlot = null;
        }
    }

    private void RefreshSlot(Slot _currentSlot, byte _indexNext)
    {
        _currentSlot.State = SlotState.Non;
        if (_indexNext + 1 < slots.Count)
        {
            slots[_indexNext + 1].State = SlotState.LockADS;
        }
    }
    private void OnClaimPlayer(ItemID _id)
    {
        Slot slotNon = GetSlotNon();
        if (slotNon != null)
        {
            User.Instance.AddListSkinMerge(_id);
            byte index = (byte)slots.IndexOf(slotNon);
            slotNon.SetupSlot(_id, index, layerTop);
        }
        else
        {
            User.Instance.AddListMergeDu(_id);
        }
        Refresh();
    }
    private void Refresh()
    {
        GameEvent.OnRefreshBtnPrice?.Invoke();
        Slot slotNon = GetSlotNon();
        if (slotNon != null)
        {
            btnBuyPlayer1.gameObject.SetActive(true);
            btnBuyPlayer2.gameObject.SetActive(true);

            if(btnBuyPlayer3.indexChar <= User.Instance.Car.slot)
            {
                btnBuyPlayer3.gameObject.SetActive(true);
            }
            else
            {
                btnBuyPlayer3.gameObject.SetActive(false);
            }
            if (btnBuyPlayer4.indexChar <= User.Instance.Car.slot)
            {
                btnBuyPlayer4.gameObject.SetActive(true);
            }
            else
            {
                btnBuyPlayer4.gameObject.SetActive(false);
            }
        }
        else
        {
            btnBuyPlayer1.gameObject.SetActive(false);
            btnBuyPlayer2.gameObject.SetActive(false);
            btnBuyPlayer3.gameObject.SetActive(false);
            btnBuyPlayer4.gameObject.SetActive(false);
        }
        ShowFxCanMerge();


    }
    private Slot GetSlotNon()
    {
        return slots.Find(x => x.State == SlotState.Non);
    }
    private List<FxItem> listFxCanMerge = new List<FxItem>();
    private void ShowFxCanMerge()
    {
        listFxCanMerge.ForEach(x =>
        {
            if (x != null)
            {
                x.Kill();
            }
        });
        listFxCanMerge.Clear();
        foreach (Slot slot in slots)
        {
            if (slot.State != SlotState.HasCharacter)
            {
                continue;
            }
            List<ItemID> listID = slots.Where(x => x.IDHero == slot.IDHero).Select(x => x.IDHero).ToList();
            if (listID != null)
            {
                if (listID.Count > 1)
                {
                    if (MergeCharacterManager.Instance.IsTypePet(slot.IDHero))
                    {
                        FxItem fxItem = ContentPoolable.Emit(ItemID.fxCanMergePistol) as FxItem;
                        fxItem.transform.SetParent(slot.transform);
                        fxItem.transform.localPosition = Vector3.zero;
                        fxItem.transform.localScale = Vector3.one;
                        fxItem.transform.SetAsFirstSibling();
                        listFxCanMerge.Add(fxItem);

                    }
                    else if (MergeCharacterManager.Instance.IsHero(slot.IDHero))
                    {
                        FxItem fxItem = ContentPoolable.Emit(ItemID.fxCanMergeMain) as FxItem;
                        fxItem.transform.SetParent(slot.transform);
                        fxItem.transform.localPosition = Vector3.zero;
                        fxItem.transform.localScale = Vector3.one;
                        fxItem.transform.SetAsFirstSibling();
                        listFxCanMerge.Add(fxItem);
                    }
                    else if (MergeCharacterManager.Instance.IsTypePet2(slot.IDHero))
                    {
                        FxItem fxItem = ContentPoolable.Emit(ItemID.fxCanMergeAK) as FxItem;
                        fxItem.transform.SetParent(slot.transform);
                        fxItem.transform.localPosition = Vector3.zero;
                        fxItem.transform.localScale = Vector3.one;
                        fxItem.transform.SetAsFirstSibling();
                        listFxCanMerge.Add(fxItem);
                    }
                    else if (MergeCharacterManager.Instance.IsTypePet3(slot.IDHero))
                    {
                        FxItem fxItem = ContentPoolable.Emit(ItemID.fxCanMergeBazoka) as FxItem;
                        fxItem.transform.SetParent(slot.transform);
                        fxItem.transform.localPosition = Vector3.zero;
                        fxItem.transform.localScale = Vector3.one;
                        fxItem.transform.SetAsFirstSibling();
                        listFxCanMerge.Add(fxItem);
                    }
                }
            }
        }
    }
    private void ShowHeroGraphic(ItemID _id = ItemID.None)
    {
        //animCharacter.Initialize(false);
        //var customSkin = new Skin("CustomSkin");
        //ItemID itemIDWeapon = MergeCharacterManager.GetNameWeapon(User.Instance.usingSkin);
        //List<string> skins = animCharacter.SkeletonDataAsset.GetSkeletonData(true).Skins.Items.Select(x => x.ToString()).ToList();
        //string current = $"{User.Instance.usingSkin.ToString()}";
       
        //string currentFind = null;
        //currentFind = skins.Find(x => x.Contains(current));
        //if (currentFind == null)
        //{
        //    currentFind = skins.Find(x => x.Contains(User.Instance.usingSkin.ToString()));
        //}
        //var charSkin = animCharacter.Skeleton.Data.FindSkin(currentFind);
        //customSkin.AddSkin(charSkin);
        //var weaponSkin = animCharacter.Skeleton.Data.FindSkin(skins.Find(x => x.Contains(itemIDWeapon.ToString())));
        //customSkin.AddSkin(weaponSkin);
        //animCharacter.Skeleton.SetSkin(customSkin);
        //animCharacter.Skeleton.SetSlotsToSetupPose();
        //animCharacter.AnimationState.SetAnimation(0, "Idle_mp5", true);
    }
    private void ShowPetGraphic(ItemID _id = ItemID.None)
    {
        //animPet.Initialize(false);
        //var customSkin = new Skin("CustomSkin");
        //ItemID itemIDWeapon = MergeCharacterManager.GetNameWeapon(User.Instance.usingPet);
        //List<string> skins = animPet.SkeletonDataAsset.GetSkeletonData(true).Skins.Items.Select(x => x.ToString()).ToList();
        //string current = $"{User.Instance.usingPet.ToString()}";
        //if (FirebaseManager.Instance.remote.GetVerCompact())
        //{
        //    current = $"{current}_mask";
        //}
        //string currentFind = null;
        //currentFind = skins.Find(x => x.Contains(current));
        //if (currentFind == null)
        //{
        //    currentFind = skins.Find(x => x.Contains(User.Instance.usingPet.ToString()));
        //}
        //var charSkin = animPet.Skeleton.Data.FindSkin(currentFind);
        //customSkin.AddSkin(charSkin);
        //var weaponSkin = animPet.Skeleton.Data.FindSkin(skins.Find(x => x.Contains(itemIDWeapon.ToString())));
        //customSkin.AddSkin(weaponSkin);
        //animPet.Skeleton.SetSkin(customSkin);
        //animPet.Skeleton.SetSlotsToSetupPose();
        //animPet.AnimationState.SetAnimation(0, "Idle_mp5", true);
    }
}
