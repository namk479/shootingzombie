using System.Collections.Generic;
using Thanh.Core;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupDeal : Popup
{
    public Button buttonGetIt;
    public GameObject bg;
    public bool isGetIt;

    public override void OnShow()
    {
        base.OnShow();
        buttonGetIt.onClick.RemoveListener(GetIt);
        buttonGetIt.onClick.AddListener(GetIt);
        bg.transform.localScale = Vector3.zero;
        bg.transform.DOScale(Vector3.one,0.5f);
        isGetIt = false;
    }

    public void GetIt()
    {
        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, (isSuccess) =>
        {
            if (isSuccess)
            {
                User.Instance[ItemID.hotDeal] = 1;
                isGetIt = true;
                this.Close();
                GlobalData.gameMode = GameMode.Normal;
                Loader.Instance.LoadScene(SceneName.GameScene.ToString());
            }
        }, AdLocation.Spin);
    }

    public override void Close()
    {
        base.Close();
        if (!isGetIt)
        {
            PopupManager.Instance.OpenPopup<PopupDefeat2>(PopupID.PopupDefeat2);
        }
    }
}
