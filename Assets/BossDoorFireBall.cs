using AA_Game;
using UnityEngine;
using Yurowm.GameCore;

public class BossDoorFireBall : Item
{
    public Rigidbody2D rigidbody2D;
    public float damage;

    public void AddForce()
    {
        rigidbody2D.AddForce(transform.right * 50f,ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            TrainManager target = collision.gameObject.GetComponent<TrainManager>();
            target.damageGiven = damage;
            if (target.carState == CarState.Hit)
            {
                target.GetHitBullet();
            }
            else
            {
                target.ChangeState(new CarHitState(target));
            }


            FxItem explosion = ContentPoolable.Emit(ItemID.fxFireBallDoorExplosion) as FxItem;
            explosion.transform.position = this.transform.position;
            this.Kill();
        }
    }
}
