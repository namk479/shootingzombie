using System.Collections.Generic;
using Thanh.Core;
using UnityEngine.UI;

public class PopupGotoStore : Popup
{
    public Button adsGold;
    public Button goToShop;
    public List<ShopInventory> _ShopGold = new List<ShopInventory>();
    public ShopInventory inventory;
    public TMPro.TMP_Text goldValue;

    public override void OnShow()
    {
        base.OnShow();
        SetUp();
        inventory = _ShopGold[User.Instance[ItemID.adsShopGoldWatched]];
        goldValue.text = "+"+inventory.itemcell.value.ToString();
    }

    public void SetUp()
    {
        adsGold.onClick.RemoveListener(ClaimGoldByAds);
        adsGold.onClick.AddListener(ClaimGoldByAds);

        goToShop.onClick.RemoveListener(GoToShop);
        goToShop.onClick.AddListener(GoToShop);
    }

    public void ClaimGoldByAds()
    {
        AudioManager.instance.Play("BtnClick");
        inventory = _ShopGold[User.Instance[ItemID.adsShopGoldWatched]];
        int amountIcon = inventory.itemcell.value;

        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, (isSuccess) =>
        {
            if (isSuccess)
            {
                PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem,
                        (pop) =>
                        {
                            pop.SetData(inventory.itemcell);
                            PopupClaimItem.Instane.btnX2.gameObject.SetActive(false);
                            PopupClaimItem.Instane.OkCollect.gameObject.SetActive(true);
                        });
                User.Instance[ItemID.Gold] += amountIcon;
                User.Instance[ItemID.adsShopGoldWatched] += 1;
                if (User.Instance[ItemID.adsShopGoldWatched] == 5)
                {
                    User.Instance[ItemID.adsShopGoldWatched] = 0;
                }
                GameEvent.OnBuyShopAds.Invoke(User.Instance[ItemID.adsShopGoldWatched]);



                //refesh
                inventory = _ShopGold[User.Instance[ItemID.adsShopGoldWatched]];
                goldValue.text = "+" + inventory.itemcell.value.ToString();
            }
        }, AdLocation.ShopPack);
    }

    public void GoToShop()
    {
        AudioManager.instance.Play("BtnClick");
        PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) =>
        {
            pop.ButtonGoldIAP();
        });
    }
}
