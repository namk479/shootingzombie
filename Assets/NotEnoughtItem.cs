using Thanh.Core;

public class NotEnoughtItem : Popup
{
    public void GoBtn()
    {
        this.Close();
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode);
    }
}
