using System.Collections;
using UnityEngine;
using Yurowm.GameCore;
using DG.Tweening;

public class BossDragonController : EnemyBase
{
    private float timeFree;
    private TypeDragonAttack TypeDragonAttack;
    private Vector3 startPos = new Vector3(17, -10, 0);
    public bool isDragonGreen;
    public bool isDragonRed;
    public bool isDragonBlue;
    public ParticleSystem fireDragon;
    public BoxCollider2D boxFire;


    public override void OnEnable()
    {
        base.OnEnable();
        TypeDragonAttack = TypeDragonAttack.AttackNear;



        if(GlobalData.gameMode == GameMode.BossWorld)
        {
            speedBase = 6;
            speed = speedBase;
            healthBase = 3 * healthBase;
            health = healthBase;
            damage = damage * 3;
            ChangeState(new SpawnState(this));
        }
        else
        {
            speedBase = 16;
            speed = speedBase;
            ChangeState(new FollowCar(this));
        }
    }

    public override void EnterSpawn()
    {
        base.EnterSpawn();
        StartCoroutine(IE_PlayFXAppear());
        boxFire.enabled = false;
    }

    /// <summary>
    /// Spawn dragon,chay het anim spawn se chuye qua free
    /// </summary>
    public override void UpdateSpawn()
    {
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.spawn, 1).duration)
        {
            ChangeState(new FreeState(this));
        }
    }


    public override void HandleEvent(string eventName)
    {
        if (enemyState == EnemyState.PowerUp)
        {
            return;
        }

        if (eventName == "attack" || eventName == "attack_tracking" || eventName == "hit")
        {
            target.damageGiven = damage;
            if (target.carState == CarState.Hit)
            {
                target.GetHitBullet();
            }
            else
            {
                if (GlobalData.gameMode == GameMode.Normal || GlobalData.gameMode == GameMode.Endless || GlobalData.gameMode == GameMode.BossWorld)
                {
                    target.ChangeState(new CarHitState(target));
                }
                else
                {
                    target.GetHitBullet();
                }
            }
        }


        ///hien fx hit car
        if (enemyState == EnemyState.Attack50)
        {
            if (eventName == "attack" || eventName == "attack_tracking" || eventName == "hit")
            {
                FxItem fxHitCar = ContentPoolable.Emit(ItemID.dragonHitCar) as FxItem;
                fxHitCar.transform.position = new Vector3(target.transform.position.x + 2f, target.transform.position.y + 2f, 10);
            }
            AudioManager.instance.Play("DragonAtkGan");
        }
    }


    /// <summary>
    /// Chay FX smoke khi dap dat
    /// </summary>
    IEnumerator IE_PlayFXAppear()
    {
        yield return new WaitForSeconds(1.4f);
        FxItem fxDie = ContentPoolable.Emit(ItemID.smoke_1) as FxItem;
        fxDie.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 1f, this.transform.position.z);
        fxDie.transform.localScale = new Vector3(2, 2, 2);
        AudioManager.instance.Play("DragonDie");
    }

    /// <summary>
    /// Free mot luc roi sau do di chuyen de danh
    /// </summary>
    public override void EnterFree()
    {
        base.EnterFree();
        timeFree = Random.Range(1.5f, 2.5f);
    }


    /// <summary>
    /// Chay het thoi gian nghi sau do chuyen state
    /// </summary>
    public override void UpdateFree()
    {
        base.UpdateFree();
        timeInState += Time.deltaTime;
        if (timeInState >= timeFree && GameManager.Instance.gameState == GameState.Playing)
        {
            ChangeState(new BossPowerUpState(this));
        }
    }



    //override state follow car de ko phai viet sate moi
    public override void EnterFollowCar()
    {
        enemyState = EnemyState.FollowCar;
        speed = speedBase;
        capsuleCollider2D.enabled = true;


        if(GlobalData.gameMode == GameMode.BossWorld)
        {
            anim.PlayAnim(AnimID.run, true, 1.5f, false);
        }
        else
        {
            transform.DOMoveY(1f, 1F);
            anim.PlayAnim(AnimID.fly, true, 1.5f, false);
        }
    }

    /// <summary>
    /// Dragon walk den ga cai xe
    /// Neu du gan se tan con gan Attack50HealthStage
    /// </summary>
    public override void UpdateFollowCar()
    {
        if(GlobalData.gameMode == GameMode.BossWorld)
        {
            targetComeTo = target.transform.position;
            float dis = Vector3.Distance(transform.position, targetComeTo);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, speed * Time.deltaTime);

            //kiem tra xem danh gan hay danh xa
            if (TypeDragonAttack == TypeDragonAttack.AttackNear)
            {
                if (dis <= distanceAttack)
                {
                    ChangeState(new Attack50HealthState(this));
                }
            }
            else
            {
                if (dis <= distanceAttack * 1.3f)
                {
                    ChangeState(new Attack100HealthState(this));
                }
            }
        }
        else
        {
            base.UpdateFollowCar();
        }
    }

    public override void EnterAttack()      
    {
        enemyState = EnemyState.Attack;
        //anim.PlayAnim(AnimID.fly_attack, true, 1, false);
        anim.PlayAnim(AnimID.fly_attack, true, 1, false);
        StartCoroutine(DelayShowFire(1.6f, 1.3f));
    }


    /// <summary>
    /// Enter attack danh gan
    /// </summary>
    public override void EnterAttack50()
    {
        enemyState = EnemyState.Attack50;
        anim.PlayAnim(AnimID.attack_1, true, 1.5f, false);
    }

    /// <summary>
    /// Enter attack danh gan
    /// </summary>
    public override void UpdateAttack50()
    {
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack_1, 1.5f).duration * 3f)
        {
            ChangeState(new LeaveTheCarState(this));
        }
    }


    /// <summary>
    /// Next attack will be attack far
    /// </summary>
    public override void ExitAttack50()
    {
        base.ExitAttack50();
        TypeDragonAttack = TypeDragonAttack.AttackFar;
        AudioManager.instance.Stop("DragonAtkGan");
    }


    /// <summary>
    /// Show fire dragon
    /// </summary>
    public override void EnterAttack100()
    {
        base.EnterAttack100();
        if (!isDragonRed)
        {
            StartCoroutine(DelayShowFire(1.8f));
        }

        //neu nhu la dragon red thi se bay len sau do moi khac lua
        if (isDragonRed)
        {
            anim.PlayAnim(AnimID.jump, true, 1, false);
            Vector3 posFly = new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z);
            this.transform.DOLocalJump(posFly, 1, 1, 2).OnComplete(() => { anim.PlayAnim(AnimID.attack_2, true, 1, false); StartCoroutine(DelayShowFire(1.6f,1.3f)); });
        }
    }


    public override void UpdateAttack100()
    {
        base.UpdateAttack100();
        if (isDragonGreen || isDragonBlue)
        {
            if (timeInState >= (anim.GetAnimData(AnimID.attack_2).duration))
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
        else
        {
            if (timeInState >= ((anim.GetAnimData(AnimID.attack_2).duration) + 2))
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }



    /// <summary>
    /// Delay show for wait animation
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayShowFire(float timeDelay,float timeOff = 1.5f)
    {
        yield return new WaitForSeconds(timeDelay);
        fireDragon.Simulate(0.0f, true, true);
        fireDragon.Play();
        boxFire.enabled = true;
        AudioManager.instance.Play("Dragonfire");
        yield return new WaitForSeconds(timeOff);
        fireDragon.Stop();
        boxFire.enabled = false;
        AudioManager.instance.Stop("Dragonfire");
    }

    /// <summary>
    /// Off fire
    /// </summary>
    public override void ExitAttack100()
    {
        base.ExitAttack100();
        StopCoroutine(DelayShowFire(0));
        TypeDragonAttack = TypeDragonAttack.AttackNear;
        fireDragon.Stop();
        boxFire.enabled = false;
    }


    /// <summary>
    /// Dragon lui lai sau khi tan con gan
    /// </summary>
    public override void EnterLeaveTheCar()
    {
        enemyState = EnemyState.LeaveTheCar;
        anim.PlayAnim(AnimID.runBack, true, 2f, false);
        capsuleCollider2D.enabled = true;

        if (isDragonRed)
        {
            if(TypeDragonAttack == TypeDragonAttack.AttackNear)
            {
                anim.PlayAnim(AnimID.jump, true, 1f, false);
            }
            else
            {
                anim.PlayAnim(AnimID.runBack, true, 2f, false);
            }
        }
    }


    /// <summary>
    /// Lui ve vi tri ban dau de chuan bi attack xa
    /// </summary>
    public override void UpdateLeaveTheCar()
    {
        transform.position = Vector3.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        float dis = Vector3.Distance(this.transform.position, startPos);
        if(dis <= 0.5f)
        {
            ChangeState(new FreeState(this));
        }
    }



    //==========ATTACK===========//
    FxItem fxHealing;
    public override void EnterPowerUp()
    {
        base.EnterPowerUp();
        fxHealing = ContentPoolable.Emit(ItemID.fxBossHealing) as FxItem;
        fxHealing.transform.position = this.transform.position;
        fxHealing.transform.localScale = new Vector3(6,6,6);
        AudioManager.instance.Play("fxHealing");
    }

    public override void UpdatePowerUp()
    {
        base.UpdatePowerUp();
    }

    public override void ExitPowerUp()
    {
        base.ExitPowerUp();
        health += healthBase * 0.05f;
        if (health > healthBase)
            health = healthBase;
        healthBar.red.transform.localScale = new Vector3(health / healthBase, healthBar.red.transform.localScale.y, healthBar.red.transform.localScale.z);
        fxHealing.Kill();
        AudioManager.instance.Stop("fxHealing");

    }


    public override void EnterDie()
    {
        base.EnterDie();
        User.Instance[ItemID.KillBoss3Times] += 1;
        User.Instance[ItemID.KillBoss10Times] += 1;
        User.Instance[ItemID.KillBoss20Times] += 1;
        User.Instance[ItemID.KillBoss40Times] += 1;
        
    }

    public override void UpdateAttack()
    {
        targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed + SpeedGrowingUp)/1.3f * Time.deltaTime);
        timeInState += Time.deltaTime;



        if (timeInState >= (anim.GetAnimData(AnimID.fly_attack).duration / anim.TimeScale))
        {
            ChangeState(new FollowCarSlowdown(this));
        }

    }

    //==========FOLLOW CAR SLOWDOWN===========//
    public override void EnterFollowCarSlow()
    {
        enemyState = EnemyState.FollowCar;
        speed = speedBase * 0.6f;
        anim.PlayAnim(AnimID.fly, true, 1f, false);
        capsuleCollider2D.enabled = true;
    }
}

public enum TypeDragonAttack
{
    AttackNear,
    AttackFar,
}
