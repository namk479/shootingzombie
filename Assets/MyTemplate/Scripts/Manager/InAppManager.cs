﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MyGame;
using System.Linq;
//using AppsFlyerSDK;
using UnityEngine.Purchasing.Extension;
using com.adjust.sdk;
#if IAP
using UnityEngine.Purchasing.Security;
using UnityEngine.Purchasing;
#endif

public class InAppManager : Singleton<InAppManager>
#if IAP
    //, IAppsFlyerValidateReceipt
    , IStoreListener
#endif
{
    // public IAP_Data iAP_Data;
    [TableList]
    public List<IAPPack> packs;
#if IAP
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
#endif
    private Action<bool> OnComplete;
    [HideInInspector]
    public float lastTimeShowIAP;
    [HideInInspector]
    public bool showingIAP;
    private double price;
    private string currency;
    private string transactionID;

    [SerializeField] private string public_key;
    void Start()
    {
        showingIAP = false;
#if IAP
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
#endif
    }
#if IAP
    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        for (int i = 0; i < packs.Count; i++)
        {
            var sku = packs[i];
            Debug.Log("init sku " + sku.skuID.ToString());
            builder.AddProduct(sku.skuID.ToString(), ProductType.Consumable);
        }
        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    ItemID curSkuID;
    public void PurchaseProduct(ItemID skuID, Action<bool> OnComplete)
    {
        showingIAP = true;
        StopAllCoroutines();
        curSkuID = skuID;
        this.OnComplete = OnComplete;
        if (Application.isEditor)
        {
            User.Instance[curSkuID]++;
            User.Instance[ItemID.IAP_Count]++;

            //AdsManager.Instance.HideBanner();
            OnComplete?.Invoke(true);
            showingIAP = false;
            return;
        }
        //#if UNITY_IOS
        //		DialogManager.I.OnShowLoadingIAP();
        //#endif
#if IAP
        BuyProductID(skuID.ToString());
#endif
    }

    public string GetPrice(ItemID skuID)
    {
        var sku = skuID.ToString();
#if IAP
        if (!IsInitialized() || Application.isEditor)
        {
            return packs.Find(x => x.skuID == skuID).DefaultPrice;
        }

        Product pd = m_StoreController.products.WithID(sku);
        if (pd != null)
        {
            string Price = pd.metadata.localizedPriceString;
            return Price;
        }
#endif
        return packs.Find(x => x.skuID == skuID).DefaultPrice;
    }

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases(Action<bool> OnComplete)
    {
        this.OnComplete = OnComplete;

        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result, str) =>
            {
                //apple.GetProductDetails()
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        string prodID = args.purchasedProduct.definition.id;
        string price = args.purchasedProduct.metadata.localizedPrice.ToString();
        string currency = args.purchasedProduct.metadata.isoCurrencyCode;

        string receipt = args.purchasedProduct.receipt;
        //var recptToJSON = (Dictionary<string, object>)AFMiniJSON.Json.Deserialize(args.purchasedProduct.receipt);
        //var receiptPayload = (Dictionary<string, object>)AFMiniJSON.Json.Deserialize((string)recptToJSON["Payload"]);
        var transactionID = args.purchasedProduct.transactionID;

        bool validPurchase = true; // Presume valid for platforms with no R.V.

        // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        // Prepare the validator with the secrets we prepared in the Editor
        // obfuscation window.
        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
            AppleTangle.Data(), Application.identifier);

        try
        {
            // On Google Play, result has a single product ID.
            // On Apple stores, receipts contain multiple products.
            var result = validator.Validate(args.purchasedProduct.receipt);
            // For informational purposes, we list the receipt(s)
            Debug.Log("Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result)
            {
                Debug.Log(productReceipt.productID);
                Debug.Log(productReceipt.purchaseDate);
                Debug.Log(productReceipt.transactionID);
            }
        }
        catch (IAPSecurityException)
        {
            Debug.Log("Invalid receipt, not unlocking content");
            validPurchase = false;
        }
#endif

        if (validPurchase)
        {
            // Unlock the appropriate content here.
            StartCoroutine(IEWaitGameFocus(true));
#if UNITY_IOS

            //if (isSandbox)
            //{
            //    AppsFlyeriOS.setUseReceiptValidationSandbox(true);
            //}

            AppsFlyer.validateAndSendInAppPurchase(prodID, price, currency, transactionID, null, this);
#elif UNITY_ANDROID
            //var purchaseData = (string)receiptPayload["json"];
            //var signature = (string)receiptPayload["signature"];
            //AppsFlyer.validateAndSendInAppPurchase(
            //public_key,
            //signature,
            //purchaseData,
            //price,
            //currency,
            //null,
            //this);

            AdjustEvent adjustEvent = new AdjustEvent(public_key);
            adjustEvent.setRevenue((double)args.purchasedProduct.metadata.localizedPrice, currency);
            Adjust.trackEvent(adjustEvent);
#endif
        }
        else
        {
            StartCoroutine(IEWaitGameFocus(false));
        }
        return PurchaseProcessingResult.Complete;
    }


#endif

#if IAP
    private IEnumerator IEWaitGameFocus(bool success)
    {
        while (!Application.isFocused)
        {
            yield return null;
        }
        if (success)
        {
            price = 0d;
            Debug.Log("Purchase Success " + curSkuID);
            User.Instance[curSkuID]++;
            User.Instance[ItemID.IAP_Count]++;
            //
            //AdsManager.Instance.HideBanner();
            FirebaseManager.Instance.iapPurchased(curSkuID.ToString());
        }
        OnComplete?.Invoke(success);
        showingIAP = false;
        lastTimeShowIAP = Time.time;
    }



    public void didFinishValidateReceipt(string result)
    {
        //AppsFlyer.AFLog("didFinishValidateReceipt", result);
    }

    public void didFinishValidateReceiptWithError(string error)
    {
        //AppsFlyer.AFLog("didFinishValidateReceiptWithError", error);
    }

#if IAP
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    public void OnInitializeFailed(InitializationFailureReason error, string message)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription)
    {
        StartCoroutine(IEWaitGameFocus(false));
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureDescription));
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        StartCoroutine(IEWaitGameFocus(false));
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
#endif

#endif
}

[Serializable]
public class IAPPack
{
    public ItemID skuID;
    public float defaul_USD;

    public string DefaultPrice
    {
        get
        {
            return defaul_USD + "$";
        }
    }
}