﻿using DG.Tweening;
using MyGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Yurowm.GameCore;

public class BuyManager : Singleton<BuyManager>
{
    List<ItemValueFloat> requires = null;
    List<ItemValue> rewards = null;
    UnityAction<bool> OnDone = null;
    UnityAction OnClaimDone = null;
    AdLocation location;
    bool isFree;
    bool iapPack;
    string txtBtnAds;
    string txtDescription;
    ItemID chestID;

    public void Buy(List<ItemValueFloat> _requires, List<ItemValue> _rewards, UnityAction<bool> _OnDone, AdLocation _location, UnityAction _OnClaimDone = null, bool _isFree = false, bool iapPack = false, string _txtBtnAds = "EXTRA CARD", string _txtDescription = "You will randomly receive 1 reward in opened cards", ItemID _chestID = ItemID.None)
    {
        requires = _requires;
        rewards = _rewards;
        OnDone = _OnDone;
        location = _location;
        OnClaimDone = _OnClaimDone;
        isFree = _isFree;
        txtBtnAds = _txtBtnAds;
        txtDescription = _txtDescription;
        this.iapPack = iapPack;
        chestID = _chestID;

        if (requires == null || requires.Count == 0)
        {
            OnCheckDone(true);
            return;
        }

        if (requires.Contains(x => x.item == ItemID.Ads))
        {
            OnAds(OnCheckDone);
        }
        else if (requires.Contains(x => x.item.IsIapPack()))
        {
            OnIAP(requires[0].item, OnCheckDone);
        }
        else
        {
            foreach (ItemValueFloat require in requires)
            {
                int userValue = User.Instance[require.item];
                if (userValue < require.value)
                {
                    Debug.Log("Not enough " + require.item.ToString());
                    OnCheckDone(false, require.item);
                    return;
                }
            }

            OnCheckDone(true);
        }
    }

    void OnAds(UnityAction<bool, ItemID> OnCompleted)
    {
        AdsManager.Instance.ShowVideoReward(location, () =>
        {
            OnCompleted?.Invoke(true, ItemID.Ads);
        },
        () => OnCompleted?.Invoke(false, ItemID.Ads));
    }

    void OnIAP(ItemID pack, UnityAction<bool, ItemID> OnCompleted)
    {
#if IAP
        InAppManager.Instance.PurchaseProduct(pack, (isSuccess) =>
        {
            OnCompleted?.Invoke(isSuccess, pack);
        });
#endif
    }

    void OnCheckDone(bool isSuccess, ItemID failItem = ItemID.None)
    {
        if (isSuccess)
        {
            CheckQuest();
            StartCoroutine(OnClaimRewards());
        }
        else
        {
            OnFail(failItem);
            OnClaimDone = null;
        }

        requires = null;
        rewards = null;
        OnDone?.Invoke(isSuccess);
    }


    public void CheckQuest()
    {
        User.Instance[ItemID.Watch10Ads] += 1;
        User.Instance[ItemID.Watch30Ads] += 1;
        User.Instance[ItemID.Watch50Ads] += 1;
    }

    //void CheckMission(AdLocation adLocation)
    //{
    //    List<MissionID> missionIDs = new List<MissionID>();

    //    if (requires != null && requires.Contains(x => x.item == ItemID.Ads))
    //    {
    //        missionIDs.Add(MissionID.Ads_Reward);
    //    }

    //    switch (adLocation)
    //    {
    //        case AdLocation.PopupSpin:
    //        case AdLocation.SpinAds:
    //        case AdLocation.SpinGold:
    //            {
    //                missionIDs.Add(MissionID.Use_Lucky_Spin);
    //                break;
    //            }
    //        case AdLocation.CommonChest:
    //        case AdLocation.Common_Chest_Key:
    //        case AdLocation.Common_Chest_Gem:
    //            {
    //                missionIDs.Add(MissionID.Open_Chest);
    //                missionIDs.Add(MissionID.Open_Common_Chest);
    //                break;
    //            }
    //        case AdLocation.Common_Chest_Ads:
    //            {
    //                missionIDs.Add(MissionID.Open_Chest);
    //                missionIDs.Add(MissionID.Open_Common_Chest);
    //                missionIDs.Add(MissionID.Open_Free_Common_Chest);
    //                break;
    //            }
    //        case AdLocation.EpicChest:
    //        case AdLocation.Epic_Chest_Key:
    //        case AdLocation.Epic_Chest_Gem:
    //            {
    //                missionIDs.Add(MissionID.Open_Chest);
    //                missionIDs.Add(MissionID.Open_Epic_Chest);
    //                break;
    //            }
    //        case AdLocation.Epic_Chest_Ads:
    //            {
    //                missionIDs.Add(MissionID.Open_Chest);
    //                missionIDs.Add(MissionID.Open_Epic_Chest);
    //                missionIDs.Add(MissionID.Open_Free_Epic_Chest);
    //                break;
    //            }
    //        case AdLocation.DailyReward:
    //            {
    //                missionIDs.Add(MissionID.Claim_Daily_Reward);
    //                break;
    //            }
    //        case AdLocation.FreeGold:
    //            {
    //                missionIDs.Add(MissionID.Open_Free_Gold_Shop);
    //                break;
    //            }
    //        case AdLocation.FreeGem:
    //            {
    //                missionIDs.Add(MissionID.Open_Free_Gem_Shop);
    //                break;
    //            }
    //    }

    //    if (missionIDs.Count > 0)
    //    {
    //        missionIDs.ForEach(x => MissionManager.Achieve(x, 1));
    //    }
    //}

    static List<ItemID> NoShowClaimItems = new List<ItemID> { ItemID.RemoveAds };

    IEnumerator OnClaimRewards()
    {
        if (requires != null)
        {
            requires.ForEach(x => User.Instance[x.item, location] -= (int)x.value);
        }

        if (rewards != null && rewards.Count > 0)
        {
            List<ItemValue> noShowClaimRewards = rewards.Where(x => NoShowClaimItems.Contains(x.item)).ToList();
            noShowClaimRewards.ForEach(x =>
            {
                User.Instance[x.item, location] += x.value;
            });

            rewards.RemoveAll(x => noShowClaimRewards.Contains(x));

            //ItemValue unlockRandomHero = rewards.Find(x => x.item == ItemID.UnlockRandomHero);
            //if (unlockRandomHero != null)
            //{
            //    ItemID rewardHero = ItemID.Ruto;
            //    List<ItemID> notUnlockedHeros = CharacterDataManager.Instance.characterDatas.Where(x => User.Instance.GetHero(x.id).Unlocked == false).Select(x => x.id).ToList();
            //    if (notUnlockedHeros != null && notUnlockedHeros.Count > 0)
            //    {
            //        rewardHero = notUnlockedHeros.GetRandom();
            //        User.Instance.UnlockHero(rewardHero);
            //        User.Instance[rewardHero, location] += unlockRandomHero.value;
            //        needWaitUnlockHero = true;
            //        PopupManager.Instance.OpenPopup<PopupEvovleDone>(PopupID.PopupEvovleDone, (pop) =>
            //        {
            //            pop.SetData(rewardHero, "New hero ");
            //            pop.onClose.AddListener(() => needWaitUnlockHero = false);
            //        });
            //    }
            //    else
            //    {
            //        // Không có hero chưa unlock -> thay bằng mảnh hero
            //        rewards.Add(new ItemValue(CharacterDataManager.Instance.characterDatas.Select(x => x.id).GetRandom(), unlockRandomHero.value));
            //    }

            //    rewards.Remove(unlockRandomHero);
            //}

            //ItemValue randomEvovle = rewards.Find(x => x.item == ItemID.RandomEvovle);
            //if (randomEvovle != null)
            //{
            //    rewards.Remove(randomEvovle);
            //    rewards.Add(new ItemValue(ItemType.Evovles.GetRandom(), randomEvovle.value));
            //}
        }

        //if (needWaitUnlockHero)
        //{
        //    yield return new WaitUntil(() => needWaitUnlockHero == false);
        //}

        //if (rewards != null && rewards.Count > 0)
        //{
        //    PopupManager.Instance.OpenPopup<PopupOpenCards>(PopupID.PopupOpenCards, (pop) =>
        //    {
        //        pop.SetData(rewards, isFree, this.iapPack, location, txtBtnAds, txtDescription, chestID);
        //        if (OnClaimDone != null) pop.onClose.AddListener(OnClaimDone);
        //    });
        //}
        //else
        //{
        OnClaimDone?.Invoke();
        OnClaimDone = null;
        //}

        yield break;
    }

    private void OnFail(ItemID failItem)
    {
        switch (failItem)
        {
            case ItemID.Ads:
                {
                    PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice, (pop) => pop.SetData("ADS NOT READY!", "Please check your internet connection!"));
                    break;
                }
            default:
                {

                    PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice, (pop) => pop.SetData("Notice", "Insufficient  " + failItem.ToString().Replace("_", " ") + " !"));

                    break;
                }
        }
    }

    //public static Dictionary<ItemID, MissionID> BuyItemGuide = new Dictionary<ItemID, MissionID>
    //{
    //    { ItemID.Gold, MissionID.Open_Free_Gold_Shop },
    //    { ItemID.Gem, MissionID.Open_Free_Gem_Shop },
    //    { ItemID.Ticket, MissionID.Use_Lucky_Spin },
    //};

    static int maxFxCount = 30;
    public static void ClaimFlyItem(ItemValue itemValue, Vector3 startPos, Vector3 endPos, AdLocation location, UnityAction OnComplete, bool realClaim = true)
    {
        ItemID itemID = itemValue.item;
        int totalValue = itemValue.value;

        int fxCount;    // Tổng số lượng fx sẽ sinh ra
        int fxValue;    // Lượng item sẽ được cộng thêm khi 1 fx done
        int fxEndValue; // Lượng item của fx cuối
        int maxFxValue = MaxFxValue(itemID);
        if (totalValue <= maxFxValue)
        {
            fxCount = 1;
            fxValue = 0;
            fxEndValue = totalValue;
        }
        else
        {
            fxCount = totalValue / maxFxValue;
            fxValue = maxFxValue;
            fxEndValue = fxValue + totalValue % fxValue;
        }

        if (fxCount > maxFxCount)
        {
            fxCount = maxFxCount;
            fxValue = totalValue / fxCount;
            fxEndValue = fxValue + totalValue % fxValue;
        }

        //Vector3 endPos = Vector3.zero;
        //switch (itemValue.item)
        //{
        //    case ItemID.Gold:
        //        {
        //            endPos = TopBar.Instance.gold.GetComponent<ItemValueUI>().icon.transform.position;
        //            break;
        //        }
        //    //case ItemID.Gem:
        //    //    {
        //    //        endPos = TopBar.Instance.gem.GetComponent<ItemValueUI>().icon.transform.position;
        //    //        break;
        //    //    }
        //}

        if (realClaim)
        {
            FirebaseManager.Instance.earn_virtual_currency(itemID, totalValue, location.ToString());
        }

        BuyManager.Instance.StartCoroutine(OnShowFlyItem(itemID, startPos, endPos, fxCount,
            () =>
            {
                if (realClaim)
                {
                    User.Instance[itemID, AdLocation.None] += fxValue;
                }
            },
            () =>
            {
                if (realClaim)
                {
                    User.Instance[itemID, AdLocation.None] += fxEndValue;
                }
                OnComplete?.Invoke();
            }
            ));
    }

    static int MaxFxValue(ItemID itemID)
    {
        switch (itemID)
        {
            case ItemID.Gold:
                {
                    return 100;
                }
                //case ItemID.Gem:
                //    {
                //        return 10;
                //    }
        }
        return 1000;
    }

    public static IEnumerator OnShowFlyItem(ItemID itemID, Vector3 startPos, Vector3 endPos, int cout, UnityAction OnEachComplete, UnityAction OnLastComplete)
    {
        List<ItemIcon> listITem = new List<ItemIcon>();
        for (int i = 0; i < cout; i++)
        {
            ItemIcon itemIcon = ContentPoolable.Emit(ItemID.ItemIcon) as ItemIcon;
            itemIcon.SetData(itemID);

            itemIcon.transform.parent = ItemFX.Instance.transform;
            itemIcon.transform.position = startPos;
            itemIcon.transform.localScale = Vector3.one;
            listITem.Add(itemIcon);
        }

        int moveDownCount = 0;
        float randomRadius = 1f;
        for (int i = 0; i < listITem.Count; i++)
        {
            listITem[i].transform.DOMove(startPos + new Vector3(UnityEngine.Random.Range(-randomRadius, randomRadius), UnityEngine.Random.Range(-randomRadius, randomRadius), 0), .3f)
                .SetDelay(UnityEngine.Random.Range(0, 0.5f)).SetUpdate(true)
                .OnComplete(() =>
                {
                    moveDownCount++;
                });
        }


        yield return new WaitUntil(() => moveDownCount == listITem.Count);

        int completeItem = 0;
        for (int i = 0; i < listITem.Count; i++)
        {
            listITem[i].transform.DOMove(endPos, 0.5f)
                .SetDelay(UnityEngine.Random.Range(0, 0.5f))
                .SetEase(Ease.InExpo).SetUpdate(true)
                .OnComplete(() =>
                {
                    switch (itemID)
                    {
                        case ItemID.Gold:
                            {
                                AudioAssistant.PlaySound("Coin");
                                break;
                            }
                            //case ItemID.Gem:
                            //    {
                            //        AudioAssistant.PlaySound("Gem");
                            //        break;
                            //    }
                    }

                    if (completeItem == listITem.Count - 1)
                    {
                        OnLastComplete?.Invoke();
                    }
                    else
                    {
                        OnEachComplete?.Invoke();
                    }

                    //SpawnFlyFxEnd(itemID, endPos);

                    completeItem++;
                });
        }
        yield return new WaitUntil(() => completeItem == listITem.Count);

        listITem.ForEach(x => x.Kill());
    }

    //static void SpawnFlyFxEnd(ItemID itemID, Vector3 pos)
    //{
    //    ItemID hitID = (ItemID)Enum.Parse(typeof(ItemID), itemID.ToString() + "Hit");

    //    var hitFx = ContentPoolable.Emit(hitID);
    //    if (hitFx == null)
    //    {
    //        return;
    //    }
    //    hitFx.transform.parent = ItemFX.Instance.transform;
    //    hitFx.transform.position = pos;
    //    hitFx.transform.localScale = Vector3.one;
    //}
}
