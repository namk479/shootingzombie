﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Threading.Tasks;
#if FIREBASE
using Firebase.RemoteConfig;
#endif

public class FirebaseRemote
{
    public bool IsInit;
    public bool FetchDone;
    private Dictionary<string, object> defaults;

    public FirebaseRemote()
    {
        defaults = new Dictionary<string, object>();
        defaults.Add(KeyRemote.LevelShowInter, ValueRemote.LevelShowInter);
        defaults.Add(KeyRemote.AndroidVersion, ValueRemote.VersionCurrent);
        defaults.Add(KeyRemote.IosVersion, ValueRemote.VersionCurrent);
        defaults.Add(KeyRemote.AndroidVersionDescription, ValueRemote.AndroidVersionDescription);
        defaults.Add(KeyRemote.IosVersionDescription, ValueRemote.IosVersionDescription);
        defaults.Add(KeyRemote.AndroidForceUpdate, ValueRemote.AndroidForceUpdate);
        defaults.Add(KeyRemote.IOSForceUpdate, ValueRemote.IosForceUpdate);
        defaults.Add(KeyRemote.TimeShowInter, ValueRemote.TimeShowInter);
        defaults.Add(KeyRemote.ShowAOALevel, ValueRemote.ShowAOALevel);
        defaults.Add(KeyRemote.APPID_IOS, ValueRemote.APPID_IOS);
        defaults.Add(KeyRemote.APPID_ANDROID, ValueRemote.APPID_ANDROID);
        defaults.Add(KeyRemote.WaitAOA, ValueRemote.WaitAOA);
        defaults.Add(KeyRemote.ShowSkill, ValueRemote.ShowSkill);
        defaults.Add(KeyRemote.ShowBonusLevel, ValueRemote.ShowBonusLevel);
        defaults.Add(KeyRemote.CheckInternet, ValueRemote.CheckInternet);
        defaults.Add(KeyRemote.TestIAP, ValueRemote.TestIAP);
        defaults.Add(KeyRemote.SkipADS, ValueRemote.SkipADS);
        defaults.Add(KeyRemote.WaveShowInter, ValueRemote.WaveShowInter);
        defaults.Add(KeyRemote.LevelWaveShowInter, ValueRemote.LevelWaveShowInter);
        defaults.Add(KeyRemote.WaveShowMayBay, ValueRemote.WaveShowMayBay);
    }

    public Task FetchDataAsync()
    {
#if FIREBASE
        FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults);
#endif
#if UNITY_EDITOR
        Debug.Log("Fetching data...");
#endif
        Task fetchTask = null;
#if FIREBASE
        fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(
        TimeSpan.Zero);
#endif
        return fetchTask.ContinueWith(FetchComplete);
    }
    private void FetchComplete(Task fetchTask)
    {
#if FIREBASE
        try
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("Fetch completed successfully!");
            }
            var info = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Info;
            switch (info.LastFetchStatus)
            {
                case Firebase.RemoteConfig.LastFetchStatus.Success:
                    Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
                    IsInit = true;
                    Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                                           info.FetchTime));
                    Debug.Log("---------------- Firebase Remote Init success ---------------");
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case Firebase.RemoteConfig.FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
            FetchDone = true;
        }
        catch { }
#endif
    }
    public float GetFloatValue(string key, double valueDefault)
    {
        double _value = valueDefault;
#if FIREBASE
        try
        {
            if (IsInit)
            {
                _value = FirebaseRemoteConfig.DefaultInstance.GetValue(key).DoubleValue;
            }
        }
        catch { }
#endif
        return (float)_value;
    }
    public int GetIntValue(string key, long valueDefault)
    {
        long _value = valueDefault;
#if FIREBASE
        try
        {
            if (IsInit)
                _value = FirebaseRemoteConfig.DefaultInstance.GetValue(key).LongValue;
        }
        catch { }
#endif
        return (int)_value;
    }
    public string GetStringValue(string key, string valueDefault)
    {
        var _value = valueDefault;
#if FIREBASE
        try
        {
            if (IsInit)
                _value = FirebaseRemoteConfig.DefaultInstance.GetValue(key).StringValue;
        }
        catch { }
#endif
        return _value;
    }
    public bool GetBooleanValue(string key, bool valueDefault)
    {
        var _value = valueDefault;
#if FIREBASE
        try
        {
            if (IsInit)
                _value = FirebaseRemoteConfig.DefaultInstance.GetValue(key).BooleanValue;
        }
        catch { }
#endif
        return _value;
    }

    public string GetVersion()
    {
#if UNITY_IOS
        return GetStringValue(KeyRemote.IosVersion, ValueRemote.VersionCurrent);
#else
        return GetStringValue(KeyRemote.AndroidVersion, ValueRemote.VersionCurrent);
#endif
    }

    public string GetVersionDescription()
    {
#if UNITY_IOS
        return GetStringValue(KeyRemote.IosVersionDescription, ValueRemote.IosVersionDescription);
#else
        return GetStringValue(KeyRemote.AndroidVersionDescription, ValueRemote.AndroidVersionDescription);
#endif
    }

    public bool GetForceUpdate()
    {
#if UNITY_IOS
        return GetBooleanValue(KeyRemote.IOSForceUpdate, ValueRemote.IosForceUpdate);
#else
        return GetBooleanValue(KeyRemote.AndroidForceUpdate, ValueRemote.AndroidForceUpdate);
#endif
    }

    public float GetTimeShowInter()
    {
        return (float)GetIntValue(KeyRemote.TimeShowInter, ValueRemote.TimeShowInter);
    }

    public int GetInterFromLevel()
    {
        return GetIntValue(KeyRemote.LevelShowInter, ValueRemote.LevelShowInter);
    }

    public bool isShowAOA()
    {
        return User.Instance[ItemID.PlayingLevel] >= GetIntValue(KeyRemote.ShowAOALevel, ValueRemote.ShowAOALevel);
    }

    public int WaitAOA()
    {
        return GetIntValue(KeyRemote.WaitAOA, ValueRemote.WaitAOA);
    }

    public string GetAppID()
    {
#if UNITY_IOS
	    return GetStringValue(KeyRemote.APPID_IOS, ValueRemote.APPID_IOS);
#elif UNITY_ANDROID
        return GetStringValue(KeyRemote.APPID_ANDROID, ValueRemote.APPID_ANDROID);
#endif
    }

    public bool GetShowSkill()
    {
        return GetBooleanValue(KeyRemote.ShowSkill, ValueRemote.ShowSkill);
        //return false;
    }

    public bool GetShowBonusLevel()
    {
        return GetBooleanValue(KeyRemote.ShowBonusLevel, ValueRemote.ShowBonusLevel);
    }

    public bool GetCheckInternet()
    {
        return GetBooleanValue(KeyRemote.CheckInternet, ValueRemote.CheckInternet);
    }

    public bool GetTestIAP()
    {
#if UNITY_IOS
        return GetBooleanValue(KeyRemote.TestIAP, ValueRemote.TestIAP);
#endif
        return false;
    }

    public bool GetSkipADS()
    {
#if UNITY_IOS
        return GetBooleanValue(KeyRemote.SkipADS, ValueRemote.SkipADS);
#endif
        return false;
    }

    public List<int> GetWaveShowMayBay()
    {
        string val = GetStringValue(KeyRemote.WaveShowMayBay, ValueRemote.WaveShowMayBay);
        if (string.IsNullOrEmpty(val))
        {
            return null;
        }
        return val.Split(",").Select(x => int.Parse(x)).ToList();
    }

    public int LevelWaveShowInter()
    {
        return GetIntValue(KeyRemote.LevelWaveShowInter, ValueRemote.LevelWaveShowInter);
    }

    public List<int> GetWaveShowInter()
    {
        string val = GetStringValue(KeyRemote.WaveShowInter, ValueRemote.WaveShowInter);
        if (string.IsNullOrEmpty(val))
        {
            return null;
        }
        return val.Split(",").Select(x => int.Parse(x)).ToList();
    }
}

public class KeyRemote
{
    public const string LevelShowInter = "level_show_inter";
    public const string AndroidVersion = "android_version";
    public const string IosVersion = "ios_version";
    public const string AndroidVersionDescription = "android_version_description";
    public const string IosVersionDescription = "ios_version_description";
    public const string AndroidForceUpdate = "android_force_update";
    public const string IOSForceUpdate = "ios_force_update";
    public const string TimeShowInter = "time_show_inter";
    public const string ShowAOALevel = "show_aoa_level";
    public const string APPID_IOS = "app_id";
    public const string APPID_ANDROID = "app_id_android";
    public const string WaitAOA = "wait_aoa";
    public const string ShowSkill = "show_skill";
    public const string ShowBonusLevel = "show_bonus_level";
    public const string CheckInternet = "check_internet";
    public const string TestIAP = "test_iap";
    public const string SkipADS = "skip_ads";
    public const string LevelWaveShowInter = "level_wave_show_inter";
    public const string WaveShowMayBay = "wave_show_maybay";
    public const string WaveShowInter = "wave_show_inter";
}
public class ValueRemote
{
    public const int LevelShowInter = 2; //Level bắt đầu show qc inter
    public const string VersionCurrent = "0";
    public const string AndroidVersionDescription = "Fix Bug";
    public const string IosVersionDescription = "Fix Bug";
    public const bool AndroidForceUpdate = false;
    public const bool IosForceUpdate = false;
    public const int TimeShowInter = 10;
    public const int ShowAOALevel = -1;
    public const int WaitAOA = 6;
    public const string APPID_IOS = "app_id";
    public const string APPID_ANDROID = "com.fusion.alphabet.ng";
    public const bool ShowSkill = true;
    public const bool ShowBonusLevel = true;
    public const bool CheckInternet = false;
    public const bool TestIAP = false;
    public const bool SkipADS = true;
    public const int LevelWaveShowInter = 3;
    public const string WaveShowMayBay = "2,4";
    public const string WaveShowInter = "";
}
