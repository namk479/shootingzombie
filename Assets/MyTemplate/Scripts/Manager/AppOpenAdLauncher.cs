using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using GoogleMobileAds.Ump.Api;
using MyGame;
using Thanh.Core;
using UnityEngine;

public class AppOpenAdLauncher : Singleton<AppOpenAdLauncher>
{

    public bool skipConsent;

    public bool CanRequestAds
    {
        get
        {
            return ConsentInformation.CanRequestAds();
        }
    }

    public void InitConsent()
    {
        var debugSettings = new ConsentDebugSettings
        {
            TestDeviceHashedIds =
        new List<string>
        {
            "4605aa3a-1a85-40a4-aa28-37015ff42289"
        }
        };
        skipConsent = false;
        // Create a ConsentRequestParameters object.
        ConsentRequestParameters request = new ConsentRequestParameters();

        // Check the current consent information status.
        ConsentInformation.Update(request, OnConsentInfoUpdated);
    }

    void OnConsentInfoUpdated(FormError consentError)
    {
        if (consentError != null)
        {
            // Handle the error.
            Debug.LogError(consentError.Message);
            skipConsent = true;
            ShowAOA();
            return;
        }

        // If the error is null, the consent information state was updated.
        // You are now ready to check if a form is available.
        ConsentForm.LoadAndShowConsentFormIfRequired((FormError formError) =>
        {
            if (formError != null)
            {
                // Consent gathering failed.
                Debug.LogError(consentError.Message);
                skipConsent = true;
                ShowAOA();
                return;
            }

            // Consent has been gathered.
            if (CanRequestAds)
            {
                ShowAOA();
            }

            skipConsent = true;
        });

    }
    private void OnAppStateChanged(AppState state)
    {
        // Display the app open ad when the app is foregrounded.
        Debug.Log("App State is " + state);
        if (state == AppState.Foreground && BaseScene.currentSceneName != SceneName.SplashScene)
        {
            if (!AdsManager.Instance.isRewardShowing && !AdsManager.Instance.isInterShowing)
            {
                Debug.Log("AOA thoat game");
                AppOpenAdManager.Instance.ShowAdIfAvailable();
            }
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        // Always unlisten to events when complete.
        AppStateEventNotifier.AppStateChanged -= OnAppStateChanged;
    }

    void ShowAOA()
    {
        AppStateEventNotifier.AppStateChanged += OnAppStateChanged;
        MobileAds.Initialize(status =>
        {
            AppOpenAdManager.Instance.LoadAOA();
            //HomeBanner.Instance.LoadBanner();
            Debug.Log("AOA open game");
        });
    }
}