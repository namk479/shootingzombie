using GoogleMobileAds.Api;
using MyGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeBanner : Singleton<HomeBanner>
{
    private const string testBannerID = "ca-app-pub-3940256099942544/6300978111";
    private bool useTestID = true;
    public string admobBannerID = "";

    BannerView _bannerView;

    #region Banner
    public void LoadBanner()
    {
        CreateBanner();

        ListenToBannerEvent();

        if(_bannerView == null)
        {
            CreateBanner();
        }

        var adRequest = new AdRequest.Builder().Build();
        adRequest.Extras.Add("collapsible", "bottom");
        adRequest.Extras.Add("collapsible_request_id", Guid.NewGuid().ToString());

        _bannerView.LoadAd(adRequest);
        Debug.Log("Loading banner ad ... ID : " + admobBannerID);
        HideBanner();
    }

    public void ShowBanner()
    {
        Debug.Log("Show Home Banner");

        if (_bannerView != null)
        {
            _bannerView.Show();
        }

        AdsManager.Instance.HideBanner();
    }

    public void HideBanner()
    {
        Debug.Log("Hide Home Banner");
        if (_bannerView != null)
        {
            _bannerView.Hide();
        }

        AdsManager.Instance.ShowBanner();
    }

    void CreateBanner()
    {       
        if (_bannerView != null)
        {
            DestroyBanner();
        }
        if (useTestID)
        {
            _bannerView = new BannerView(testBannerID, AdSize.Banner, AdPosition.Bottom);
        }
        else
        {
            _bannerView = new BannerView(admobBannerID, AdSize.Banner, AdPosition.Bottom);
        }
    }

    void ListenToBannerEvent()
    {
        // Raised when an ad is loaded into the banner view.
        _bannerView.OnBannerAdLoaded += () =>
        {
            Debug.Log("Banner view loaded an ad with response : "
                + _bannerView.GetResponseInfo());
        };
        // Raised when an ad fails to load into the banner view.
        _bannerView.OnBannerAdLoadFailed += (LoadAdError error) =>
        {
            Debug.LogError("Banner view failed to load an ad with error : "
                + error);
        };
        // Raised when the ad is estimated to have earned money.
        _bannerView.OnAdPaid += (AdValue adValue) =>
        {
            Debug.Log("Banner view paid " + adValue.Value + adValue.CurrencyCode);
        };
        // Raised when an impression is recorded for an ad.
        _bannerView.OnAdImpressionRecorded += () =>
        {
            Debug.Log("Banner view recorded an impression.");
        };
        // Raised when a click is recorded for an ad.
        _bannerView.OnAdClicked += () =>
        {
            Debug.Log("Banner view was clicked.");
        };
        // Raised when an ad opened full screen content.
        _bannerView.OnAdFullScreenContentOpened += () =>
        {
            Debug.Log("Banner view full screen content opened.");
        };
        // Raised when the ad closed full screen content.
        _bannerView.OnAdFullScreenContentClosed += () =>
        {
            Debug.Log("Banner view full screen content closed.");
        };
    }

    void DestroyBanner()
    {
        if(_bannerView != null)
        {
            Debug.Log("Destroy admob banner");
            _bannerView.Destroy();
            _bannerView = null;
        }
    }

    #endregion
}
