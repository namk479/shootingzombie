using System.Collections;
using System.Collections.Generic;
using Thanh.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupGDPR : Popup
{
    public TextMeshProUGUI txtThankPlaying;
    public Button btnAgree;
    public Button btnNoThank;

    public override void OnShow()
    {
        base.OnShow();

        txtThankPlaying.text = "Thanks for playing\n" + Application.productName;

        btnAgree.onClick.RemoveAllListeners();
        btnAgree.onClick.AddListener(() => OnAgree(true));

        btnNoThank.onClick.RemoveAllListeners();
        btnNoThank.onClick.AddListener(() => OnAgree(false));
    }

    void OnAgree(bool isAgree)
    {
        User.Instance[ItemID.IronSource_Consent] = isAgree ? 1 : 0;
        Close();
    }
}
