﻿//using AppsFlyerSDK;
using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseManager : Singleton<FirebaseManager>
{
    public bool firebaseInitialized { get; private set; }

    FirebaseRemote _remote = null;
    public FirebaseRemote remote
    {
        get
        {
            if (_remote == null)
            {
                _remote = new FirebaseRemote();
            }

            return _remote;
        }
    }

    private void Start()
    {
#if FIREBASE
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
#endif
    }
#if FIREBASE
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    private void InitializeFirebase()
    {
        Debug.Log("----------------Init FIREBASE------------------");
        FirebaseApp app = FirebaseApp.DefaultInstance;
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        FirebaseAnalytics.SetUserProperty(
          FirebaseAnalytics.UserPropertySignUpMethod,
          "Google");
        FirebaseAnalytics.SetUserId(SystemInfo.deviceUniqueIdentifier);
        FirebaseAnalytics.SetSessionTimeoutDuration(new TimeSpan(0, 30, 0));
        LogEvent(FirebaseAnalytics.EventLogin);
        firebaseInitialized = true;
        remote.FetchDataAsync();
    }
#endif

    #region Log Event

    public void LogEventAppflyer(string event_name)
    {
        Dictionary<string, string> paramas = new Dictionary<string, string>();
        //AppsFlyer.sendEvent(event_name, paramas);
    }

    public void LogEvent(string name, params Parameter[] parameters)
    {
#if FIREBASE
        if (!firebaseInitialized)
        {
            return;
        }

#if UNITY_EDITOR
        return;
#endif

        FirebaseAnalytics.LogEvent(name, parameters);
#endif
    }

    public void UserProperty(string name, string value, string location)
    {
#if FIREBASE
        if (!firebaseInitialized)
        {
            return;
        }

#if UNITY_EDITOR
        return;
#endif

        FirebaseAnalytics.SetUserProperty(name, value);
#endif
    }
    #endregion

    #region custom event
    public void rewarded_video_show(string location)
    {
        Debug.Log("___________ videoAds ___________");
        Debug.Log("location: " + location + " level " + User.Instance[ItemID.PlayingLevel]);

        //AppsFlyer.sendEvent(EventName.rewarded_video_show, null);

        LogEvent(EventName.rewarded_video_show,
            new Parameter[]
            {
                new Parameter(EventParameter.level, User.Instance[ItemID.PlayingLevel]),
                new Parameter(EventParameter.placement, location)
            });
        LogEventAppflyer("rewarded_video_show");
    }

    public void Interstitial_show(string location)
    {
        Debug.Log("___________ showInter ___________");

        //AppsFlyer.sendEvent(EventName.Interstitial_show, null);

        LogEvent(EventName.Interstitial_show,
            new Parameter[]
            {
                new Parameter(EventParameter.level, User.Instance[ItemID.PlayingLevel]),
                new Parameter(EventParameter.placement, location)
            });
        LogEventAppflyer("Interstitial_show");
    }

    public void iapPurchased(string sku)
    {
        Debug.Log("___________ iapPurchased ___________");
        Debug.Log("item_id: " + sku
            + "\nlevel: " + User.Instance[ItemID.PlayingLevel]);

        LogEvent(EventName.iapPurchased,
            new Parameter[]
            {
                new Parameter(EventParameter.item_id, sku),
                new Parameter(EventParameter.level, User.Instance[ItemID.PlayingLevel])
            });
        LogEventAppflyer("in_app_purchase");
    }

    public void earn_virtual_currency(ItemID item_id, int value, string location)
    {
        if (location == "None")
        {
            return;
        }

        Debug.Log("___________ earn_virtual_currency ___________");
        Debug.Log("itemsID: " + item_id.ToString());
        Debug.Log("value: " + value);
        Debug.Log("location: " + location);

        LogEvent(EventName.earn_virtual_currency,
            new Parameter[]
            {
                new Parameter(EventParameter.item_id, item_id.ToString()),
                new Parameter(EventParameter.value, value),
                new Parameter(EventParameter.placement, location)
            });
    }

    public void spend_virtual_currency(ItemID item_id, int value, string location)
    {
        Debug.Log("___________ spend_virtual_currency ___________");
        Debug.Log("item_id: " + item_id.ToString());
        Debug.Log("value: " + value);
        Debug.Log("location: " + location);

        LogEvent(EventName.spend_virtual_currency,
            new Parameter[]
            {
                new Parameter(EventParameter.item_id, item_id.ToString()),
                new Parameter(EventParameter.value, value),
                new Parameter(EventParameter.placement, location)
            });
    }

    public void winrate(int level, bool status, int firstWinCount)
    {
        Debug.Log("___________ winrate ___________" + level);
        Debug.Log("level: " + level);
        Debug.Log("status: " + status);
        Debug.Log("firstWinCount: " + firstWinCount);

        LogEvent(EventName.winrate,
            new Parameter[]
            {
                new Parameter(EventParameter.level, level.ToString()),
                new Parameter(EventParameter.status, status ? "win" : "lose"),
                new Parameter(EventParameter.firstWinCount, firstWinCount.ToString())
            });
    }

    public void tutorial_begin()
    {
        Debug.Log("___________ tutorial_begin ___________");

        LogEvent(EventName.tutorial_begin);
    }

    public void tutorial_complete()
    {
        Debug.Log("___________ tutorial_complete ___________");

        LogEvent(EventName.tutorial_complete);
    }

    public void level_start(int level)
    {
        Debug.Log("___________ start_level ___________");

        LogEvent(EventName.level_start,
            new Parameter[]
            {
                new Parameter(EventParameter.level, level)
            });
    }

    public void level_win(int level, int time)
    {
        Debug.Log("___________ level_win ___________");

        LogEvent(EventName.level_win,
            new Parameter[]
            {
                new Parameter(EventParameter.level, level),
                new Parameter(EventParameter.time, time)
            });
        LogEventAppflyer("level_win");
    }

    public void level_lose(int level, int time)
    {
        Debug.Log("___________ start_level ___________");

        LogEvent(EventName.level_lose,
            new Parameter[]
            {
                new Parameter(EventParameter.level, level),
                new Parameter(EventParameter.time, time)
            });
    }

    public void day_3_login()
    {
        LogEvent("day_3_login");
        LogEventAppflyer("day_3_login");
    }

    public void rv_reach_6_show()
    {
        LogEvent("rv_reach_6_show");
        LogEventAppflyer("rv_reach_6_show");
    }

    public void inter_reach_8_show()
    {
        LogEvent("inter_reach_8_show");
        LogEventAppflyer("inter_reach_8_show");
    }
    #endregion

    public class EventName
    {
        public const string rewarded_video_show = "rewarded_video_show";
        public const string iapPurchased = "iapPurchased";
        public const string earn_virtual_currency = "earn_virtual_currency";
        public const string spend_virtual_currency = "spend_virtual_currency";
        public const string winrate = "winrate";
        public const string hero_profile = "hero_profile";
        public const string tutorial_begin = "tutorial_begin";
        public const string tutorial_complete = "tutorial_complete";
        public const string Interstitial_show = "Interstitial_show";
        public const string level_start = "level_start";
        public const string level_win = "level_win";
        public const string level_lose = "level_lose";
        public const string level_reach = "level_reach";
    }

    public class EventParameter
    {
        public const string placement = "placement";
        public const string level = "level";
        public const string item_id = "item_id";
        public const string dayLogin = "dayLogin";
        public const string hero_id = "hero_id";
        public const string value = "value";
        public const string status = "status";
        public const string game_mode = "dif";
        public const string firstWinCount = "firstWinCount";
        public const string time = "time";
        public const string total_spent = "total_spent";
        public const string total_earn = "total_earn";
        public const string days_playing = "days_playing";
    }
}

public enum AdLocation
{
    None,
    ClaimX2,
    Spin,
    DailyGift,
    EndGame,
    BoosterUnchange,
    TruyBooster,
    BoosterVip,
    ExtraBooster,
    ResetBooster,
    AutoPlay,
    Revive,
    PlayCollection,
    UnlockHero,
    ShopPack,
    PlayModeBoss,
    PlayModeCollect,
    PlayModeEndless,
    FightBossNow,
    RemoveAds,
    BuyHero,
    BuySlotMerge,
}

public enum AdsType
{
    Video,
    Inter,
    Banner
}