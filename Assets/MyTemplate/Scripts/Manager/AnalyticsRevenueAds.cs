using com.adjust.sdk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsRevenueAds : MonoBehaviour
{

    public static void SendRevToAdjust(IronSourceImpressionData data)
    {
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
        adjustAdRevenue.setRevenue((double)data.revenue, "USD");
        // optional fields
        adjustAdRevenue.setAdRevenueNetwork(data.adNetwork);
        adjustAdRevenue.setAdRevenueUnit(data.adUnit);
        adjustAdRevenue.setAdRevenuePlacement(data.placement);
        // track Adjust ad revenue
        Adjust.trackAdRevenue(adjustAdRevenue);
    }

    public static void SendRevAOAToAdjust(double rev)
    {
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
        adjustAdRevenue.setRevenue(rev, "USD");
        // optional fields
        adjustAdRevenue.setAdRevenueNetwork("appopenads");
        //adjustAdRevenue.setAdRevenueUnit(data.adUnit);
        //adjustAdRevenue.setAdRevenuePlacement(data.placement);
        // track Adjust ad revenue
        Adjust.trackAdRevenue(adjustAdRevenue);
    }


}
