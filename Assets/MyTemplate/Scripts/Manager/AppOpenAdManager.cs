using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using Thanh.Core;

public class AppOpenAdManager
{
    private const string test_Key = "ca-app-pub-3940256099942544/9257395921";
    private bool isEnableTest = false;
#if UNITY_ANDROID
    private const string ID_ANDROID = "ca-app-pub-9778753195567892/9024251429";

#elif UNITY_IOS
    private const string ID_IOS = "";
#else
    private const string ID_ANDROID = "ca-app-pub-9778753195567892/9024251429";
#endif

    private static AppOpenAdManager instance;

    private AppOpenAd ad;

    private bool isShowingAd = false;

    private bool isRequesting = false;

    private bool isFirstShow = false;

    //public static bool ResumeFromAds = false;

    public static AppOpenAdManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new AppOpenAdManager();
            }

            return instance;
        }
    }

    private bool IsAdAvailable => ad != null;

    int numReload = 3;
    public void LoadAOA()
    {
        // destroy old instance.
        numReload -=1;
        DestroyAppOpenAd();
        isRequesting = true;
        string id = ID_ANDROID;
        if (isEnableTest)
        {
            id = test_Key;
        }

        Debug.Log($"Start request Open App Ads ID: {id}");
        //89DDBC4FCF390B071A86E0668CD8A211
        AdRequest request = new AdRequest();
        try
        {
            AppOpenAd.Load(id, request, ((appOpenAd, error) =>
            {
                if (error != null || appOpenAd == null)
                {
                    // Handle the error.
                    Debug.LogFormat(
                        $"Failed to load AOA id: {id}. Reason: {error.GetMessage()}");
                    isRequesting = false;

                    if(numReload > 0)
                    {
                        LoadAOA();
                    }
                    return;
                }

                // App open ad is loaded.
                numReload = 3;
                ad = appOpenAd;
                isRequesting = false;
                if (!isFirstShow)
                {
                    if (BaseScene.currentSceneName == SceneName.SplashScene)
                    {
                        Debug.Log("First open show aoa");
                        ShowAdIfAvailable();
                    }
                    isFirstShow = true;
                }
            }));
        }
        catch (Exception e)
        {
            isRequesting = false;
        }
    }


    public void ShowAdIfAvailable()
    {
        if (isShowingAd)
        {
            return;
        }

        if (!IsAdAvailable && !isRequesting)
        {
            LoadAOA();
            return;
        }

        if (ad == null)
        {
            return;
        }

        ad.OnAdFullScreenContentClosed += HandleAdDidDismissFullScreenContent;
        ad.OnAdFullScreenContentFailed += HandleAdFailedToPresentFullScreenContent;
        ad.OnAdFullScreenContentOpened += HandleAdDidPresentFullScreenContent;
        ad.OnAdImpressionRecorded += HandleAdDidRecordImpression;
        ad.OnAdClicked += HandleAdClicked;
        ad.OnAdPaid += HandlePaidEvent;
        if (ad.CanShowAd())
            ad.Show();
        else
        {
            LoadAOA();
        }
    }

    public void DestroyAppOpenAd()
    {
        if (ad != null)
        {
            ad.Destroy();
            ad = null;
        }
    }

    private void HandleAdDidDismissFullScreenContent()
    {
        //Debug.Log("Closed app open ad");
        isShowingAd = false;
        LoadAOA();
    }

    private void HandleAdFailedToPresentFullScreenContent(AdError error)
    {
        Debug.LogError("App open ad failed to open full screen content " +
                       "with error : " + error);
        LoadAOA();
    }

    private void HandleAdDidPresentFullScreenContent()
    {
        //Debug.Log("Displayed app open ad");
        isShowingAd = true;
    }

    private void HandleAdDidRecordImpression()
    {
        //Debug.Log("Recorded ad impression");
    }

    private void HandleAdClicked()
    {
        //Debug.Log("Ad Clicked");
    }

    private void HandlePaidEvent(AdValue adValue)
    {
        Debug.Log(String.Format("App open ad paid {0} {1}.",
            adValue.Value,
            adValue.CurrencyCode));
        var rev = adValue.Value / 1000000f;
        AnalyticsRevenueAds.SendRevAOAToAdjust(rev);
    }
}