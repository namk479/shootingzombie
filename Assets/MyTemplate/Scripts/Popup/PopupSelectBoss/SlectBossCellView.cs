using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Thanh.Core;
using Spine.Unity;
using DanielLochner.Assets.SimpleScrollSnap;

public class SlectBossCellView : MonoBehaviour
{
    [SerializeField]
    private Image bg;
    [SerializeField]
    private Image imgBgLook;
    [SerializeField]
    private TMP_Text Name;
    [SerializeField]
    private LevelBossSetUp LevelBos;
    [SerializeField]
    public Button clickBoss;
    [SerializeField]
    private Button clickReview;
    public int level;
    public SkeletonGraphic SkeletonGraphic;/*{ get; private set; }*/
    public Mode mode;
    public SimpleScrollSnap scrollSnap;
    public Material materialGray;




    public bool isUnlocked;


    private void OnEnable()
    {
        Invoke("CheckingUnlock",0.1f);
    }

    public void SetData(LevelBossSetUp levelBossSetUp, SimpleScrollSnap scrollSnap)
    {
        mode.nameMode = NameMode.BossMode;
        LevelBos = levelBossSetUp;
        this.scrollSnap = scrollSnap;
        levelBossSetUp.ChangeSkeletonDataAsset(SkeletonGraphic);
        SkeletonGraphic.Skeleton.SetSkin(levelBossSetUp.skin);


        //fix scale by boss has spine diffirent
        SkeletonGraphic.gameObject.transform.localScale = levelBossSetUp.scale;
        SkeletonGraphic.gameObject.transform.localPosition = levelBossSetUp.pos;

        Name.text = LevelBos.Name.ToString() ;
        //bg.sprite = LevelBos.avt;
        clickBoss.onClick.RemoveListener(OnClickBoss);
        clickBoss.onClick.AddListener(OnClickBoss);
        clickReview.onClick.RemoveListener(OnClickReview);
        clickReview.onClick.AddListener(OnClickReview);

        CheckingUnlock();
    }
    
    public void OnClickBoss()
    {
        if (isUnlocked)
        {
            GlobalData.gameMode = GameMode.BossWorld;
            GlobalData.instance.levelToPlay = LevelBos.level;
            GlobalData.instance.bossToFight = LevelBos.bossId;
            GlobalData.instance.levelUnlockNextBoss = LevelBos.levelUnlock;
            GlobalData.instance.LevelBossSetUp = LevelBos;
            GameEvent.OnMoveToPlay.Invoke();
        }
        else
        {
            GlobalData.instance.levelToPlay = LevelBos.level;
            GlobalData.instance.bossToFight = LevelBos.bossId;
            PopupManager.Instance.OpenPopup<PopupOpenReview>(PopupID.PopupReviewBoss, (pop) =>
            {
                pop.txtGold.text = "+" + (GameManager.Instance.rewardBoss.rewardConfig[level - 1].gold).ToString();
                pop.txtTiket.text = "+" + (GameManager.Instance.rewardBoss.rewardConfig[level - 1].ticket).ToString();
                pop.SetData(mode, this.LevelBos, isUnlocked,false);
            });
        }
    }

    public void OnClickReview()
    {
        GlobalData.instance.levelToPlay = LevelBos.level;
        GlobalData.instance.bossToFight = LevelBos.bossId;
        PopupManager.Instance.OpenPopup<PopupOpenReview>(PopupID.PopupReviewBoss, (pop) =>
        {
            if (User.Instance[LevelBos.bossId] == 2 || User.Instance[LevelBos.isPass] == 1)
            {
                pop.txtGold.text = "+" + ((GameManager.Instance.rewardBoss.rewardConfig[level - 1].gold) / 10).ToString();
                pop.txtTiket.text = "+ 0";// + (GameManager.Instance.rewardBoss.rewardConfig[level - 1].ticket).ToString();
              
            }
            else
            {
                pop.txtGold.text = "+" + (GameManager.Instance.rewardBoss.rewardConfig[level - 1].gold).ToString();
                pop.txtTiket.text = "+" + (GameManager.Instance.rewardBoss.rewardConfig[level - 1].ticket).ToString();
            }

            pop.SetData(mode, this.LevelBos,isUnlocked)  ;
        });

    }

 
    public void CheckingUnlock()
    {
        if(LevelBos.bossId == ItemID.bossDeath)
        {
            imgBgLook.gameObject.SetActive(false);
            SkeletonGraphic.material = null;
            isUnlocked = true;

            if (User.Instance[LevelBos.bossId] == 0)
            {
                User.Instance[LevelBos.bossId] = 1;
            }
            return;
        }



        if(LevelBos.levelUnlock <= User.Instance[ItemID.PlayingLevel] && User.Instance[LevelBos.preBoss] == 2)
        {
            imgBgLook.gameObject.SetActive(false);
            SkeletonGraphic.material = null;
            isUnlocked = true;

            if(User.Instance[LevelBos.bossId] == 0)
            {
                User.Instance[LevelBos.bossId] = 1;
            }
        }
        else
        {
            isUnlocked = false;
            imgBgLook.gameObject.SetActive(true);
            SkeletonGraphic.material = materialGray;
        }
    }
}
