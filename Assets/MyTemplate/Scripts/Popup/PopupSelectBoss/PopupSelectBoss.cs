﻿using Thanh.Core;
using UnityEngine.UI;

using UnityEngine;

public class PopupSelectBoss : Popup
{
    public Button btnCancel;
    public Button hammerShopPopupBoss;
    public Button coinShopPopupBoss;

    public void ClickHammerShop()
    {
/*
        PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => {
            pop.TypeInventory = TypeShop.ShopHammer;
            //pop.Setup();

        });*/
    }

    public void ClickCoinShop()
    {/*
        PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => {
            pop.TypeInventory = TypeShop.ShopGold;
           // pop.Setup();
        });*/
    }

    private void OnEnable()
    {
        GameEvent.OnMoveToPlay.RemoveListener(OnMoveToPlay);
        GameEvent.OnMoveToPlay.AddListener(OnMoveToPlay);

        btnCancel.onClick.RemoveListener(OnClickCancel);
        btnCancel.onClick.AddListener(OnClickCancel);

        hammerShopPopupBoss.onClick.RemoveListener(ClickHammerShop);
        hammerShopPopupBoss.onClick.AddListener(ClickHammerShop);

        coinShopPopupBoss.onClick.RemoveListener(ClickCoinShop);
        coinShopPopupBoss.onClick.AddListener(ClickCoinShop);
    }

    public void OnMoveToPlay()
    {
        this.Close();
    }
    public void OnClickCancel()
    {
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode);
        this.Close();
    }

}
