﻿using System.Collections.Generic;
using UnityEngine;
using DanielLochner.Assets.SimpleScrollSnap;
public class PopupSelectBossSwipe : MonoBehaviour
{
    [SerializeField]
    private GameObject scrollbar;
    public List<GameObject> btnCellBoss=  new List<GameObject>();
    public SimpleScrollSnap scrollSnap;
    [SerializeField] private Transform content;
    public GameObject buttonPrefab;

    public void Awake()
    {
        spawnCell();
    }

    public void Start()
    {
        Invoke("OnCenterSnap", 0.1f);
    }
   
    public void spawnCell()
    {
        foreach (LevelBossSetUp levelBossSetUp in LevelConfig.instance.levelBossSetUps)
        {
            GameObject buttonInstance = Instantiate(buttonPrefab, content);
            buttonInstance.GetComponent<SlectBossCellView>().SetData(levelBossSetUp, scrollSnap);
            buttonInstance.GetComponent<SlectBossCellView>().level = levelBossSetUp.level;
            buttonInstance.name = levelBossSetUp.bossName.ToString();
            btnCellBoss.Add(buttonInstance);
        }
    }

    public void OnCenterSnap()
    {
        foreach(GameObject obj in btnCellBoss)
        {
            if(obj.name == scrollSnap.Panels[scrollSnap.CenteredPanel].name)
            {
                obj.GetComponent<SlectBossCellView>().clickBoss.interactable = true;
            }
            else
            {
                obj.GetComponent<SlectBossCellView>().clickBoss.interactable = false;
            }
        }
    }
}
