using Spine.Unity;
using System.Collections.Generic;
using Thanh.Core;
using TMPro;
using UnityEngine;

public class PopupOpenReview : Popup
{
    public TMP_Text txtboss;
    public TMP_Text txtGold;
    public TMP_Text txtTiket;
    public TMP_Text txtReview;
    public SkeletonGraphic SkinIcon;
    public Mode mode;


    public GameObject playByAds;
    public GameObject[] reward;

    public LevelBossSetUp levelBoss;

    public void SetData(Mode mode, LevelBossSetUp levelBossSetUp, bool isUnlocked, bool isReview = true)
    {
        this.mode = mode;
        this.levelBoss = levelBossSetUp;
        levelBossSetUp.ChangeSkeletonDataAsset(SkinIcon);
        SkinIcon.Skeleton.SetSkin(levelBossSetUp.skin);

        //fix scale by boss has spine diffirent
        SkinIcon.gameObject.transform.localScale = levelBossSetUp.scaleInReview;
        SkinIcon.gameObject.transform.localPosition = levelBossSetUp.posInReview;


        if (mode.nameMode == NameMode.BossMode)
        {
            txtGold.gameObject.SetActive(true);
            txtTiket.gameObject.SetActive(true);
            AudioManager.instance.Play("BtnClick");
            txtReview.text = "Kill Boss to receive items";
            GlobalData.gameMode = GameMode.BossWorld;
            User.Instance.Save();
        }




        //neu chua mo
        if (!isUnlocked)
        {
            if (!isReview)
            {
                playByAds.SetActive(true);
                txtReview.text = "Clear Stage " + (levelBossSetUp.levelUnlock).ToString() + " to unlock";

                if (reward.Length > 0)
                {
                    foreach (GameObject game in reward)
                    {
                        game.SetActive(false);
                    }
                }
            }
            else
            {
                playByAds.SetActive(false);
                txtReview.text = "Kill Boss to receive items";
                if (reward.Length > 0)
                {
                    foreach (GameObject game in reward)
                    {
                        if (User.Instance[levelBoss.bossId] == 2 || User.Instance[levelBoss.isPass] == 1)
                        {
                            reward[0].gameObject.SetActive(false);
                            reward[1].gameObject.SetActive(true);
                        }
                        else
                        {
                            game.SetActive(true);
                        }
                    }
                }
            }
        }
        else
        {
            playByAds.SetActive(false);
            txtReview.text = "Kill Boss to receive items";
            if (reward.Length > 0)
            {
                foreach (GameObject game in reward)
                {
                    if (User.Instance[levelBoss.bossId] == 2 || User.Instance[levelBoss.isPass] == 1)
                    {
                        reward[0].gameObject.SetActive(false);
                        reward[1].gameObject.SetActive(true);
                    }
                    else
                    {
                        game.SetActive(true);
                    }
                }
            }
        }
    }


    public void PlayByAds()
    {
        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
        {
            if (isSuccess)
            {
                GlobalData.gameMode = GameMode.BossWorld;
                GlobalData.instance.levelToPlay = this.levelBoss.level;
                GlobalData.instance.bossToFight = this.levelBoss.bossId;
                GlobalData.instance.levelUnlockNextBoss = this.levelBoss.levelUnlock;
                GlobalData.instance.LevelBossSetUp = this.levelBoss;
                GameEvent.OnMoveToPlay.Invoke();
                this.Close();
            }

        }, AdLocation.FightBossNow);
    }
}
