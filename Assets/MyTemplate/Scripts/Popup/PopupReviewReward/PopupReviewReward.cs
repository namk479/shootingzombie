using AA_Game;
using Gamelogic.Extensions.Algorithms;
using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using Thanh.Core;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Yurowm.GameCore;

public class PopupReviewReward : Popup
{
    public Button ClickPlay;
    public List<CellReview> Items = new List<CellReview>();
    public List<GameObject> iconGameObject = new List<GameObject>();
    public int index;
    public Button GameojctLock;
    public GameObject TxtWarning;


    public TMP_Text txtReview;
    public TMP_Text txtboss;
    public TMP_Text txtPlay;
    public Mode mode;
    public Image iconADs;

    public void ExitPopup()
    {
        base.Close();

    }
    public void OnEnable()
    {

    }

    public void SetData(Mode mode)
    {
        this.mode = mode;
        /*   if(mode.isAds == true )
           {
               iconADs.gameObject.SetActive(true);
           }
           else if(mode.isAds == false)
           {
               iconADs.gameObject.SetActive(false);
           }*/

        if (mode.nameMode == NameMode.EndlessMode)
        {

            Items[0].gameObject.SetActive(false);
            Items[1].gameObject.SetActive(true);
            Items[2].gameObject.SetActive(false);
            Items[3].gameObject.SetActive(true);
            AudioManager.instance.Play("BtnClick");
            GlobalData.gameMode = GameMode.Endless;
            User.Instance.Save();
            txtReview.gameObject.SetActive(true);
            txtReview.text = "Kill zombies to receive items";
            ClickPlay.gameObject.SetActive(true);
            txtboss.gameObject.SetActive(false);
            iconGameObject[0].gameObject.SetActive(true);
            iconADs.gameObject.SetActive(false);
        }
        if (mode.nameMode == NameMode.BossMode)
        {
            Items[0].gameObject.SetActive(true);
            Items[1].gameObject.SetActive(false);
            Items[2].gameObject.SetActive(true);
            Items[3].gameObject.SetActive(false);
            iconGameObject[2].gameObject.SetActive(true);
            ClickPlay.gameObject.SetActive(false);
            AudioManager.instance.Play("BtnClick");
            txtReview.text = "Kill Boss to receive items";
            GlobalData.gameMode = GameMode.BossWorld;
            User.Instance.Save();
            iconADs.gameObject.SetActive(false);
        }
        if (mode.nameMode == NameMode.CollectionMode)
        {

            AudioManager.instance.Play("BtnClick");
            GlobalData.gameMode = GameMode.CollectFuel;
            User.Instance.Save();
            iconADs.gameObject.SetActive(true);
            Items[0].gameObject.SetActive(true);
            Items[1].gameObject.SetActive(false);
            Items[2].gameObject.SetActive(false);
            Items[3].gameObject.SetActive(false);
            iconGameObject[1].gameObject.SetActive(true);
            txtReview.text = "Kill zombies to earn gold";
            ClickPlay.gameObject.SetActive(true);

            if (User.Instance[ItemID.AdsCollectMode] >= 10)
            {
                iconADs.sprite = SpriteManager.Instance.GetSprite(ItemID.Lock);
                txtPlay.text = "LOCK";
            }
            else
            {
                iconADs.gameObject.SetActive(true);
            }

        }
        if (mode.nameMode == NameMode.CampaignMode)
        {
            AudioManager.instance.Play("BtnClick");
            GlobalData.gameMode = GameMode.Normal;
            User.Instance.Save();
        }
        ClickPlay.onClick.RemoveListener(ClickBtnPlay);
        ClickPlay.onClick.AddListener(ClickBtnPlay);

    }
    public override void Close()
    {
        base.Close();
        for (int i = 0; i < iconGameObject.Count; i++)
        {
            iconGameObject[i].gameObject.SetActive(false);

        }

    }


    public void ClickBtnPlay()
    {
        if (mode.nameMode == NameMode.CollectionMode)
        {
            //chua het 10 lan ads
            if (User.Instance[ItemID.AdsCollectMode] < 10)
            {
                BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
            {
                if (isSuccess)
                {
                    // SHOW ADS
                    ClickPlayRevew();
                }

            }, AdLocation.PlayCollection);

            }
            else
            {
                //da het 10 lan ads
                //thong bao da het 10 lan ads
                //  isSuccess = false;
                StartCoroutine(clicbtnLock());
                Debug.Log("qua 10 lan choi");
            }

        }
        else
        {
            ClickPlayRevew();
        }
    }


    public void ClickPlayRevew()
    {
        if (mode.nameMode == NameMode.CollectionMode)
        {
            User.Instance[ItemID.AdsCollectMode] += 1;
        }
        GameEvent.OnMoveToPlay.Invoke();
        this.Close();
    }
    void BtnLock()
    {
        StartCoroutine(clicbtnLock());
    }
    IEnumerator clicbtnLock()
    {
        TxtWarning.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        TxtWarning.gameObject.SetActive(false);
    }
}
