// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("NsXV785AesEBVj73/WcSC4uq8GgFSVvGSRXF3y+DuPF6PPY0tBzwxNGjWw4UMzg+05Qh+MUSLLYceSWUtZNzpF7iV989sF8jY6Z4glmroU1l5ujn12Xm7eVl5ubnIa5DxhUZwmqBWmDSTKgKDqpFFuqnpdzKLrlTko3i+aYUic9XLYi2Cxi/CQz1Cpvqe4tj+JKxzEWVnlA0ZJqVVl5JVA18llWXKtP0JxqyG3CF2tAzxpUe12Xmxdfq4e7NYa9hEOrm5ubi5+Q6f4MW7ctIKfoiC2SMBJN/eBnaGeg/3yHc/9c/ymvnx3zxOxWKRgdin/ACMu/nX//E0Bq81AqP28P+WcjfY0QDe3sy2X9IQALOO8IZyD4YytIs3nQftIngDuXk5ufm");
        private static int[] order = new int[] { 12,3,7,4,4,13,9,13,13,12,13,11,13,13,14 };
        private static int key = 231;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
