// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("KyU3N8kyMwY1NzfJBjgwNWMrOTfynz2F+VQ5zYKKwe2yaB7dcTvH2+HSTp6ZnluR9+xeG1D6+R89ZhrV3YF2twuxC0Zn5/Jru5PqSVxuLOg7jQ6mXTDyyjvTo2b7p/6ME8Ju/36wwTs3PzcgPmVCWURTfV9CBrQ3GkPQeock5cvVKWg7S45u4ZZK0kk6PmVCWURTfV9CBz0GPzA1YzIwJb2aw6mGPw6VWE39EGY8LNe/50RANga0Nzw0tDc3Nu1JpgoaRTntoRAyNTo+ZUJZRFN9X0IHJwY5MDVjMmVCWURTfV9CBighOwQGBgIGBwcB9M3QaST3NDU3NjeVDQYPBjkwNWM3yTIyNTQ0sgYgMDVjKxM3N8kyOlWdoawd9Rzu5oTX42w7Dy1ZUR6fNGNlByEGIzA/HLB+sME7Nz83ID5WQAFk2smaL2s6Ixm8OcO//uEdKwZpBycGOTA1YzI1Oj5lQllEU31fBrQ1Qga0NGqWNTQ3NDQ3NwY7MD+vzYW8Y6nZKx6OaRznRakDcQGNcLrXR+18c8UbL6A1YLeQcthDQW1lJLXgsU5br4tohTzWrvjA3PW4W199pY08zpcXCP5X7xL+aClDCOyl7T0GPzA1YzIwJTRjZQchBiMwPxyw+U6aa4wIaWYdah+66/ThHCHpFIKL9uArdqhhjw6mQUBk5J59RliOUQMEBQNsITsCBgYFBAEHAQMEBQNss05++PbzJCdcOTqYGTP8WUxSSRZRE4+dWgVTm+70rE5+Od/69xE78AY8MD4dMDczMzE1NQY7MD8csH6wHLB+sME7Nzc9MzYGaQcnBjkwNWM8Oj5lQllEU31fQgcnBjkwNWMyPdFZ1i3QO177E1j1QD7DfD2YTEIKByynto+J0zr9Co04clQQzDIfgH8UBjswPxywfrDBOzc3NzM2NbQ3OSrZP75OU/i3xLtE0TEsWBAkwQSHPQ96/XlY9oVpgaOMj55rfEKd2VnBOzc3PTM2NbQ3NzaENtYKx97fVOVAJsTGX9r7VK9Hz1a8sn4O8ou9BycGOTA1YzI9Oj5lQllEU31fQgd3k0g/u4wLC/tGYe7b2ePsQf6lMvlSClu2bwW6/AAwHCctu3/4PslQQgcnBjkwNWMyPDo+ZUJZRFN9X0L2cR593Wz3HT9gGpOcPzVNBAfq3ijaHaczkQt5");
        private static int[] order = new int[] { 28,18,26,42,23,12,39,23,19,14,26,39,27,29,40,27,43,19,28,33,24,41,27,39,25,32,39,38,35,30,35,42,33,43,35,38,43,42,38,39,41,43,43,43,44 };
        private static int key = 54;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
