using UnityEngine;

public class NotiDaiLy : MonoBehaviour
{
    public GameObject noti;
    public float timeReset;

    public void Update()
    {
        timeReset += Time.deltaTime;
        if(timeReset >= 1)
        {
            timeReset = 0;
            CheckingNotiQuest();
        }
    }

    public void CheckingNotiQuest()
    {
        bool isHaveClaim = false;
        foreach (Quest quest in User.Instance.DailyQuest())
        {
            if (quest.questID != ItemID.PlayEndless15Min && quest.questID != ItemID.PlayEndless30Min && quest.questID != ItemID.PlayEndless60Min)
            {
                if (User.Instance[quest.questID] >= quest.questRequier)
                {
                    quest.isSuccess = true;
                }
            }
            else
            {
                if ((int)((User.Instance[quest.questID]) / 60) >= quest.questRequier)
                {
                    quest.isSuccess = true;
                }
            }

            if (quest.isSuccess && quest.isClaimed == false)
            {
                isHaveClaim = true;
            }
        }

        foreach (Quest quest in User.Instance.DailyQuestWeek())
        {
            if (quest.questID != ItemID.PlayEndless15Min && quest.questID != ItemID.PlayEndless30Min && quest.questID != ItemID.PlayEndless60Min)
            {
                if (User.Instance[quest.questID] >= quest.questRequier)
                {
                    quest.isSuccess = true;
                }
            }
            else
            {
                if ((int)((User.Instance[quest.questID]) / 60) >= quest.questRequier)
                {
                    quest.isSuccess = true;
                }
            }


            if (quest.isSuccess && quest.isClaimed == false)
            {
                isHaveClaim = true;
            }
        }

        noti.SetActive(isHaveClaim);
    }
}
