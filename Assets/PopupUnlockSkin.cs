using Spine;
using Spine.Unity;
using System.Collections.Generic;
using System.Linq;
using Thanh.Core;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Yurowm.GameCore;
using DG.Tweening;

public class PopupUnlockSkin : Popup
{
    //public SkeletonGraphic skeletonGraphic;
    //public Transform halo;
    //private FxItem fx;
    //[SerializeField] private TextMeshProUGUI txtdamagePer, txtDamageCurrent;
    //[SerializeField] private TextMeshProUGUI txtHpPer, txtHpCurrent;


    public bool isNewChar;
    public TMP_Text level;
    public TMP_Text damage;
    public TMP_Text crit;
    public TMP_Text textDamageUp;
    public TMP_Text textCritUp;
    public GameObject[] stars;
    public BotUI botUI;
    public TMP_Text nameHero;
    public bool isCharMax;
    //public SkeletonDataAsset[] skeletonDataAsset; // PLAYER----->-BOT1----->-BOT2----->-BOT3
    //public SkeletonGraphic skeletonGraphic;

    public Slider levelSlider;
    public Slider atkSlider;
    public Slider critSlider;

    public UserBot userBot;


    public override void OnShow()
    {
        base.OnShow();
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f);
    }

    public void SetStar(int currentStar)
    {
        foreach (GameObject star in stars)
        {
            star.SetActive(false);
            for (int i = 0; i < currentStar; i++)
            {
                stars[i].SetActive(true);
            }
        }
    }


    public void SetData(ItemID itemID,bool isCarMerge)
    {
        //ShowHeroGraphic(itemID);
        //int hpHero = MergeCharacterManager.Instance.GetHP(itemID);
        //int damageHero = MergeCharacterManager.Instance.GetDamager(itemID);
        //txtDamageCurrent.text = $"{damageHero}";
        //txtHpCurrent.text = $"{hpHero}";
        //txtdamagePer.text = $"{damageHero / 2}";
        //txtHpPer.text = $"{hpHero / 2}";
        //User.Instance[itemID] = 1;
        //if (fx != null)
        //{
        //    fx.Kill();
        //}

        //PlayeFx();




        User.Instance[itemID] = 1;
        SetUp(itemID,isCarMerge);

    }

    void PlayeFx()
    {
        //fx = ContentPoolable.Emit(ItemID.fxUnlock) as FxItem;
        //fx.transform.parent = halo.transform;
        //fx.transform.localScale = Vector3.one * 10;
        //fx.transform.localPosition = Vector3.zero;
    }

    public void ShowHeroGraphic(ItemID _idSkin)
    {
        //string animCurrent = GetAllDance().GetRandom();
        //skeletonGraphic.Initialize(false);
        //ItemID idWeapon = MergeCharacterManager.GetNameWeapon(_idSkin);
        //var customSkin = new Skin("CustomSkin");
        //List<string> skins = skeletonGraphic.SkeletonDataAsset.GetSkeletonData(true).Skins.Items.Select(x => x.ToString()).ToList();
        //string current = $"{_idSkin.ToString()}";
        ////if (FirebaseManager.Instance.remote.GetVerCompact())
        ////{
        ////    current = $"{current}_mask";
        ////}

        //string currentFind = null;
        //currentFind = skins.Find(x => x.Contains(current));
        //if (currentFind == null)
        //{
        //    currentFind = skins.Find(x => x.Contains(_idSkin.ToString()));
        //}

        //var charSkin = skeletonGraphic.Skeleton.Data.FindSkin(currentFind);
        //customSkin.AddSkin(charSkin);
        ////var weaponSkin = skeletonGraphic.Skeleton.Data.FindSkin(skins.Find(x => x.Contains(User.Instance.usingWeapon.ToString())));
        //var weaponSkin = skeletonGraphic.Skeleton.Data.FindSkin(skins.Find(x => x.Contains(idWeapon.ToString())));
        //customSkin.AddSkin(weaponSkin);
        //skeletonGraphic.Skeleton.SetSkin(customSkin);
        //skeletonGraphic.Skeleton.SetSlotsToSetupPose();
        //skeletonGraphic.AnimationState.SetAnimation(0, animCurrent, true);
    }

    //private List<string> GetAllDance()
    //{
    //    List<string> list = new List<string>();
    //    Spine.AnimationState state = skeletonGraphic.AnimationState;
    //    foreach (Spine.Animation skin in state.Data.SkeletonData.Animations.Where(x => x.Name.StartsWith("dance")))
    //    {
    //        list.Add(skin.Name);
    //    }

    //    list.Remove("dance_handgun");
    //    return list;
    //}



    public void SetUp(ItemID itemID,bool isCarMege)
    {
        if (isCarMege)
        {
            UserCar userCar = null;
            userCar = new UserCar(Resources.Load<CarLevelData>("Scriptable/CarLevelData").carLevelDatas.First(x => x.skin == itemID));
            User.Instance.Car = userCar;
            User.Instance.Save();
            GameEvent.OnCarLevelUp.Invoke();
        }
        else
        {
            UserBot userBot = null;
            if (MergeCharacterManager.Instance.IsHero(itemID))
            {
                User.Instance.UserPlayers1.First(x => x.isUsing).isUsing = false;
                userBot = User.Instance.UserPlayers1.First(x => x.id == itemID);
                User.Instance.UserPlayerUsing = userBot;
            }
            if (MergeCharacterManager.Instance.IsTypePet(itemID))
            {
                User.Instance.UserBots1.First(x => x.isUsing).isUsing = false;
                userBot = User.Instance.UserBots1.First(x => x.id == itemID);
                User.Instance.UserBot1Using = userBot;
            }
            if (MergeCharacterManager.Instance.IsTypePet2(itemID))
            {
                if (User.Instance.Car.slot >= 2)
                {
                    User.Instance.UserBots2.First(x => x.isUsing).isUsing = false;
                }
                userBot = User.Instance.UserBots2.First(x => x.id == itemID);
                User.Instance.UserBot2Using = userBot;
            }
            if (MergeCharacterManager.Instance.IsTypePet3(itemID))
            {
                if (User.Instance.Car.slot >= 3)
                {
                    User.Instance.UserBots3.First(x => x.isUsing).isUsing = false;
                }
                userBot = User.Instance.UserBots3.First(x => x.id == itemID);
                User.Instance.UserBot3Using = userBot;
            }
            userBot.isUsing = true;
            this.userBot = userBot;
            this.userBot.isUsing = true;
            damage.text = userBot.damage.ToString();
            crit.text = userBot.crit.ToString();
            SetStar(userBot.star);
            botUI.SetSkin(this.userBot);
            nameHero.text = userBot.name.ToUpper();
            User.Instance.Save();
            GameEvent.OnEquipSkin.Invoke(userBot);
        }
    }
}