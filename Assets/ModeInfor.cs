using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Spine;
public class ModeInfor : MonoBehaviour
{
    public Mode mode;
    public Button btnPlay;
    // public Button btnPlayCampaign;
    public Image imglock;
    public TMP_Text txtTypeLock;
    public Image  imgNotUnlock;
    public GameObject txtWarning;


    public void OnEnable()
    {
        btnPlay.onClick.RemoveListener(PlayButton);
        btnPlay.onClick.AddListener(PlayButton);

        if (User.Instance[ItemID.PlayingLevel]< 6 && mode.nameMode != NameMode.CampaignMode)
        {
            //chua mo khoa
            imglock.gameObject.SetActive(true);
            txtTypeLock.text = "UNLOCK";
        }
        else
        {
            
            imglock.gameObject.SetActive(false);
            imgNotUnlock .material = null;
           txtTypeLock.text = "PLAY";
        }

    }
    public void PlayButton()
    {
        if (User.Instance[ItemID.PlayingLevel] < 6 && mode.nameMode != NameMode.CampaignMode)
        {
            StartCoroutine(clicbtnLock());
        }
        else 
        {
            if (mode.nameMode == NameMode.BossMode)
            {
                PopupManager.Instance.OpenPopup<PopupSelectBoss>(PopupID.PopupSelectBoss);
            }
            if (mode.nameMode == NameMode.CampaignMode)
            {
                AudioManager.instance.Play("BtnClick");
                GlobalData.instance.levelToPlay = User.Instance[ItemID.PlayingLevel];
                GlobalData.gameMode = GameMode.Normal;
                Debug.Log("Playinglevel---"+User.Instance[ItemID.PlayingLevel]);
                GameEvent.OnMoveToPlay.Invoke();
                User.Instance.Save();
            }
            else if (mode.nameMode != NameMode.BossMode && mode.nameMode != NameMode.CampaignMode)
            {
                PopupManager.Instance.OpenPopup<PopupReviewReward>(PopupID.PopupReviewReward, (pop) =>
                {
                    transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);

                    pop.SetData(mode);
                });
            }

        }
        IEnumerator clicbtnLock()
        {
            txtWarning.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            txtWarning.gameObject.SetActive(false);
        }
    }


}


