using UnityEngine;
using Yurowm.GameCore;

public class FireBallBoss : FxItem
{
    public float damage;
    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.trainManager.GunPosition.position, 50*Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            TrainManager target = collision.gameObject.GetComponent<TrainManager>();
            target.damageGiven = damage;
            if (target.carState == CarState.Hit)
            {
                target.GetHitBullet();
            }
            else
            {
                target.ChangeState(new CarHitState(target));
            }


            FxItem explosion = ContentPoolable.Emit(ItemID.fxBossDeathExplosion) as FxItem;
            explosion.transform.position = this.transform.position + new Vector3(-2,0,0);
            this.Kill();
        }
    }
}
