using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class TiepTeUI : MonoBehaviour
{
    public TMP_Text nameTiepTe;
    public Slider slider;
    public TiepTe tiepTe;
    private bool isStart =false;
    public RectTransform rectTransform;

    private void Update()
    {
        if (isStart)
        {
            tiepTe.timeActive -= Time.deltaTime;
            slider.value = tiepTe.timeActive;
            if(tiepTe.timeActive <= 0)
            {
                isStart = false;
                Hide();
                GameManager.Instance.trainManager.playerGun.tiepTe = null;
                GameManager.Instance.trainManager.HetTiepTe();
            }
        }
    }

    public void Show(TiepTe tiepTe)
    {
        this.tiepTe = tiepTe;
        nameTiepTe.text = tiepTe.nameTiepTe;
        slider.maxValue = tiepTe.timeActive;
        slider.value = tiepTe.timeActive;

        rectTransform.DOAnchorPos(new Vector2(253f,-155.61f),0.5f).OnComplete(()=>isStart = true);
    }

    public void Hide()
    {
        rectTransform.DOAnchorPos(new Vector2(-300f, -155.61f), 0.5f);
    }
}
