using AA_Game;
using UnityEngine;
using DG.Tweening;
using Yurowm.GameCore;

public class FXCollectTiepTe : Item
{
    private void OnEnable()
    {
        transform.parent = GameManager.Instance.trainManager.GunPosition;
        transform.DOLocalMove(Vector3.zero, 1.5f).OnComplete(() => {

            Item fxUp = ContentPoolable.Emit(ItemID.car_up_1) as Item;
            fxUp.transform.position = GameManager.Instance.trainManager.transform.position;
            fxUp.transform.parent = GameManager.Instance.trainManager.carPos;
        });
        transform.localScale = new Vector3(3, 3, 3);
    }
}
