using UnityEngine;
using Yurowm.GameCore;

public class BossSkelController : EnemyBase
{
    int attackIndex;
    private Vector3 startPos = new Vector3(17, -12, 0);
    bool isSkillOut;

    public override void OnEnable()
    {
        base.OnEnable();
        if (GlobalData.gameMode == GameMode.BossWorld)
        {
            distanceAttack += 6f;
        }
    }

    public override void Start()
    {
        base.Start();
        if (GlobalData.gameMode == GameMode.BossWorld)
        {
            transform.position = startPos;
        }

        if (GlobalData.gameMode == GameMode.Endless || GlobalData.gameMode == GameMode.Normal)
        {
            transform.position = new Vector3(transform.position.x,0,transform.position.z);
        }
    }

    public override void EnterFollowCar()
    {
        base.EnterFollowCar();
        anim.PlayAnim(AnimID.run, true, 1f, false);
    }

    public override void UpdateFollowCar()
    {
        targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        float dis = Vector3.Distance(transform.position, targetComeTo);
        if (SpeedGrowingUp < 3 + RandomSpeed)
        {
            SpeedGrowingUp += Time.deltaTime / 2;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed + SpeedGrowingUp) * Time.deltaTime);

        if (dis <= distanceAttack && transform.position.y < 8f)
        {
            if (attackIndex == 0)
            {
                attackIndex += 1;
                ChangeState(new AttackState(this));
            }
            else if (attackIndex == 1)
            {
                attackIndex += 1;
                ChangeState(new Attack2State(this));
            }
            else
            {
                attackIndex += 1;
                ChangeState(new Attack3State(this));
            }
            if (attackIndex > 2)
            {
                attackIndex = 0;
            }
        }
    }

    public override void UpdateAttack()
    {
        if (GlobalData.gameMode != GameMode.BossWorld)
        {
            targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2.5f) * Time.deltaTime);
        }
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack_1).duration)
        {
            if (GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
        if(timeInState >= (anim.GetAnimData(AnimID.attack_1).duration - 0.6f))
        {
            if(isSkillOut == false)
            {
                isSkillOut = true;
                OutSkill1();
            }
        }
    }

    public void OutSkill1()
    {
        AudioManager.instance.Play("Thunder");
        FxItem fxItem = ContentPoolable.Emit(ItemID.skelThunder) as FxItem;
        fxItem.transform.parent = GameManager.Instance.trainManager.transform;
        fxItem.transform.localPosition = new Vector3(0,1,0);
        target.damageGiven = damage;
        if (target.carState == CarState.Hit)
        {
            target.GetHitBullet();
        }
        else
        {
            target.ChangeState(new CarHitState(target));
        }
    }

    public override void ExitAttack()
    {
        isSkillOut = false;
    }

    public override void UpdateAttack2()
    {
        if (GlobalData.gameMode != GameMode.BossWorld)
        {
            targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2) * Time.deltaTime);
        }
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack_2).duration)
        {
            if (GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }

    public override void UpdateAttack3()
    {
        if (GlobalData.gameMode != GameMode.BossWorld)
        {
            targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2f) * Time.deltaTime);
        }
        
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack3).duration)
        {
            if (GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }

    public override void HandleEvent(string eventName)
    {
        if (eventName == "attack" || eventName == "attack_tracking" || eventName == "hit")
        {
            target.damageGiven = damage;
            if (target.carState == CarState.Hit)
            {
                target.GetHitBullet();
            }
            else
            {
                target.ChangeState(new CarHitState(target));
            }
        }

        if (eventName == "attack1" || eventName == "attack2" || eventName == "attack3")
        {
        }
    }

    public override void EnterFree()
    {
        base.EnterFree();
    }


    public override void UpdateFree()
    {
        base.UpdateFree();
        timeInState += Time.deltaTime;
        if (timeInState >= 2f && GameManager.Instance.gameState == GameState.Playing)
        {
            ChangeState(new FollowCar(this));
        }
    }

    public override void EnterLeaveTheCar()
    {
        enemyState = EnemyState.LeaveTheCar;
        anim.PlayAnim(AnimID.run, true, 1f, false);
    }


    public override void UpdateLeaveTheCar()
    {
        transform.position = Vector3.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        float dis = Vector3.Distance(this.transform.position, startPos);
        if (dis <= 0.5f)
        {
            ChangeState(new FreeState(this));
        }
    }
}
