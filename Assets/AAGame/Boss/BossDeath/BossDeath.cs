using UnityEngine;
using Yurowm.GameCore;

public class BossDeath : EnemyBase
{
    int attackIndex;
    public GameObject fxShootHand;
    private Vector3 startPos = new Vector3(17, -8, 0);

    public override void OnEnable()
    {
        base.OnEnable();
        startPos = new Vector3(17, -8, 0);
    }

    public override void EnterFollowCar()
    {
        base.EnterFollowCar();
        anim.PlayAnim(AnimID.run, true, 1f, false);
        if (GlobalData.gameMode == GameMode.BossWorld)
        {
            if (attackIndex == 1)
            {
                distanceAttack += 3f;
            }
        }
    }

    public override void UpdateFollowCar()
    {
        if (GlobalData.gameMode != GameMode.BossWorld)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        
        targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        float dis = Vector3.Distance(transform.position, targetComeTo);
        if (SpeedGrowingUp < 3 + RandomSpeed)
        {
            SpeedGrowingUp += Time.deltaTime / 2;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed + SpeedGrowingUp) * Time.deltaTime);

        if (dis <= distanceAttack && transform.position.y < 8f)
        {
            if (attackIndex == 0)
            {
                attackIndex += 1;
                ChangeState(new AttackState(this));
            }
            else if (attackIndex == 1)
            {
                attackIndex += 1;
                ChangeState(new Attack2State(this));
                if (GlobalData.gameMode == GameMode.BossWorld)
                {
                    distanceAttack -= 3f;
                }
            }
            else
            {
                attackIndex += 1;
                ChangeState(new AttackState(this));
            }
            if (attackIndex > 2)
            {
                attackIndex = 0;
            }
        }
    }

    public override void UpdateAttack()
    {
        if(GlobalData.gameMode != GameMode.BossWorld)
        {
            targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2) * Time.deltaTime);
        }
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack_1).duration)
        {
            if (GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }

    public override void EnterAttack2()
    {
        base.EnterAttack2();
        fxShootHand.SetActive(true);
    }

    public override void UpdateAttack2()
    {
        if (GlobalData.gameMode != GameMode.BossWorld)
        {
            targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2) * Time.deltaTime);
        }
        timeInState += Time.deltaTime;
        if(timeInState >= anim.GetAnimData(AnimID.attack_2).duration - 0.3f)
        {
            fxShootHand.SetActive(false);
        }
        if (timeInState >= anim.GetAnimData(AnimID.attack_2).duration)
        {
            if(GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }

    public override void ExitAttack2()
    {
        base.ExitAttack2();
        fxShootHand.SetActive(false);
        
    }

    public override void UpdateAttack3()
    {
        targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed / 2f) * Time.deltaTime);
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack3).duration)
        {
            if (GlobalData.gameMode != GameMode.BossWorld)
            {
                ChangeState(new FollowCar(this));
            }
            else
            {
                ChangeState(new LeaveTheCarState(this));
            }
        }
    }

    public override void HandleEvent(string eventName)
    {
        if (eventName == "attack" || eventName == "attack_tracking" || eventName == "hit")
        {
            target.damageGiven = damage;
            if (target.carState == CarState.Hit)
            {
                target.GetHitBullet();
            }
            else
            {
                target.ChangeState(new CarHitState(target));
            }
        }

        if (eventName == "attack1" || eventName == "attack2" || eventName == "attack3")
        {
            FireBallBoss fireBall = ContentPoolable.Emit(ItemID.magicBossDeath) as FireBallBoss;
            fireBall.transform.position = fxShootHand.transform.position;
        }
    }

    public override void EnterFree()
    {
        base.EnterFree();
    }


    public override void UpdateFree()
    {
        base.UpdateFree();
        timeInState += Time.deltaTime;
        if (timeInState >= 2f && GameManager.Instance.gameState == GameState.Playing)
        {
            ChangeState(new FollowCar(this));
        }
    }

    public override void EnterLeaveTheCar()
    {
        enemyState = EnemyState.LeaveTheCar;
        anim.PlayAnim(AnimID.run, true, 1f, false);
    }


    public override void UpdateLeaveTheCar()
    {
        transform.position = Vector3.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        float dis = Vector3.Distance(this.transform.position, startPos);
        if (dis <= 0.5f)
        {
            ChangeState(new FreeState(this));
        }
    }
}
