using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Thanh.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupClaimItem : Popup
{
    public Image imgIcon;
    public Button OkCollect;
    public Button btnX2;
    [SerializeField]
    private TextMeshProUGUI textVale;
    private int value;
    private ItemValue itemValue;
    [SerializeField]
    private Image halo;

    [SerializeField]
    public Button btnNothanks;
    public Image imgSkinIcon;
    public static PopupClaimItem Instane;
    public void Awake()
    {
        Instane = this;
    }

    public bool setSkin;
    private int Value
    {
        get { return value; }
        set
        {
            this.value = value;


            textVale.gameObject.SetActive(true);

            textVale.text = "+" + this.Value.ToString();
            
        }
    }
    public override void OnShow()
    {
        base.OnShow();
        AudioManager.instance.Play("unlockitem");

    }
    public void OnEnable()
    {
        halo.transform.DORotate(new Vector3(0, 0, 360), 2f, DG.Tweening.RotateMode.FastBeyond360).OnComplete(() =>
        {
            btnNothanks.onClick.RemoveAllListeners();
            btnNothanks.onClick.AddListener(() => OnClickNoThanks());
        });
        StartCoroutine(InsBtnNothanks());
    }
    public void SetData(ItemValue _itemValue, int XValue = 1)
    {
        itemValue = _itemValue;
        Value = itemValue.value;

        btnX2.gameObject.SetActive(true);
        btnX2.interactable = true;


        SetIconImg();
        

        OkCollect.onClick.RemoveListener(Close);
        OkCollect.onClick.AddListener(Close);
        btnX2.onClick.RemoveAllListeners();
        btnX2.onClick.AddListener(() =>
        {
            ClickBtnX2();
        });
    }


    public void SetIconImg()
    {
        if (itemValue.item != ItemID.Gold && itemValue.item != ItemID.ticket && itemValue.item != ItemID.itemDotPha && itemValue.item != ItemID.thorAmount)
        {

            imgIcon.sprite = SpriteManager.Instance.GetSprite(itemValue.item);
            imgIcon.SetNativeSize();
            imgIcon.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }
        else if (itemValue.item == ItemID.Gold || itemValue.item == ItemID.ticket || itemValue.item == ItemID.itemDotPha || itemValue.item == ItemID.thorAmount)
        {
            imgIcon.sprite = SpriteManager.Instance.GetSprite(itemValue.item);
            imgIcon.SetNativeSize();
            imgIcon.transform.localScale = new Vector3(2f, 2f, 2f);

            if (itemValue.item == ItemID.thorAmount)
            {
                imgIcon.sprite = SpriteManager.Instance.GetSprite(itemValue.item, 1);
                textVale.text = "+" + this.Value.ToString();
                textVale.gameObject.SetActive(true);

                return;
            }
        }

    }

    public override void Close()
    {
        base.Close();
        btnNothanks.gameObject.SetActive(false);
    }

    public void OnClickNoThanks()
    {

        btnX2.interactable = false;
        btnNothanks.gameObject.SetActive(false);
        Close();
    }

    public void ClickBtnX2()
    {
        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null,
            (success) =>
            {
                if (success)
                {

                    int gold = itemValue.value;
                    if (gold > 0)
                    {
                        PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem, (pop) =>
                        {

                            ItemValue reward = new ItemValue(itemValue.item, gold);

                            pop.SetData(reward, 1);

                        });

                    }
                    if (itemValue.item == ItemID.thorAmount)
                    {

                        User.Instance[ItemID.thorAmount] += gold;
                    }
                    else if (itemValue.item == ItemID.Gold)
                    {
                        User.Instance[ItemID.Gold] += gold;
                    }
                    Close();
                }
            }, AdLocation.ClaimX2);
    }


    IEnumerator InsBtnNothanks()
    {
        yield return new WaitForSeconds(3f);
        btnNothanks.gameObject.SetActive(true);
        btnNothanks.interactable = true;

    }
}
