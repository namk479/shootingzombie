
lv5.png
size: 960,960
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: true
  xy: 710, 579
  size: 269, 54
  orig: 269, 54
  offset: 0, 0
  index: -1
L_eye
  rotate: false
  xy: 863, 273
  size: 64, 66
  orig: 64, 66
  offset: 0, 0
  index: -1
R_eye
  rotate: true
  xy: 863, 341
  size: 101, 90
  orig: 101, 90
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 766, 607
  size: 147, 46
  orig: 147, 47
  offset: 0, 0
  index: -1
blur
  rotate: false
  xy: 2, 382
  size: 364, 573
  orig: 364, 573
  offset: 0, 0
  index: -1
eye
  rotate: false
  xy: 368, 442
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
eyebrow
  rotate: false
  xy: 766, 755
  size: 169, 93
  orig: 169, 93
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 409, 21
  size: 316, 453
  orig: 317, 455
  offset: 0, 1
  index: -1
head_fake
  rotate: false
  xy: 368, 476
  size: 340, 479
  orig: 340, 479
  offset: 0, 0
  index: -1
mouth1
  rotate: true
  xy: 863, 444
  size: 161, 86
  orig: 161, 160
  offset: 0, 22
  index: -1
neck
  rotate: false
  xy: 727, 16
  size: 134, 561
  orig: 134, 563
  offset: 0, 1
  index: -1
nose
  rotate: false
  xy: 766, 655
  size: 156, 98
  orig: 156, 99
  offset: 0, 0
  index: -1
toilet2
  rotate: false
  xy: 2, 16
  size: 405, 364
  orig: 478, 584
  offset: 64, 1
  index: -1
toilet4
  rotate: false
  xy: 710, 850
  size: 245, 105
  orig: 327, 118
  offset: 63, 11
  index: -1
