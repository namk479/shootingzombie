
R.png
size: 624,624
format: RGBA8888
filter: Linear,Linear
repeat: none
1/body
  rotate: false
  xy: 239, 26
  size: 235, 283
  orig: 236, 283
  offset: 1, 0
  index: -1
1/glass_1
  rotate: true
  xy: 573, 148
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
1/glass_2
  rotate: false
  xy: 476, 75
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
1/glass_3
  rotate: false
  xy: 476, 27
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 2, 26
  size: 235, 283
  orig: 236, 283
  offset: 1, 0
  index: -1
body_die
  rotate: false
  xy: 2, 311
  size: 255, 308
  orig: 261, 316
  offset: 3, 4
  index: -1
die
  rotate: false
  xy: 259, 389
  size: 227, 230
  orig: 239, 253
  offset: 1, 11
  index: -1
glass_1
  rotate: true
  xy: 464, 319
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
glass_2
  rotate: true
  xy: 573, 78
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
glass_3
  rotate: false
  xy: 546, 30
  size: 68, 46
  orig: 70, 48
  offset: 1, 1
  index: -1
mouth_1
  rotate: false
  xy: 259, 338
  size: 112, 49
  orig: 114, 51
  offset: 1, 1
  index: -1
wing_L1
  rotate: true
  xy: 488, 401
  size: 115, 95
  orig: 115, 95
  offset: 0, 0
  index: -1
wing_L2
  rotate: true
  xy: 488, 518
  size: 101, 109
  orig: 101, 110
  offset: 0, 1
  index: -1
wing_L3
  rotate: false
  xy: 476, 218
  size: 116, 83
  orig: 116, 83
  offset: 0, 0
  index: -1
wing_R1
  rotate: true
  xy: 512, 303
  size: 96, 104
  orig: 96, 104
  offset: 0, 0
  index: -1
wing_R2
  rotate: true
  xy: 476, 123
  size: 93, 95
  orig: 93, 95
  offset: 0, 0
  index: -1
wing_R3
  rotate: true
  xy: 373, 329
  size: 58, 89
  orig: 58, 89
  offset: 0, 0
  index: -1
