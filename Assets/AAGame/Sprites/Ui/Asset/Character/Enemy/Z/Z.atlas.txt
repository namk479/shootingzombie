
Z.png
size: 608,608
format: RGBA8888
filter: Linear,Linear
repeat: none
1/body
  rotate: true
  xy: 319, 2
  size: 301, 277
  orig: 302, 277
  offset: 1, 0
  index: -1
1/glass_1
  rotate: false
  xy: 88, 18
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
1/glass_2
  rotate: true
  xy: 260, 218
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
1/glass_3
  rotate: false
  xy: 231, 78
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
body
  rotate: true
  xy: 319, 305
  size: 301, 277
  orig: 302, 277
  offset: 1, 0
  index: -1
body_die
  rotate: false
  xy: 2, 304
  size: 315, 302
  orig: 321, 308
  offset: 3, 3
  index: -1
die
  rotate: false
  xy: 2, 72
  size: 227, 230
  orig: 239, 253
  offset: 1, 11
  index: -1
glass_1
  rotate: false
  xy: 2, 18
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
glass_2
  rotate: false
  xy: 174, 18
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
glass_3
  rotate: true
  xy: 260, 132
  size: 84, 52
  orig: 86, 54
  offset: 1, 1
  index: -1
