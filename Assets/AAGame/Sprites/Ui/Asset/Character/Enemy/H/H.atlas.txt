
H.png
size: 516,516
format: RGBA8888
filter: Linear,Linear
repeat: none
1/body
  rotate: true
  xy: 2, 8
  size: 186, 279
  orig: 186, 279
  offset: 0, 0
  index: -1
1/glass_1
  rotate: false
  xy: 391, 332
  size: 48, 49
  orig: 50, 51
  offset: 1, 1
  index: -1
1/glass_2
  rotate: false
  xy: 441, 383
  size: 48, 49
  orig: 50, 51
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 203, 234
  size: 186, 279
  orig: 186, 279
  offset: 0, 0
  index: -1
body_die
  rotate: false
  xy: 2, 196
  size: 199, 317
  orig: 205, 323
  offset: 3, 3
  index: -1
die
  rotate: false
  xy: 283, 2
  size: 230, 230
  orig: 253, 253
  offset: 12, 11
  index: -1
glass_1
  rotate: false
  xy: 391, 383
  size: 48, 49
  orig: 50, 51
  offset: 1, 1
  index: -1
glass_2
  rotate: false
  xy: 391, 281
  size: 48, 49
  orig: 50, 51
  offset: 1, 1
  index: -1
wing
  rotate: false
  xy: 391, 434
  size: 87, 79
  orig: 88, 79
  offset: 0, 0
  index: -1
