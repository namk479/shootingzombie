using UnityEngine;
using UnityEngine.UI;

public class OpenShop : MonoBehaviour
{
    private Button buttonOpenShop;
    public Tab tabOpen;

    private void Start()
    {
        buttonOpenShop = GetComponent<Button>();
        buttonOpenShop.onClick.AddListener(Open);
    }

    public void Open()
    {
        if(tabOpen == Tab.Gold_IAP)
        {
            PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => { pop.ButtonGoldIAP(); });
        }
        else if (tabOpen == Tab.Hammer_IAP)
        {
            PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => { pop.ButtonHammerIAP(); });
        }
        else if (tabOpen == Tab.Gold_Free)
        {
            PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => { pop.ButtonGoldAds(); });
        }
        else if (tabOpen == Tab.Hammer_Free)
        {
            PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => { pop.ButtonHammerAds(); });
        }
    }
}

public enum Tab
{
    Gold_IAP,
    Hammer_IAP,
    Gold_Free,
    Hammer_Free,
}
