using System;
using System.Collections.Generic;
using System.Linq;

public class QuestManager : Singleton<QuestManager>
{
    public QuestableData questTable;
    public QuestableData questTableWeek;

    public bool isUsingWitch;
    public bool isUsingBomb;
    public bool isUsingDrone;
    public bool isUsingShield;
    public bool isUsingReviveModeBoss;

    public void Start()
    {
        TimeSpan timeRemainValue = (DateTime.Parse(User.Instance.DateStartQuest).AddDays(1) - DateTime.Now);
        TimeSpan timeRemainValueWeek = (DateTime.Parse(User.Instance.DateStartQuestWeek).AddDays(7) - DateTime.Now);
        if (timeRemainValue.TotalDays < 0)
        {
            InitQuest();
        }

        if (timeRemainValueWeek.TotalDays < 0)
        {
            InitQuestWeek();
        }
    }

    public void InitQuest()
    {
        User.Instance.DailyQuest().Clear();

        User.Instance.DateStartQuest = DateTime.Now.Date.ToString();
        List<Quest> temp = new List<Quest>();
        temp.AddRange(questTable.quests);


        //xoa nhung quest mac dinh vi luon luon co trong daily quest
        foreach (var item in ItemType.QuestContains)
        {
            Quest quest = temp.First(x => x.questID == item);
            temp.Remove(quest);
        }

        //them 10 quest mac dinh
        for (int i = 0; i < 10; i++)
        {
            Quest quest = questTable.quests.First(x => x.questID == ItemType.QuestContains[i]);
            Quest quest1 = new Quest(quest);
            User.Instance.DailyQuest().Add(quest1);
        }

        //them 3 quest random
        for (int i = 0; i < 3; i++)
        {
            Quest quest = temp.GetRandom();
            Quest quest1 = new Quest(quest);
            User.Instance.DailyQuest().Add(quest1);
            temp.Remove(quest);
        }

        //clear daily quest value
        foreach (ItemID item in ItemType.Quest)
        {
            User.Instance[item] = 0;
        }
        foreach (ItemID item in ItemType.BonusQuestIndex)
        {
            User.Instance[item] = 0;
        }
        User.Instance[ItemID.processDailyQuest] = 0;

        User.Instance.Save();
    }


    public void InitQuestWeek()
    {
        User.Instance.DailyQuestWeek().Clear();

        User.Instance.DateStartQuestWeek = DateTime.Now.Date.ToString();
        List<Quest> temp = new List<Quest>();
        temp.AddRange(questTableWeek.quests);
        for (int i = 0; i < 12; i++)
        {
            Quest quest = temp.GetRandom();
            Quest quest1 = new Quest(quest);
            User.Instance.DailyQuestWeek().Add(quest1);
            temp.Remove(quest);
        }

        //clear week quest
        foreach (ItemID item in ItemType.QuestWeek)
        {
            User.Instance[item] = 0;
        }
        foreach (ItemID item in ItemType.BonusWeekIndex)
        {
            User.Instance[item] = 0;
        }
        User.Instance[ItemID.processWeekQuest] = 0;

        User.Instance.Save();
    }
}
