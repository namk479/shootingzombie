using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardDailyGroup : MonoBehaviour
{
    public Slider slider;
    public TMP_Text sliderValue;
    public bool isWeekQuest;

    private void OnEnable()
    {
        GameEvent.OnClaimDailyQuest.RemoveListener(SetUp);
        GameEvent.OnClaimDailyQuest.AddListener(SetUp);

        if (!isWeekQuest)
        {
            GameEvent.OnResetDailyQuest.RemoveListener(SetUp);
            GameEvent.OnResetDailyQuest.AddListener(SetUp);
        }
        else
        {
            GameEvent.OnResetWeekQuest.RemoveListener(SetUp);
            GameEvent.OnResetWeekQuest.AddListener(SetUp);
        }


        SetUp();
    }

    public void SetUp()
    {
        if (!isWeekQuest)
        {
            slider.DOValue(User.Instance[ItemID.processDailyQuest], 1f);
            sliderValue.text = User.Instance[ItemID.processDailyQuest].ToString() + "/100";
        }
        else
        {
            slider.DOValue(User.Instance[ItemID.processWeekQuest], 1f);
            sliderValue.text = User.Instance[ItemID.processWeekQuest].ToString() + "/100";
        }
    }
}
