using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using System.Collections;

public class BonusDailyQuest : MonoBehaviour
{
    public int processNeeded;
    public QuestReward bonus;
    public Button claimButton;
    public Image iconTick;
    public SkeletonGraphic iconReward;
    public Material gray;

    public ItemID bonusIndex;
    public bool isWeekQuest;
    public ReviewQuestBonus reviewQuestBonus;

    private void OnEnable()
    {
        claimButton.onClick.RemoveListener(ButtonClaim);
        claimButton.onClick.AddListener(ButtonClaim);
        GameEvent.OnClaimDailyQuest.RemoveListener(SetUp);
        GameEvent.OnClaimDailyQuest.AddListener(SetUp);

        if (!isWeekQuest)
        {
            GameEvent.OnResetDailyQuest.RemoveListener(SetUp);
            GameEvent.OnResetDailyQuest.AddListener(SetUp);
        }
        else
        {
            GameEvent.OnResetWeekQuest.RemoveListener(SetUp);
            GameEvent.OnResetWeekQuest.AddListener(SetUp);
        }

        SetUp();
    }

    public void SetUp()
    {
        if (!IsPass())
        {
            iconReward.material = gray;
            iconTick.gameObject.SetActive(false);
        }
        else
        {
            iconReward.material = null;
            if (IsClaimed())
            {
                iconTick.gameObject.SetActive(true);
                iconReward.AnimationState.SetAnimation(0,"Open_idle",true);
            }
            else
            {
                iconTick.gameObject.SetActive(false);
            }
        }
        reviewQuestBonus.gameObject.SetActive(false);
    }


    /// <summary>
    /// Call by button reward,claim or view
    /// </summary>
    public void ButtonClaim()
    {
        if(IsPass() && IsClaimed() == false)
        {
            User.Instance[bonus.rewardId] += bonus.rewardAmount;
            User.Instance[bonusIndex] += 1;
            iconTick.gameObject.SetActive(true);
            PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem, (pop) =>
            {
                ItemValue reward = new ItemValue(bonus.rewardId, bonus.rewardAmount);
                pop.SetData(reward, 1);
            });

            StartCoroutine(AnimOpen());
        }
        else if(IsPass() == false)
        {
            reviewQuestBonus.gameObject.SetActive(true);
            reviewQuestBonus.SetUp(this.bonus);
        }
    }


    /// <summary>
    /// Checking pass or not
    /// </summary>
    /// <returns></returns>
    public bool IsPass()
    {
        bool value = false;
        if (!isWeekQuest)
        {
            value = processNeeded <= User.Instance[ItemID.processDailyQuest];
        }
        else
        {
            value = processNeeded <= User.Instance[ItemID.processWeekQuest];
        }
        return value;
    }


    /// <summary>
    /// If passed,checking is claimed?
    /// </summary>
    /// <returns></returns>
    public bool IsClaimed()
    {
        return User.Instance[bonusIndex] >= 1;
    }


    IEnumerator AnimOpen()
    {
        iconReward.AnimationState.SetAnimation(0, "Open", false);
        yield return new WaitForSeconds(3.667f);
        SetUp();
    }
}
