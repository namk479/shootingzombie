using System.Collections.Generic;
using Thanh.Core;
using UnityEngine;
using Yurowm.GameCore;
using TMPro;
using System;
using UnityEngine.UI;
using System.Linq;

public class PopupDailyQuest : Popup
{
    public Transform groupQuest;
    public Transform groupQuestWeek;

    public List<Quest> quests;
    public List<Quest> questsWeek;

    public TMP_Text timeRemain;
    private bool isWeekQuest;

    public Button buttonDay;
    public Button buttonWeek;

    public Sprite[] spriteButton;

    public GameObject rewardDaily;
    public GameObject rewardWeek;

    private void OnEnable()
    {
        buttonDay.onClick.RemoveListener(QuestDay);
        buttonDay.onClick.AddListener(QuestDay);

        buttonWeek.onClick.RemoveListener(QuestWeek);
        buttonWeek.onClick.AddListener(QuestWeek);
    }


    public override void OnShow()
    {
        base.OnShow();
        QuestDay();
    }

    public void QuestDay()
    {
        isWeekQuest = false;
        buttonDay.image.sprite = spriteButton[0];
        buttonWeek.image.sprite = spriteButton[1];
        groupQuest.transform.localScale = Vector3.one;
        groupQuestWeek.transform.localScale = Vector3.zero;
        SetUp();
        rewardDaily.gameObject.SetActive(true);
        rewardWeek.gameObject.SetActive(false);
    }


    public void QuestWeek()
    {
        isWeekQuest = true;
        buttonDay.image.sprite = spriteButton[1];
        buttonWeek.image.sprite = spriteButton[0];
        groupQuest.transform.localScale = Vector3.zero;
        groupQuestWeek.transform.localScale = Vector3.one;
        SetUpWeek();
        rewardDaily.gameObject.SetActive(false);
        rewardWeek.gameObject.SetActive(true);
    }

    public void SetUpWeek()
    {
        this.quests = User.Instance.DailyQuestWeek();

        if (groupQuestWeek.childCount == 0)
        {
            foreach (Quest quest in quests)
            {
                QuestCell questCell = ContentPoolable.Emit(ItemID.QuestCell) as QuestCell;
                questCell.transform.SetParent(groupQuestWeek);// = groupQuestWeek;
                questCell.transform.localScale = Vector3.one;
                questCell.isQuestWeek = true;
                questCell.SetUp(quest);
            }
        }
        else
        {

            int index = 0;
            foreach (Transform questCell in groupQuestWeek)
            {
                questCell.GetComponent<QuestCell>().SetUp(User.Instance.DailyQuestWeek()[index]);
                index += 1;
            }
        }
    }

    public void SetUp()
    {
        this.quests = User.Instance.DailyQuest();
        if(groupQuest.childCount == 0)
        {
            foreach (Quest quest in quests)
            {
                QuestCell questCell = ContentPoolable.Emit(ItemID.QuestCell) as QuestCell;
                questCell.transform.SetParent(groupQuest);// = groupQuest;
                questCell.transform.localScale = Vector3.one;
                questCell.isQuestWeek = false;
                questCell.SetUp(quest);
            }
        }
        else
        {
            
            int index = 0;
            foreach (Transform questCell in groupQuest)
            {
                questCell.GetComponent<QuestCell>().SetUp(User.Instance.DailyQuest()[index]);
                index += 1;
            }
        }
    }


    string formattedTime;
    private void Update()
    {
        TimeSpan timeRemainValue = (DateTime.Parse(User.Instance.DateStartQuest).AddDays(1) - DateTime.Now);
        TimeSpan timeRemainValueWeek = (DateTime.Parse(User.Instance.DateStartQuestWeek).AddDays(7) - DateTime.Now);

        if (!isWeekQuest)
        {
            formattedTime = timeRemainValue.ToString(@"hh\:mm\:ss");
        }
        else
        {
            formattedTime = timeRemainValueWeek.ToString(@"dd\:hh\:mm\:ss");
        }
        timeRemain.text = formattedTime;




        if (timeRemainValue.TotalDays < 0)
        {
            User.Instance.DateStartQuest = DateTime.Now.Date.ToString();
            QuestManager.Instance.InitQuest();
            SetUp();
            GameEvent.OnResetDailyQuest.Invoke();
        }

        if (timeRemainValueWeek.TotalDays < 0)
        {
            User.Instance.DateStartQuestWeek = DateTime.Now.Date.ToString();
            QuestManager.Instance.InitQuestWeek();
            SetUpWeek();
            GameEvent.OnResetWeekQuest.Invoke();
        }
    }
}
