using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest
{
    public ItemID questID;
    public string questName;
    public string questDecs;
    public int questRequier;
    public QuestMode questMode;
    public List<QuestReward> questReward;
    public bool isSuccess;
    public bool isClaimed;
    public DoQuest doQuest;
     
    public Quest(Quest quest)
    {
        questID = quest.questID;
        questName = quest.questName;
        questDecs = quest.questDecs;
        questRequier = quest.questRequier;
        questMode = quest.questMode;
        questReward = quest.questReward;
        isSuccess = quest.isSuccess;
        isClaimed = quest.isClaimed;
        doQuest = quest.doQuest;
    }
}

public enum QuestMode
{
    Easy,
    Hard,
}


[System.Serializable]
public class QuestReward
{
    public ItemID rewardId;
    public int rewardAmount;
}

public enum DoQuest
{
    OpenCampaign,
    OpenCollect,
    OpenBoss,
    OpenEndless,
    OpenCarUpgrade,
    OpenLuckySpin,
    OpenTalent,
}