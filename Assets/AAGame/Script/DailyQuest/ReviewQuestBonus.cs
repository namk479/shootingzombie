using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ReviewQuestBonus : MonoBehaviour
{
    public Image iconReward;
    public TMP_Text amountReward;

    private void OnEnable()
    {
        this.transform.localScale = Vector3.zero;
        this.transform.DOScale(Vector3.one,0.3f).OnComplete(()=> {
            this.transform.DOScale(Vector3.one, 0.5f).OnComplete(() =>
            {
                this.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => gameObject.SetActive(false));
            });
            
        });
    }

    public void SetUp(QuestReward questReward)
    {
        iconReward.sprite = SpriteManager.Instance.GetSprite(questReward.rewardId);
        amountReward.text = "x" + questReward.rewardAmount.ToString();
    }
}
