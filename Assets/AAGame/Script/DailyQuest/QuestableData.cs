using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "Quest/QuestData", order = 1)]
public class QuestableData : ScriptableObject
{
    public List<Quest> quests;
}
