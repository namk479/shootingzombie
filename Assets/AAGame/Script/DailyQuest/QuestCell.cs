using UnityEngine;
using UnityEngine.UI;
using TMPro;
using AA_Game;
using System.Linq;

public class QuestCell : Item
{
    public Quest quest;
    public Image iconReward_1;
    public TMP_Text amountReward_1;
    public Image iconReward_2;
    public TMP_Text amountReward_2;

    public TMP_Text decs;
    public TMP_Text valueCurrent;

    public Button claimBtn;
    public Button goBtn;
    public Image tick;
    public Sprite[] spriteClaim;

    public bool isQuestWeek;

    public void SetUp(Quest questt)
    {
        quest = questt;

        if (quest.questID != ItemID.PlayEndless15Min && quest.questID != ItemID.PlayEndless30Min && quest.questID != ItemID.PlayEndless60Min)
        {
            if (User.Instance[quest.questID] >= quest.questRequier)
            {
                quest.isSuccess = true;
            }
        }
        else
        {
            if ((int)((User.Instance[quest.questID]) / 60) >= quest.questRequier)
            {
                quest.isSuccess = true;
            }
        }



        iconReward_1.sprite = SpriteManager.Instance.GetSprite(quest.questReward[0].rewardId);
        amountReward_1.text = quest.questReward[0].rewardAmount.ToString();

        if (quest.questReward[0].rewardId == ItemID.thorAmount)
        {
            iconReward_1.sprite = SpriteManager.Instance.GetSprite(quest.questReward[0].rewardId, 1);
        }
        else
        {
            iconReward_1.sprite = SpriteManager.Instance.GetSprite(quest.questReward[0].rewardId);
        }

        if (quest.questReward[1].rewardId == ItemID.thorAmount)
        {
            iconReward_2.sprite = SpriteManager.Instance.GetSprite(quest.questReward[1].rewardId, 1);
        }
        else
        {
            iconReward_2.sprite = SpriteManager.Instance.GetSprite(quest.questReward[1].rewardId);
        }
        amountReward_2.text = quest.questReward[1].rewardAmount.ToString();

        decs.text = quest.questDecs.ToString();

        if (quest.questID != ItemID.PlayEndless15Min && quest.questID != ItemID.PlayEndless30Min && quest.questID != ItemID.PlayEndless60Min)
        {
            valueCurrent.text = User.Instance[quest.questID].ToString() + "/" + quest.questRequier.ToString();
        }
        else
        {
            valueCurrent.text = ((int)((User.Instance[quest.questID]) / 60)).ToString() + "/" + quest.questRequier.ToString(); ;
        }


        if (quest.questRequier == 0)
        {
            valueCurrent.gameObject.SetActive(false);
        }

        claimBtn.gameObject.SetActive(true);
        goBtn.gameObject.SetActive(true);
        if (quest.isSuccess && !quest.isClaimed)
        {
            claimBtn.image.sprite = spriteClaim[0];
            claimBtn.interactable = true;
            tick.gameObject.SetActive(false);
            goBtn.gameObject.SetActive(false);
        }
        else if (quest.isSuccess && quest.isClaimed)
        {
            claimBtn.gameObject.SetActive(false);
            goBtn.gameObject.SetActive(false);
            tick.gameObject.SetActive(true);
        }
        else if (!quest.isSuccess)
        {
            tick.gameObject.SetActive(false);
            claimBtn.gameObject.SetActive(false);
            goBtn.gameObject.SetActive(true);
            goBtn.onClick.RemoveListener(DoQuest);
            goBtn.onClick.AddListener(DoQuest);

        }
    }


    public void Claim()
    {
        foreach (QuestReward reward in quest.questReward)
        {
            User.Instance[reward.rewardId] += reward.rewardAmount;
        }
        quest.isClaimed = true;
        claimBtn.gameObject.SetActive(false);
        tick.gameObject.SetActive(true);

        if (!isQuestWeek)
        {
            User.Instance.DailyQuest().First(x => x.questID == quest.questID).isSuccess = true;
            User.Instance.DailyQuest().First(x => x.questID == quest.questID).isClaimed = true;
            User.Instance[ItemID.processDailyQuest] += 10;
            GameEvent.OnClaimDailyQuest.Invoke();
        }
        else
        {
            User.Instance.DailyQuestWeek().First(x => x.questID == quest.questID).isSuccess = true;
            User.Instance.DailyQuestWeek().First(x => x.questID == quest.questID).isClaimed = true;
            User.Instance[ItemID.processWeekQuest] += 10;
            GameEvent.OnClaimDailyQuest.Invoke();
        }
    }

    public void DoQuest()
    {
        AudioManager.instance.Play("BtnClick");
        PopupManager.Instance.CloseCurrentPopup();
        Invoke(this.quest.doQuest.ToString(), 0f);
    }

    public void OpenCampaign()
    {
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode, (pop) =>
        {
            pop.SetData(0);
        });
    }

    public void OpenCollect()
    {
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode, (pop) =>
        {
            pop.SetData(2);
        });
    }

    public void OpenBoss()
    {
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode, (pop) =>
        {
            pop.SetData(3);
        });
    }

    public void OpenEndless()
    {
        PopupManager.Instance.OpenPopup<PopupSelectMode>(PopupID.PopupSelectMode, (pop) =>
        {
            pop.SetData(1);
        });
    }

    public void OpenCarUpgrade()
    {
        GameScene.main.homePanel.OpenCarUpgrade();
    }

    public void OpenLuckySpin()
    {
        GameScene.main.homePanel.OpenSpinWheel();
    }

    public void OpenTalent()
    {
        GameScene.main.homePanel.OpenTalentPopup();
    }
}
