public class AttackSkillState : IState
{
    private readonly EnemyBase enemy;

    public AttackSkillState(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterAttackSkill();
    }

    public void UpdateState()
    {
        enemy.UpdateAttackSkill();
    }

    public void ExitState()
    {
        enemy.ExitAttackSkill();
    }
}

