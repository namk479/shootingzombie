public class FollowCarSlowdown : IState
{
    private readonly EnemyBase enemy;

    public FollowCarSlowdown(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterFollowCarSlow();
    }

    public void UpdateState()
    {
        enemy.UpdateFollowCarSlow();
    }

    public void ExitState()
    {
        enemy.ExitFollowCarSlow();
    }
}
