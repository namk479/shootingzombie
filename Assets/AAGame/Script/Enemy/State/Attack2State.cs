public class Attack2State : IState
{
    private readonly EnemyBase enemy;

    public Attack2State(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterAttack2();
    }

    public void UpdateState()
    {
        enemy.UpdateAttack2();
    }

    public void ExitState()
    {
        enemy.ExitAttack2();
    }
}

