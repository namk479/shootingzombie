public class LeaveTheCarState : IState
{
    private readonly EnemyBase enemy;


    public LeaveTheCarState(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterLeaveTheCar();
    }

    public void UpdateState()
    {
        enemy.UpdateLeaveTheCar();
    }

    public void ExitState()
    {
        enemy.ExitLeaveTheCar();
    }
}
