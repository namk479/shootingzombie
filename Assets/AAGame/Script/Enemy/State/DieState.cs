public class DieState : IState
{
    private readonly EnemyBase enemy;

    public DieState(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterDie();
    }

    public void UpdateState()
    {
        enemy.UpdateDie();
    }

    public void ExitState()
    {
        enemy.ExitDie();
    }
}
