public class SpawnState : IState
{
    private readonly EnemyBase enemy;

    public SpawnState(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterSpawn();
    }

    public void UpdateState()
    {
        enemy.UpdateSpawn();
    }

    public void ExitState()
    {
        enemy.ExitSpawn();
    }
}
