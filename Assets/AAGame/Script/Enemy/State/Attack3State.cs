public class Attack3State : IState
{
    private readonly EnemyBase enemy;

    public Attack3State(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterAttack3();
    }

    public void UpdateState()
    {
        enemy.UpdateAttack3();
    }

    public void ExitState()
    {
        enemy.ExitAttack3();
    }
}

