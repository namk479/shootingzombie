public class GetHitState : IState
{
    private readonly EnemyBase enemy;

    public GetHitState(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
    }

    public void UpdateState()
    {

    }

    public void ExitState()
    {

    }
}
