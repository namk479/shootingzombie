public class FollowCar : IState
{
    private readonly EnemyBase enemy;

    public FollowCar(EnemyBase enemy)
    {
        this.enemy = enemy;
    }

    public void EnterState()
    {
        enemy.EnterFollowCar();
    }

    public void UpdateState()
    {
        enemy.UpdateFollowCar();
    }

    public void ExitState()
    {
        enemy.ExitFollowCar();
    }
}
