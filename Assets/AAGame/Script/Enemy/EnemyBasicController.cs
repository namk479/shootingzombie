using System.Collections.Generic;
using UnityEngine;
using Yurowm.GameCore;

public class EnemyBasicController : EnemyBase
{
    public bool isDogEnemy;
    private Vector3 scaleOrigin;
    float offSetScale;

    public override void OnEnable()
    {
        base.OnEnable();
        ChangeState(new SpawnState(this));
        offSetScale = Random.Range(-0.2f, 0.2f);
    }

    public override void Start()
    {
        base.Start();
        scaleOrigin = transform.localScale;
        transform.localScale = scaleOrigin + new Vector3(offSetScale, offSetScale, transform.localScale.z);
    }

    public override void OnDisable()
    {
        transform.localScale = scaleOrigin;
    }

    public override void EnterSpawn()
    {
        base.EnterSpawn();
        if (typeEnemy == TypeEnemy.Enemy_Basic)
        {
            AudioManager.instance.Play("spawnEnemybasic");
        }
        if (typeEnemy == TypeEnemy.Enemy_Tank)
        {

            AudioManager.instance.Play("enemyTank");

        }

    }
    public override void EnterDie()
    {
        base.EnterDie();
        AudioManager.instance.Play("Enemydie2");
    }

    public override void EnterAttack()
    {
        if (typeEnemy == TypeEnemy.Enemy_Basic)
        {
            base.EnterAttack();

            AudioManager.instance.Play("basicEneAtk");
        }
        else if (typeEnemy == TypeEnemy.Enemy_Tank)
        {
            if (Random.Range(0, 2) == 1)
            {
                enemyState = EnemyState.Attack;
                anim.PlayAnim(AnimID.attack_1, true, 1, false);
            }
            else
            {
                enemyState = EnemyState.Attack;
                anim.PlayAnim(AnimID.attack_2, true, 1, false);
            }
            AudioManager.instance.Play("TankEneAtk");
        }
    }

    public override void GoldFxEnemyDie()
    {
        FxItem fxcoin = ContentPoolable.Emit(ItemID.FxGoldEnemyDie) as FxItem;
        if (typeEnemy == TypeEnemy.Enemy_Tank)
        {
            fxcoin.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z);
        }
        else
        {
            fxcoin.transform.position = this.transform.position;
        }
    }

    public override void UpdateAttack()
    {
        if (!isDogEnemy && typeEnemy != TypeEnemy.Enemy_Tank)
        {
            base.UpdateAttack();
        }
        else
        {
            timeInState += Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, (speed / 2) * Time.deltaTime);
            if (timeInState >= anim.GetAnimData(AnimID.attack_2, 1).duration)
            {
                ChangeState(new FollowCar(this));
            }
        }
    }
}
