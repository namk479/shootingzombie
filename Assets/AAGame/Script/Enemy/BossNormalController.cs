using DG.Tweening;
using UnityEngine;
using Yurowm.GameCore;

public class BossNormalController : EnemyBase
{
    public Transform[] posMiniTrain;
    public bool isBossTrain = false;
    private bool isSpawnMiniTrain;
    private int numberSkillUse;
    public bool isBossDoor;
    public bool isBossFly;
    public Transform posFireBall;
    private float timeFree;
    private TypeDragonAttack typeAttack;
    public float distanceAttackNearModeBoss;
    private Vector3 startPos = new Vector3(17, -10, 0);


    public bool isBossBlue;
    public bool isBossBanban;


    public void SpawnMiniTrain()
    {
        if (isBossTrain && !isSpawnMiniTrain && numberSkillUse ==2)
        {
            foreach(Transform transform in posMiniTrain)
            {
                EnemyPool enemy = ContentPoolable.Emit(ItemID.enemy_mini_train) as EnemyPool;
                EnemyBase enemyBase = enemy.GetComponent<EnemyBase>();
                enemy.transform.position = transform.position;
                enemy.GetComponent<EnemyBase>().target = GameManager.Instance.trainManager;
                enemyBase.ChangeState(new FollowCar(enemyBase));
                enemyBase.transform.localScale = new Vector3(0.4f,0.4f,1f);
                GameManager.Instance.enemiesCurrentAmount += 1;
                GameManager.Instance.totalEnemyInLevel += 1;
                GameManager.Instance.listEnemy.Add(enemy.GetComponent<EnemyBase>());
            }

            isSpawnMiniTrain = true;
            numberSkillUse = 0;
        }
    }

    public override void OnEnable()
    {
        base.OnEnable();

        if (isBossFly)
        {
            startPos = new Vector3(17,-5,0);
        }
        typeAttack = TypeDragonAttack.AttackNear;
        
    }

    /// <summary>
    /// Spawn dragon,chay het anim spawn se chuye qua free
    /// </summary>
    public override void UpdateSpawn()
    {
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.spawn, 1).duration)
        {
            ChangeState(new FreeState(this));
        }
    }

    public override void EnterFollowCar()
    {
        base.EnterFollowCar();
        anim.PlayAnim(AnimID.run, true, 1f, false);

        if(GlobalData.gameMode != GameMode.BossWorld)
        {
            if (isBossFly)
            {
                transform.DOMoveY(Random.Range(1.5F, 4F), 1F);
            }
        }
    }
    //public override void UpdateFollowCar()
    //{
    //    base.UpdateFollowCar();
    //    float dis = Vector3.Distance(transform.position, targetComeTo);
    //    if (dis <= 10 && User.Instance[ItemID.TutPlay] == 3)
    //    {
    //        Time.timeScale = 0.2f;
    //        targetComeTo = target.transform.position;
    //        GameEvent.OnSetTrueTutVip.Invoke(NameBooster.Shield);
    //    }
    //}

    public override void UpdateAttack()
    {
        targetComeTo = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetComeTo, (speed + SpeedGrowingUp) / 2 * Time.deltaTime);
        timeInState += Time.deltaTime;



        if (!isBossDoor && !isBossFly)
        {
            if (timeInState >= anim.GetAnimData(AnimID.attack_1).duration / anim.TimeScale)
            {
                numberSkillUse += 1;
                ChangeState(new FollowCarSlowdown(this));
            }
        }
        else
        {
            if (timeInState >= (anim.GetAnimData(AnimID.attack_1).duration / anim.TimeScale) * 4f )
            {
                ChangeState(new FollowCarSlowdown(this));
            }
        }

    }


    public override void EnterFollowCarSlow()
    {
        enemyState = EnemyState.FollowCar;
        speed = speedBase * 0.6f;
        if(isBossTrain)
        {
            anim.PlayAnim(AnimID.run, true, 0.5f, false);
        }
        else
        {
            anim.PlayAnim(AnimID.run, true, 1f, false);
        }
        capsuleCollider2D.enabled = true;
    }


    public override void UpdateFollowCarSlow()
    {
        base.UpdateFollowCarSlow();

        if (isBossTrain)
        {
            timeInState += Time.deltaTime;
            if (timeInState >= 3f)
            {
                SpawnMiniTrain();
            }
        }
    }

    public override void ExitFollowCarSlow()
    {
        base.ExitFollowCarSlow();

        if (isBossTrain)
        {
            isSpawnMiniTrain = false;
        }
    }



    public override void HandleEvent(string eventName)
    {
        if (eventName == "hit" || eventName == "attack_tracking")
        {
            if (!isBossDoor && !isBossFly)
            {
                target.damageGiven = damage;
                if (target.carState == CarState.Hit)
                {
                    target.GetHitBullet();
                }
                else
                {
                    target.ChangeState(new CarHitState(target));
                }
            }
            else
            {
                //door spawn fire ball
                BossDoorFireBall fireBall = ContentPoolable.Emit(ItemID.fireBallBossDoor) as BossDoorFireBall;
                fireBall.transform.position = posFireBall.position;
                Vector3 dir = - posFireBall.transform.position + new Vector3(target.transform.position.x-10f, target.transform.position.y+4f, target.transform.position.z);
                fireBall.transform.right = dir;
                fireBall.AddForce();
                fireBall.damage = this.damage;
            }
        }
    }


    /// <summary>
    /// Free mot luc roi sau do di chuyen de danh
    /// </summary>
    public override void EnterFree()
    {
        base.EnterFree();
        timeFree = Random.Range(1.5f, 2.5f);

        if (isBossBlue || isBossBanban)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1,transform.localScale.y,transform.localScale.z);
        }
    }


    /// <summary>
    /// Chay het thoi gian nghi sau do chuyen state
    /// </summary>
    public override void UpdateFree()
    {
        base.UpdateFree();
        timeInState += Time.deltaTime;
        if (timeInState >= timeFree && GameManager.Instance.gameState == GameState.Playing)
        {
            ChangeState(new BossPowerUpState(this));
        }
    }

    /// <summary>
    /// Dragon walk den ga cai xe
    /// Neu du gan se tan con gan Attack50HealthStage
    /// </summary>
    public override void UpdateFollowCar()
    {
        if (GlobalData.gameMode == GameMode.BossWorld)
        {
            targetComeTo = target.transform.position;
            float dis = Vector3.Distance(transform.position, targetComeTo);
            transform.position = Vector3.MoveTowards(transform.position, targetComeTo, speed * Time.deltaTime);

            //kiem tra xem danh gan hay danh xa
            if (typeAttack == TypeDragonAttack.AttackNear)
            {
                if (dis <= distanceAttackNearModeBoss)
                {
                    ChangeState(new Attack50HealthState(this));
                }
            }
            else
            {
                if (dis <= distanceAttackNearModeBoss * 1.3f)
                {
                    ChangeState(new Attack100HealthState(this));
                }
            }
        }
        else
        {
            base.UpdateFollowCar();
        }
    }

    //==========ATTACK50===========//
    public override void EnterAttack50()
    {
        enemyState = EnemyState.Attack50;

        if (!isBossBlue && !isBossBanban)
        {
            anim.PlayAnim(AnimID.attack_1, true, 1, false);
        }
        else
        {
            anim.PlayAnim(AnimID.attack_2, true, 1, false);
        }
    }

    /// <summary>
    /// Enter attack danh gan
    /// </summary>
    public override void UpdateAttack50()
    {
        timeInState += Time.deltaTime;
        if (timeInState >= anim.GetAnimData(AnimID.attack_1, 1.5f).duration * 3f)
        {
            ChangeState(new LeaveTheCarState(this));
        }
    }


    /// <summary>
    /// Dragon lui lai sau khi tan con gan
    /// </summary>
    public override void EnterLeaveTheCar()
    {
        enemyState = EnemyState.LeaveTheCar;
        anim.PlayAnim(AnimID.run, true, 1f, false);
        if (isBossBlue || isBossBanban)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
    }


    /// <summary>
    /// Lui ve vi tri ban dau de chuan bi attack xa
    /// </summary>
    public override void UpdateLeaveTheCar()
    {
        transform.position = Vector3.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        float dis = Vector3.Distance(this.transform.position, startPos);
        if (dis <= 0.5f)
        {
            ChangeState(new FreeState(this));
        }
    }
}
