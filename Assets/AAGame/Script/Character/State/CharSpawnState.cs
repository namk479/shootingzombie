public class CharSpawnState : IState
{
    private readonly CharacterBase character;

    public CharSpawnState(CharacterBase character)
    {
        this.character = character;
    }

    public void EnterState()
    {

    }

    public void UpdateState()
    {

    }

    public void ExitState()
    {

    }
}
