public class CharHitState : IState
{
    private readonly CharacterBase character;

    public CharHitState(CharacterBase character)
    {
        this.character = character;
    }

    public void EnterState()
    {

    }

    public void UpdateState()
    {

    }

    public void ExitState()
    {

    }
}
