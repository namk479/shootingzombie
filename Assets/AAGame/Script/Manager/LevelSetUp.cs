using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "LevelSetup/LevelData", order = 1)]
public class LevelSetUp : ScriptableObject
{
    public int level;
    public bool isLevelBlock;
    public WaveSetup[] waveSetups;
    public List<WaveSetup> listEndlessWave = new List<WaveSetup>();

    private void OnValidate()
    {
        if(level % 3 == 0)
        {
            isLevelBlock = true;
        }
        else
        {
            isLevelBlock = false;
        }
    }
}
