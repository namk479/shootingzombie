using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IManager
{
    void OnShowPopupRevive();
    void OnRevive();
    void OnCloseRevive();
}
