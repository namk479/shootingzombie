using Spine.Unity;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelBoss", menuName = "LevelBossSetup/LevelBossData", order = 1)]
public class LevelBossSetUp : ScriptableObject
{
    public int level;
    public BossWorldName bossName;
    public ItemID bossId;
    public ItemID isPass;
    public SkeletonDataAsset newSkeletonData;
    public string Name;
    public string skin;
    public Vector3 scale = new Vector3(0.5f,0.5f,0.5f);
    public Vector3 pos;

    public Vector3 scaleInReview = new Vector3(0.2f, 0.2f, 0.2f);
    public Vector3 posInReview;

    public void ChangeSkeletonDataAsset(SkeletonGraphic skeletonGraphic )
    {
        // Assign the new SkeletonDataAsset
        skeletonGraphic.skeletonDataAsset = newSkeletonData;

        // Initialize the SkeletonGraphic with the new SkeletonDataAsset
        skeletonGraphic.Initialize(true);
    }

    public int levelUnlock;

    public ItemID preBoss;
    public ItemID nextBoss;

}


public enum BossWorldName
{
    Boss_Train,
    Boss_Toilet,
    Boss_Door,
    Boss_Blue,
    Boss_BanBan,
    Boss_Fly,
    Dragon_Green,
    Dragon_Red,
    Dragon_Blue,
    Boss_Death,
    Boss_Skeleton,
}
