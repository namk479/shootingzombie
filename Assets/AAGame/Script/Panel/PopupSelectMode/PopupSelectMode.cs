﻿using DanielLochner.Assets.SimpleScrollSnap;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using Thanh.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupSelectMode : Popup
{
    public Button playBossModeBtn;
    public Button playEndlessModeBtn;
    public Button playCollectionModeBtn;
    public GameObject TxtWarning;
    public SkeletonGraphic SkeleletonPlay;
    public SimpleScrollSnap scrollSnap;
    public Mode mode;
    public Button btnPlay;
    public Image imglockPlay;
    public TMP_Text txtTypeLockPlay;
    public GameObject txtWarningBtnPlay;
    public Material btnPlayMaterial;
    public List<GameObject> lstCell = new List<GameObject>();
    public TMP_Text txtNameMode;
    public TMP_Text txtReward;
    public GameObject iconTypelock;
    public GameObject[] lstIcon;
    public TMP_Text txtWarning;

    public GameObject imgads;

    public Button hammerShop;
    public Button coinShop;

   
    public void OnEnable()
    {
        GlobalData.gameMode = GameMode.Normal;
        User.Instance.Save();
        GameEvent.OnMoveToPlay.RemoveListener(OnMoveToPlay);
        GameEvent.OnMoveToPlay.AddListener(OnMoveToPlay);
        btnPlay.onClick.RemoveListener(ClickPlay);
        btnPlay.onClick.AddListener(ClickPlay);

        hammerShop.onClick.RemoveListener(ClickHammerShop);
        hammerShop.onClick.AddListener(ClickHammerShop);

        coinShop.onClick.RemoveListener(ClickCoinShop);
        coinShop.onClick.AddListener(ClickCoinShop);
        //scrollSnap.ResetScrol();
        Invoke("OnCenterPanel", 0.1f);
    }

    public void SetData(int indexCell = 0)
    {
        scrollSnap.ResetScrol(indexCell);
    }
   

    public void ClickHammerShop()
    {
        base.Close();

    /*    PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => {
            pop.TypeInventory = TypeShop.ShopHammer;
            //pop.Setup();

        });*/
    }
    public void ClickCoinShop()
    {
        base.Close();
       /* PopupManager.Instance.OpenPopup<PopupShop>(PopupID.PopupShop, (pop) => {
            pop.TypeInventory = TypeShop.ShopGold;
            //pop.Setup();
        });*/
    }
   
    public void ClickPlay()
    {
        if (GlobalData.gameMode == GameMode.Normal)
        {
            GlobalData.instance.levelToPlay = User.Instance[ItemID.PlayingLevel];
            GlobalData.gameMode = GameMode.Normal;
            GameEvent.OnMoveToPlay.Invoke();
            User.Instance.Save();
        }
        if (GlobalData.gameMode == GameMode.BossWorld)
        {
            if (User.Instance[ItemID.AdsBossMode] == 0)
            {
                StartModeBoss();
            }
            else if (User.Instance[ItemID.AdsBossMode] == 1)
            {
                PopupManager.Instance.OpenPopup<PopupSelectBoss>(PopupID.PopupSelectBoss);
            }
        }

        if (GlobalData.gameMode == GameMode.Endless)
        {
            if(User.Instance[ItemID.AdsEndLessMode] == 0)
            {
                StartModeEndless();
            }
            else if(User.Instance[ItemID.AdsEndLessMode] == 1)
            {
                GameEvent.OnMoveToPlay.Invoke();
                User.Instance.Save();
                base.Close();
            }
        }

        //base.Close();


        if (GlobalData.gameMode == GameMode.CollectFuel)
        {
            if (User.Instance[ItemID.AdsCollectMode] < 10)
            {
                BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
                {
                    if (isSuccess)
                    {
                        StartModeCollect();
                    }

                }, AdLocation.PlayModeCollect);
            }
            else
            {
                txtWarning.text = "Played all the rounds for the day";
                //StartCoroutine(SetTrueWarning());
                PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice, (pop) => pop.SetData("The turn is over !", "Play is over for the day"));

            }
        }
    }


    public void OnMoveToPlay()
    {
        this.Close();
    }
    public void StartModeBoss()
    {

        if (GlobalData.gameMode == GameMode.BossWorld && User.Instance[ItemID.PlayingLevel] < 5)
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
            {
                if (isSuccess)
                {
                    PopupManager.Instance.OpenPopup<PopupSelectBoss>(PopupID.PopupSelectBoss);
                    User.Instance[ItemID.AdsBossMode] = 1;
                    User.Instance.Save();
                    base.Close();
                }
            }, AdLocation.PlayModeBoss);
        }
        else if (GlobalData.gameMode == GameMode.BossWorld && User.Instance[ItemID.PlayingLevel] >= 5 )
        {
            PopupManager.Instance.OpenPopup<PopupSelectBoss>(PopupID.PopupSelectBoss);
            base.Close();
        }
    }

    public void StartModeEndless()
    {
        if (GlobalData.gameMode == GameMode.Endless)
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
            {
                if (isSuccess)
                {
                    // SHOW ADS
                    GameEvent.OnMoveToPlay.Invoke();
                    base.Close();
                }

            }, AdLocation.PlayModeEndless);
            User.Instance[ItemID.AdsEndLessMode] = 1;
            User.Instance.Save();
        }
        else if (GlobalData.gameMode == GameMode.Endless && User.Instance[ItemID.PlayingLevel] >= 6)
        {
            GameEvent.OnMoveToPlay.Invoke();
            User.Instance.Save();
            base.Close();
        }

    }

    public void StartModeCollect()
    {
        //if (GlobalData.gameMode == GameMode.CollectFuel)
        //{
        //    if (User.Instance[ItemID.AdsCollectMode] < 10)
        //    {
        //        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
        //        {
        //            if (isSuccess)
        //            {
        //                // SHOW ADS
        //                ClickPlayCollect();
        //                base.Close();
        //                //PopupManager.Instance.OpenPopup<PopupSelectBoss>(PopupID.PopupSelectBoss);
        //            }

        //        }, AdLocation.PlayModeCollect);
        //    }
        //    else
        //    {
        //        txtWarning.text = "Played all the rounds for the day";
        //        //StartCoroutine(SetTrueWarning());
        //        PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice, (pop) => pop.SetData("The turn is over !", "Play is over for the day"));

        //    }

        //}


        User.Instance[ItemID.AdsCollectMode] += 1;
        GameEvent.OnMoveToPlay.Invoke();
        User.Instance.Save();
        base.Close();
    }

    public void OnCenterPanel()
    {

        if (scrollSnap.Panels[scrollSnap.CenteredPanel].name == "Normalmode")
        {
            mode.nameMode = NameMode.CampaignMode;
            txtNameMode.text = "CAMPAIGN";
            SetMat(0);
            SetIconTrue(0);
            txtTypeLockPlay.text = "FIGHT";

            txtReward.text = "Stage " + (User.Instance[ItemID.PlayingLevel] + 1).ToString();
            imgads.gameObject.SetActive(false);
            imglockPlay.gameObject.SetActive(false);
            iconTypelock.gameObject.SetActive(false);
            GlobalData.gameMode = GameMode.Normal;
            User.Instance.Save();
        }


        else if (scrollSnap.Panels[scrollSnap.CenteredPanel].name == "EndlessMode")
        {
            mode.nameMode = NameMode.EndlessMode;
            txtNameMode.text = "ENDLESS";
            txtTypeLockPlay.text = "FIGHT";

            SetMat(1);
            SetIconTrue(1);
            SetUpIconEndLess();
            GlobalData.gameMode = GameMode.Endless;
            User.Instance.Save();

        }


        else if (scrollSnap.Panels[scrollSnap.CenteredPanel].name == "CollectionMode")
        {
            mode.nameMode = NameMode.CollectionMode;
            txtNameMode.text = "COLLECTION";
            txtReward.text = "Kill zombies to earn gold";
            iconTypelock.gameObject.SetActive(false);
            imgads.gameObject.SetActive(true);
            SetMat(2);
            SetIconTrue(2);

            txtTypeLockPlay.text = User.Instance[ItemID.AdsCollectMode].ToString() + "/10";
            GlobalData.gameMode = GameMode.CollectFuel;
            User.Instance.Save();
            /*    if (User.Instance[ItemID.PlayingLevel] <= 5)
                {
                    txtReward.text = "Level 7 to unlock";
                    iconTypelock.gameObject.SetActive(true);
                }
                else if(User.Instance[ItemID.PlayingLevel] >= 6)
                {

                }*/
        }
        else if (scrollSnap.Panels[scrollSnap.CenteredPanel].name == "BossMode")
        {
            mode.nameMode = NameMode.BossMode;
            txtTypeLockPlay.text = "FIGHT";
            txtNameMode.text = "BOSS";
            SetMat(3);
            SetIconTrue(3);
            SetUpIconBoss();


            GlobalData.gameMode = GameMode.BossWorld;
            User.Instance.Save();
        }
    }
    public void OnCenterSnap()
    {
    }

    public void SetUpIconEndLess()
    {

        if (User.Instance[ItemID.PlayingLevel] < 5 && User.Instance[ItemID.AdsEndLessMode] == 0)
        {
            txtReward.text = "Clear stage 5 to unlock";
            imgads.gameObject.SetActive(true);
            iconTypelock.gameObject.SetActive(true);

        }
        if (User.Instance[ItemID.PlayingLevel] >= 5 || User.Instance[ItemID.AdsEndLessMode] == 1)
        {
            txtReward.text = "Kill zombies to receive items";
            imgads.gameObject.SetActive(false);
            iconTypelock.gameObject.SetActive(false);
        }
    }

    public void SetUpIconBoss()
    {
        if (User.Instance[ItemID.PlayingLevel] < 5 && User.Instance[ItemID.AdsBossMode] == 0)
        {
            txtReward.text = "Clear stage 5 to unlock";
            imgads.gameObject.SetActive(true);
            iconTypelock.gameObject.SetActive(true);

        }
        if (User.Instance[ItemID.PlayingLevel] >= 5 || User.Instance[ItemID.AdsBossMode] == 1)
        {

            txtReward.text = "Kill Boss to receive items";
            imgads.gameObject.SetActive(false);
            iconTypelock.gameObject.SetActive(false);
        }
    }
    public void SetMat(int index)
    {
        foreach (GameObject obj in lstCell)
        {
            obj.GetComponent<CellSelectMode>().btnIcon.material = obj.GetComponent<CellSelectMode>().gray;
            lstCell[index].GetComponent<CellSelectMode>().btnIcon.material = null;
        }
    }

    public void SetIconTrue(int Icon1)
    {
        for (int i = 0; i < lstIcon.Length; i++)
        {
            if (i == Icon1)
            {
                lstIcon[i].gameObject.SetActive(true);
            }
            else
            {
                lstIcon[i].gameObject.SetActive(false);
            }
        }
    }

    IEnumerator SetTrueWarning()
    {
        txtWarning.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        txtWarning.gameObject.SetActive(false);
        
        
    }

    public void ClickPlayCollect()
    {
        User.Instance[ItemID.AdsCollectMode] += 1;
        GameEvent.OnMoveToPlay.Invoke();
        User.Instance.Save();
      
        base.Close();
    }
}

[Serializable]
public class Mode
{
    public NameMode nameMode;
    public int levelUnlock;
    public bool isAds;
}


public enum NameMode
{
    CampaignMode,
    EndlessMode,
    CollectionMode,
    BossMode,
}