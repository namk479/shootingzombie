using System.Collections;
using Thanh.Core;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;

public class RevivePopup : Popup
{
    public int timeCount;
    public TMP_Text timeText;
    public Button revive;
    public Image imgfill;
    public Button nothanks;


    private void OnEnable()
    {
        revive.onClick.RemoveListener(Revive);
        revive.onClick.AddListener(Revive);
        nothanks.onClick.RemoveListener(Nothanks);
        nothanks.onClick.AddListener(Nothanks);
    }

    public override void OnShow()
    {
        base.OnShow();
        SetUp();
    }

    public void SetUp()
    {
        timeCount = 10;
        imgfill.fillAmount = 1f;
        nothanks.gameObject.SetActive(false);
        StartCoroutine(Countdown());

    }

    public void Nothanks()
    {
        this.Close();
        if (GlobalData.gameMode == GameMode.Normal)
        {
            GameManager.Instance.LoseNormalMode();
        }
        else if (GlobalData.gameMode == GameMode.BossWorld)
        {
            GameManager.Instance.LoseBossWorldMode();
        }
        else if (GlobalData.gameMode == GameMode.Endless)
        {
            GameManager.Instance.LoseEndlessMode();
        }
    }

    public void Revive()
    {
        BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
        {
            if (isSuccess)
            {
                base.Close();
                GameEvent.OnRevive.Invoke();
                if(GlobalData.gameMode == GameMode.BossWorld)
                {
                    QuestManager.Instance.isUsingReviveModeBoss = true;
                }
            }

        }, AdLocation.Revive);

    }

    public override void Close()
    {
        base.Close();
       
    }

    IEnumerator Countdown()
    {
        timeText.text = timeCount.ToString();

        imgfill.DOFillAmount(0f, 10f);
        while (timeCount > 0)
        {
            yield return new WaitForSeconds(1);
            timeCount -= 1;
            if(timeCount <= 7)
            {
                nothanks.gameObject.SetActive(true);
            }
            timeText.text = timeCount.ToString();
            if (timeCount <= 0)
            {
                this.Close();
                if (GlobalData.gameMode == GameMode.Normal)
                {
                    GameManager.Instance.LoseNormalMode();
                }
                else if(GlobalData.gameMode == GameMode.BossWorld)
                {
                    GameManager.Instance.LoseBossWorldMode();
                }
                else if(GlobalData.gameMode == GameMode.Endless)
                {
                    GameManager.Instance.LoseEndlessMode();
                }
            }

        }
    }
}
