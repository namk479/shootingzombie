using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageLeveControler : MonoBehaviour
{
    public List<CellTageLevel> cells = new List<CellTageLevel>();

    public Sprite[] mapsIcon;
    //map
    public Image currentMap;
    public Image nextMap;


    public void OnSetMap(int map)
    {
        if(map == 0)
        {
            currentMap.sprite = mapsIcon[0];
            nextMap.sprite = mapsIcon[1];
        }
        else
        {
            currentMap.sprite = mapsIcon[1];
            nextMap.sprite = mapsIcon[0];
        }
    }

    public void OnEnable()
    {
        GameEvent.OnSetMap.RemoveListener(OnSetMap);
        GameEvent.OnSetMap.AddListener(OnSetMap);

        SetUp();
    }

    public void SetUp()
    {
        if ((int)(User.Instance[ItemID.PlayingLevel] / 5) == 0)
        {
            for (int i = 1; i <= 5; i++)
            {
                cells[i - 1].SetUp(i);
            }
        }
        else
        {
            int levelDau = (5 * ((int)(User.Instance[ItemID.PlayingLevel] / 5)) +1);
            int levelCuoi = levelDau + 4;
            for(int i = levelDau; i <=levelCuoi; i++)
            {
                cells[i - levelDau].SetUp(i);
            }
        }
    }
}
