public class BossPowerUpState : IState
{
    private readonly EnemyBase boss;

    public BossPowerUpState(EnemyBase boss)
    {
        this.boss = boss;
    }
    public void EnterState()
    {
        boss.EnterPowerUp();
    }

    public void UpdateState()
    {
        boss.UpdatePowerUp();
    }

    public void ExitState()
    {
        boss.ExitPowerUp();
    }
}
