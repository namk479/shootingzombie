using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class TryBoosterButton : MonoBehaviour
{
    public Booster booster;
    public PopupTryHero popupTry;
    public GameObject adsIcon;

    private void OnEnable()
    {
        this.transform.localScale = Vector3.zero;
        this.transform.DOScale(new Vector3(1f, 1f, 1), 0.5f);
        adsIcon.SetActive(User.Instance[ItemID.PlayingLevel] >= 2);
    }


    public void Select()
    {
        if (User.Instance[ItemID.PlayingLevel] < 2)
        {
            GameManager.Instance.isSelectTryHero = true;
            BoosterManager.instance.listBoost.Add(this.booster.booster);
            BoosterManager.instance.boostersSelected.Add(this.booster);
            GameEvent.OnSelectBooster.Invoke();
            popupTry.Close();
            if (booster.booster == NameBooster.Strange)
            {
                QuestManager.Instance.isUsingWitch = true;
            }
            else if (booster.booster == NameBooster.Drone)
            {
                QuestManager.Instance.isUsingDrone = true;
            }
        }
        else
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, isSuccess =>
            {
                if (isSuccess)
                {
                    GameManager.Instance.isSelectTryHero = true;
                    BoosterManager.instance.listBoost.Add(this.booster.booster);
                    BoosterManager.instance.boostersSelected.Add(this.booster);
                    GameEvent.OnSelectBooster.Invoke();
                    popupTry.Close();
                    if (booster.booster == NameBooster.Strange)
                    {
                        QuestManager.Instance.isUsingWitch = true;
                    }
                    else if (booster.booster == NameBooster.Drone)
                    {
                        QuestManager.Instance.isUsingDrone = true;
                    }
                }

            }, AdLocation.TruyBooster);
        }
    }
}
