using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RewardConfig", menuName = "Reward/BossMode", order = 1)]
public class RewardBossTableData : ScriptableObject
{
    public List<LevelBossReward> rewardConfig;
    public RewardStageTableData rewardState;
    public void OnValidate()
    {
        rewardConfig = new List<LevelBossReward>(); // Initialize the list

        for (int i = 0; i < 10; i++)
        {
           
           if(i<= 4)
            {
                rewardConfig.Add(new LevelBossReward { gold = rewardState.goldNormalMode[rewardState.RewardBoss[i]] * 5, ticket = 1 });
            }
           else if(i > 4 && i <= 10)
            {
                rewardConfig.Add(new LevelBossReward { gold = rewardState.goldNormalMode[rewardState.RewardBoss[i]] * 5, ticket = 2});
            }
        }
    }
}


[System.Serializable]
public class LevelBossReward
{
    public int gold;
    public int ticket;
    public ItemID skin;
}
