using UnityEngine;
[CreateAssetMenu(fileName = "ShopInventory", menuName = "ShopIventory/ShopData", order = 1)]
public class ShopInventory : ScriptableObject
{
    public ItemID itemID;
    public ItemValue itemcell;
    public Sprite spriteIcon;
    public bool isClaimShop = false;
    public int index;
}
