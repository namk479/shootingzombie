﻿using System.Collections.Generic;
using Thanh.Core;
using UnityEngine;
using UnityEngine.UI;

public class PopupShop : Popup
{
    [SerializeField] private Button btnGoldIAP;
    [SerializeField] private Button btnHammerIAP;
    [SerializeField] private Button btnGoldADS;
    [SerializeField] private Button btnHammerADS;
    [SerializeField] private Sprite btnOn;
    [SerializeField] private Sprite btnOff;
    [SerializeField] private GameObject prefabCell;
    [SerializeField] private Transform parentCell;
    [SerializeField] public TypeShop TypeProduct;

    public List<ShopInventory> _ShopGoldIAP = new List<ShopInventory>();
    public List<ShopInventory> _ShopHammerIAP = new List<ShopInventory>();
    public List<ShopInventory> _ShopGold = new List<ShopInventory>();
    public List<ShopInventory> _ShopHammer = new List<ShopInventory>();

    public void ButtonGoldIAP()
    {
        SetSpriteButton();
        TypeProduct = TypeShop.ShopGold_IAP;
        btnGoldIAP.image.sprite = btnOn;
        SpawnCell(_ShopGoldIAP);
    }
    public void ButtonHammerIAP()
    {
        SetSpriteButton();
        TypeProduct = TypeShop.ShopHammer_IAP;
        btnHammerIAP.image.sprite = btnOn;
        SpawnCell(_ShopHammerIAP);
    }
    public void ButtonGoldAds()
    {
        SetSpriteButton();
        TypeProduct = TypeShop.ShopGold;
        btnGoldADS.image.sprite = btnOn;
        SpawnCell(_ShopGold);
    }
    public void ButtonHammerAds()
    {
        SetSpriteButton();
        TypeProduct = TypeShop.ShopHammer;
        btnHammerADS.image.sprite = btnOn;
        SpawnCell(_ShopHammer);
    }

    public void SetSpriteButton()
    {
        btnGoldIAP.image.sprite = btnOff;
        btnHammerIAP.image.sprite = btnOff;
        btnHammerADS.image.sprite = btnOff;
        btnGoldADS.image.sprite = btnOff;
    }

    public void SpawnCell(List<ShopInventory> listProData)
    {
        foreach (Transform trans in parentCell)
        {
            if (trans != null)
            {
                Destroy(trans.gameObject);
            }
        }

        for (int i = 1; i <= listProData.Count; i++)
        {
            IconShopInven iconshopivent = Instantiate(prefabCell, parentCell).GetComponent<IconShopInven>();
            iconshopivent.SetUp(listProData[i-1], TypeProduct);
        }
    }



    /*if (User.Instance[ItemID.adsShopGoldWatched] == 0)
            {
                listProData[i].isClaimShop = false;
                iconshopivent.Tick.gameObject.SetActive(false);
                iconshopivent.indexSttIcon.gameObject.SetActive(true);
                iconshopivent.SetUp(listProData[i], TypeProduct);
            }*/

    //[SerializeField] private Button btnLstGold;
    //[SerializeField] private Button btnLstHammer;


    //[SerializeField] private Button btnShopGold;
    //[SerializeField] private Button btnShopHammer;

    //public List<ShopInventory> _ShopGold = new List<ShopInventory>();
    //public List<ShopInventory> _ShopHammer = new List<ShopInventory>();


    //[SerializeField] private Transform gunGroup;
    //[SerializeField] public TypeShop TypeInventory;
    //[SerializeField] private GameObject prefabCell;
    //[SerializeField] private Sprite btnOn;
    //[SerializeField] private Sprite btnOff;


    //public List<IconShopInven> listItemShopGold = new List<IconShopInven>();
    //public List<IconShopInven> listItemShopHammer = new List<IconShopInven>();

    //private void OnEnable()
    //{
    //    btnLstGold.onClick.RemoveListener(ClickbtnGold);
    //    btnLstGold.onClick.AddListener(ClickbtnGold);

    //    btnLstHammer.onClick.RemoveListener(ClickbtnHammer);
    //    btnLstHammer.onClick.AddListener(ClickbtnHammer);



    //   // GameEvent.OnBuyShopAds.RemoveListener(Setup);
    //   // GameEvent.OnBuyShopAds.AddListener(Setup);


    //}


    //public void SetData(string type)
    //{
    //    if(type == "Gold")
    //    {
    //        ClickbtnGold();
    //    }
    //    else if(type == "Hammer")
    //    {
    //        ClickbtnHammer();
    //    }
    //}


    //public void SetUpGoldFree()
    //{
    //    foreach (ShopInventory shopGold in _ShopGold)
    //    {
    //        IconShopInven iconshopivent = Instantiate(prefabCell, gunGroup).GetComponent<IconShopInven>();
    //        iconshopivent.transform.SetParent(gunGroup);
    //        iconshopivent.SetUp(shopGold, TypeInventory);
    //        listItemShopGold.Add(iconshopivent);
    //        iconshopivent.indexStt.text = shopGold.indexStt.ToString();
    //        if (User.Instance[ItemID.adsShopGoldWatched] == 0)
    //        {
    //            shopGold.isClaimShop = false;
    //            iconshopivent.Tick.gameObject.SetActive(false);
    //            iconshopivent.indexSttIcon.gameObject.SetActive(true);
    //            iconshopivent.SetUp(shopGold, TypeInventory);
    //        }
    //    }

    //    listItemShopGold[User.Instance[ItemID.adsShopGoldWatched]].SetUpGoldByAds();
    //}

    //public void SetUpHammerFree()
    //{
    //    foreach (ShopInventory shopGold in _ShopHammer)
    //    {
    //        IconShopInven iconshopivent = Instantiate(prefabCell, gunGroup).GetComponent<IconShopInven>();
    //        iconshopivent.transform.SetParent(gunGroup);// = gunGroup;
    //        listItemShopHammer.Add(iconshopivent);
    //        iconshopivent.SetUp(shopGold, TypeInventory);
    //        iconshopivent.indexStt.text = shopGold.indexStt.ToString();
    //        if (User.Instance[ItemID.adsShopHammerWatched] == 0)
    //        {
    //            shopGold.isClaimShop = false;
    //            iconshopivent.Tick.gameObject.SetActive(false);
    //            iconshopivent.indexSttIcon.gameObject.SetActive(true);
    //            iconshopivent.SetUp(shopGold, TypeInventory);

    //        }
    //    }
    //    listItemShopHammer[User.Instance[ItemID.adsShopHammerWatched]].SetUpGoldByAds();
    //}


    //public void ClearShopItem()
    //{
    //    foreach (Transform trans in gunGroup)
    //    {
    //        Destroy(trans.gameObject);
    //    }
    //    listItemShopGold.Clear();
    //    listItemShopHammer.Clear();
    //}

    //public void OnClaimAdsBuy()
    //{
    //    listItemShopGold[User.Instance[ItemID.adsShopGoldWatched]].SetUpGoldByAds();
    //    listItemShopHammer[User.Instance[ItemID.adsShopHammerWatched]].SetUpGoldByAds();
    //}

    //public void ClickbtnGold()
    //{
    //    TypeInventory = TypeShop.ShopGold;
    //    AudioManager.instance.Play("BtnClick");
    //    btnLstGold.image.sprite = btnOn;
    //    SetUpGoldFree();
    //}

    //public void ClickbtnHammer()
    //{
    //    TypeInventory = TypeShop.ShopHammer;
    //    AudioManager.instance.Play("BtnClick");
    //    btnLstHammer.image.sprite = btnOn;
    //    SetUpHammerFree();
    //}

    //public void SetSpriteBtn()
    //{
    //    btnLstGold.image.sprite = btnOff;
    //    btnLstHammer.image.sprite = btnOff;
    //}

}
public enum TypeShop
{
    ShopGold,
    ShopHammer,
    ShopGold_IAP,
    ShopHammer_IAP,
}
