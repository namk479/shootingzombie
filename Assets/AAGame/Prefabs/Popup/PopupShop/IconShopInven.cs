using AA_Game;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IconShopInven : Item
{
    public Image itemIcon;
    public TMP_Text txtPrice;
    public TMP_Text IconCollect;
    public ItemValue itemvalue;
    public ShopInventory inventory;

    public Button buttonBuy;

    [SerializeField] private Sprite btnShopYellow;
    [SerializeField] private Sprite btnShopGreen;
    [SerializeField] private Sprite btnShopBlue;

    [SerializeField] private Sprite btnShopLock;
    [SerializeField] public Image bg;
    [SerializeField] private GameObject FxIcon;

    public TMP_Text indexStt;
    public Material matterGray;
    //public bool isClaim;
    public Image bgGray;
    public GameObject Tick;
    public GameObject indexSttIcon;
    public GameObject IconNext;
    public GameObject IconAds;

    public TypeShop typeShop;

    private void OnEnable()
    {
        buttonBuy.onClick.RemoveListener(OnClaim);
        buttonBuy.onClick.AddListener(OnClaim);

        GameEvent.OnBuyShopAds.RemoveListener(ResetUp);
        GameEvent.OnBuyShopAds.AddListener(ResetUp);
    }

    public void SetCurrentPack()
    {
        bg.sprite = btnShopYellow;
        buttonBuy.interactable = true;
        bg.color = Color.white;
        bg.material = null;
    }

    public void SetUp(ShopInventory _shopinventory, TypeShop type)
    {
        this.inventory = _shopinventory;
        this.typeShop = type;
        IconCollect.text = inventory.itemcell.value.ToString();
        itemIcon.sprite = _shopinventory.spriteIcon;
        buttonBuy.interactable = false;

        if (ItemType.IsIapPack(_shopinventory.itemID))
        {
            txtPrice.text = InAppManager.Instance.GetPrice(_shopinventory.itemID);
            FxIcon.SetActive(true);
            if (type == TypeShop.ShopGold_IAP)
            {
                bg.sprite = btnShopGreen;
            }
            else if (type == TypeShop.ShopHammer_IAP)
            {
                bg.sprite = btnShopBlue;
            }
            buttonBuy.interactable = true;
            Tick.gameObject.SetActive(false);
            indexSttIcon.gameObject.SetActive(false);
            IconNext.gameObject.SetActive(false);
            IconAds.gameObject.SetActive(false);
        }
        else
        {
            txtPrice.text = "FREE";

            if (type == TypeShop.ShopGold)
            {
                ResetUp(User.Instance[ItemID.adsShopGoldWatched]);
            }
            else if (type == TypeShop.ShopHammer)
            {
                ResetUp(User.Instance[ItemID.adsShopHammerWatched]);
            }
            FxIcon.SetActive(false);
            IconNext.gameObject.SetActive(true);
        }
    }


    public void OnClaim()
    {
        int amountIcon = inventory.itemcell.value;
        inventory.isClaimShop = true;

        if (inventory.itemID == ItemID.Ads)
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(ItemID.Ads, 1) }, null, (isSuccess) =>
            {
                if (isSuccess)
                {
                    if (inventory.itemcell.item == ItemID.Gold)
                    {

                        PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem,
                            (pop) =>
                            {
                                pop.SetData(inventory.itemcell);
                                PopupClaimItem.Instane.btnX2.gameObject.SetActive(false);
                                PopupClaimItem.Instane.OkCollect.gameObject.SetActive(true);
                            });
                        User.Instance[ItemID.Gold] += amountIcon;
                        User.Instance[ItemID.adsShopGoldWatched] += 1;
                        if (User.Instance[ItemID.adsShopGoldWatched] == 5)
                        {
                            User.Instance[ItemID.adsShopGoldWatched] = 0;
                        }
                        GameEvent.OnBuyShopAds.Invoke(User.Instance[ItemID.adsShopGoldWatched]);

                    }
                    else if (inventory.itemcell.item == ItemID.thorAmount)
                    {
                        PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem,
                            (pop) =>
                            {
                                pop.SetData(inventory.itemcell);
                                PopupClaimItem.Instane.btnX2.gameObject.SetActive(false);
                                pop.btnNothanks.gameObject.SetActive(false);
                                PopupClaimItem.Instane.OkCollect.gameObject.SetActive(true);
                            });
                        User.Instance[ItemID.thorAmount] += amountIcon;
                        User.Instance[ItemID.adsShopHammerWatched] += 1;
                        if (User.Instance[ItemID.adsShopHammerWatched] == 5)
                        {
                            User.Instance[ItemID.adsShopHammerWatched] = 0;
                        }
                        GameEvent.OnBuyShopAds.Invoke(User.Instance[ItemID.adsShopHammerWatched]);

                    }
                    //Tick.gameObject.SetActive(true);
                    //indexSttIcon.gameObject.SetActive(false);

                }
                //else
                //{
                //    PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice,
                //        (pop) => { pop.SetData("RETRY", "Video Ads not available"); });
                //}
            }, AdLocation.ShopPack);

        }
        else if (ItemType.IsIapPack(inventory.itemID))
        {
            BuyManager.Instance.Buy(new List<ItemValueFloat> { new ItemValueFloat(inventory.itemID, 1) }, null, (isSuccess) =>
               {
                   if (isSuccess)
                   {
                       if (inventory.itemcell.item == ItemID.Gold)
                       {
                           PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem,
                               (pop) =>
                               {
                                   pop.SetData(inventory.itemcell);
                                   pop.btnNothanks.gameObject.SetActive(false);
                                   PopupClaimItem.Instane.btnX2.gameObject.SetActive(false);
                                   PopupClaimItem.Instane.OkCollect.gameObject.SetActive(true);
                               });
                           User.Instance[ItemID.Gold] += amountIcon;
                       }
                       else if (inventory.itemcell.item == ItemID.thorAmount)
                       {
                           PopupManager.Instance.OpenPopup<PopupClaimItem>(PopupID.PopupClaimItem,
                               (pop) =>
                               {
                                   pop.SetData(inventory.itemcell);
                                   PopupClaimItem.Instane.btnX2.gameObject.SetActive(false);
                                   pop.btnNothanks.gameObject.SetActive(false);
                                   PopupClaimItem.Instane.OkCollect.gameObject.SetActive(true);
                               });
                           User.Instance[ItemID.thorAmount] += amountIcon;
                       }
                       //isClaim = true;
                       Tick.gameObject.SetActive(false);
                       indexSttIcon.gameObject.SetActive(false);
                   }
                   //else
                   //{
                   //    PopupManager.Instance.OpenPopup<PopupNotice>(PopupID.PopupNotice,
                   //        (pop) => { pop.SetData("RETRY", "Payment failed, please try again"); });
                   //}
               }, AdLocation.ShopPack);
        }
    }

    public void ResetUp(int indexClaim)
    {
        buttonBuy.interactable = false;
        indexStt.text = (inventory.index + 1).ToString();
        if (this.inventory.index < indexClaim)
        {
            bg.material = null;
            bg.sprite = btnShopGreen;
            Tick.gameObject.SetActive(true);
            indexSttIcon.gameObject.SetActive(false);
        }
        else if(this.inventory.index == indexClaim)
        {
            SetCurrentPack();
            Tick.gameObject.SetActive(false);
            indexSttIcon.gameObject.SetActive(true);
        }
        else
        {
            bg.sprite = btnShopGreen;
            bg.material = matterGray;
            Tick.gameObject.SetActive(false);
            indexSttIcon.gameObject.SetActive(true);
        }
    }
}
