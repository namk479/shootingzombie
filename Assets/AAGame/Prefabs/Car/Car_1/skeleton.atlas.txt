
skeleton.png
size: 1637,1637
format: RGBA8888
filter: Linear,Linear
repeat: none
car_1
  rotate: false
  xy: 2, 745
  size: 1519, 890
  orig: 1519, 890
  offset: 0, 0
  index: -1
car_2
  rotate: false
  xy: 320, 231
  size: 268, 183
  orig: 268, 183
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 2, 416
  size: 1633, 327
  orig: 1633, 327
  offset: 0, 0
  index: -1
wheel
  rotate: false
  xy: 2, 98
  size: 316, 316
  orig: 316, 316
  offset: 0, 0
  index: -1
