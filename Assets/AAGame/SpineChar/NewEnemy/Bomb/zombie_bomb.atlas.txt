
zombie_bomb.png
size: 1000,1000
format: RGBA8888
filter: Linear,Linear
repeat: none
R_foot_fake
  rotate: false
  xy: 358, 545
  size: 71, 47
  orig: 71, 47
  offset: 0, 0
  index: -1
R_leg1_fake
  rotate: true
  xy: 714, 505
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
R_leg2_fake
  rotate: false
  xy: 358, 430
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
crack_1
  rotate: false
  xy: 2, 824
  size: 828, 173
  orig: 828, 173
  offset: 0, 0
  index: -1
crack_2
  rotate: false
  xy: 2, 706
  size: 828, 116
  orig: 828, 173
  offset: 0, 0
  index: -1
eye
  rotate: true
  xy: 360, 594
  size: 110, 72
  orig: 110, 72
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 970, 402
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
zbomb/L_foot_2
  rotate: true
  xy: 2, 2
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
zbomb/L_hand
  rotate: true
  xy: 790, 286
  size: 94, 111
  orig: 94, 111
  offset: 0, 0
  index: -1
zbomb/L_hand_3
  rotate: true
  xy: 2, 611
  size: 93, 111
  orig: 93, 111
  offset: 0, 0
  index: -1
zbomb/L_leg1_2
  rotate: false
  xy: 115, 611
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/L_leg1_3
  rotate: false
  xy: 164, 611
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/L_leg2_2
  rotate: true
  xy: 56, 6
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/L_leg2_3
  rotate: false
  xy: 213, 611
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/L_leg2_4
  rotate: true
  xy: 151, 6
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_foot
  rotate: true
  xy: 29, 2
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
zbomb/R_hand
  rotate: true
  xy: 790, 196
  size: 88, 105
  orig: 88, 105
  offset: 0, 0
  index: -1
zbomb/R_hand_3
  rotate: false
  xy: 897, 169
  size: 88, 104
  orig: 88, 104
  offset: 0, 0
  index: -1
zbomb/R_leg1
  rotate: false
  xy: 262, 611
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg1_1
  rotate: false
  xy: 311, 611
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg1_2
  rotate: true
  xy: 246, 6
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg1_3
  rotate: true
  xy: 341, 6
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg2
  rotate: true
  xy: 436, 16
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg2_1
  rotate: true
  xy: 531, 16
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/R_leg2_2
  rotate: true
  xy: 626, 16
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
zbomb/backpack
  rotate: true
  xy: 832, 902
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
zbomb/backpack_1
  rotate: true
  xy: 832, 805
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
zbomb/backpack_2
  rotate: true
  xy: 832, 708
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
zbomb/backpack_3
  rotate: true
  xy: 832, 514
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
zbomb/backpack_4
  rotate: true
  xy: 832, 611
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
zbomb/body
  rotate: false
  xy: 2, 333
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
zbomb/body_1
  rotate: false
  xy: 2, 55
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
zbomb/body_2
  rotate: false
  xy: 180, 333
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
zbomb/body_3
  rotate: true
  xy: 436, 528
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
zbomb/body_4
  rotate: false
  xy: 180, 55
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
zbomb/bomb1
  rotate: false
  xy: 358, 65
  size: 214, 239
  orig: 214, 239
  offset: 0, 0
  index: -1
zbomb/bomb2
  rotate: false
  xy: 574, 65
  size: 214, 239
  orig: 214, 239
  offset: 0, 0
  index: -1
zbomb/bomb3
  rotate: false
  xy: 903, 275
  size: 91, 105
  orig: 91, 105
  offset: 0, 0
  index: -1
zbomb/bomb_fx0
  rotate: false
  xy: 829, 382
  size: 139, 130
  orig: 141, 135
  offset: 2, 1
  index: -1
zbomb/bomb_fx1
  rotate: false
  xy: 714, 574
  size: 116, 130
  orig: 116, 132
  offset: 0, 1
  index: -1
zbomb/bomb_fx2
  rotate: false
  xy: 682, 384
  size: 134, 119
  orig: 136, 123
  offset: 0, 4
  index: -1
zbomb/hat_2
  rotate: true
  xy: 721, 16
  size: 47, 41
  orig: 47, 41
  offset: 0, 0
  index: -1
zbomb/hat_4
  rotate: false
  xy: 436, 306
  size: 244, 220
  orig: 244, 220
  offset: 0, 0
  index: -1
