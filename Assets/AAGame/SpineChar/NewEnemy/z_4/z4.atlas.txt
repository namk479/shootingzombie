
z4.png
size: 1032,1032
format: RGBA8888
filter: Linear,Linear
repeat: none
R_foot_fake
  rotate: true
  xy: 692, 5
  size: 71, 47
  orig: 71, 47
  offset: 0, 0
  index: -1
R_leg1_fake
  rotate: true
  xy: 568, 366
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
R_leg2_fake
  rotate: true
  xy: 683, 366
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
crack_1
  rotate: false
  xy: 2, 857
  size: 828, 173
  orig: 828, 173
  offset: 0, 0
  index: -1
crack_2
  rotate: false
  xy: 2, 739
  size: 828, 116
  orig: 828, 173
  offset: 0, 0
  index: -1
eye
  rotate: true
  xy: 536, 2
  size: 110, 72
  orig: 110, 72
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 358, 12
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
z4/L_hand
  rotate: false
  xy: 610, 5
  size: 80, 107
  orig: 80, 107
  offset: 0, 0
  index: -1
z4/L_leg1
  rotate: true
  xy: 239, 315
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/L_leg2
  rotate: true
  xy: 334, 315
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/L_leg2_fake
  rotate: true
  xy: 741, 9
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
z4/R_foot
  rotate: true
  xy: 212, 311
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
z4/R_hand
  rotate: true
  xy: 701, 544
  size: 96, 95
  orig: 96, 95
  offset: 0, 0
  index: -1
z4/R_leg1
  rotate: true
  xy: 429, 315
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg1_1
  rotate: true
  xy: 619, 317
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg1_2
  rotate: true
  xy: 524, 315
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg1_3
  rotate: true
  xy: 692, 80
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg1_4
  rotate: true
  xy: 714, 317
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg2
  rotate: false
  xy: 798, 547
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg2_1
  rotate: true
  xy: 701, 495
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg2_2
  rotate: false
  xy: 798, 452
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg2_3
  rotate: true
  xy: 856, 29
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/R_leg2_4
  rotate: true
  xy: 701, 446
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z4/backpack
  rotate: true
  xy: 2, 642
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z4/backpack_1
  rotate: true
  xy: 167, 642
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z4/backpack_2
  rotate: true
  xy: 497, 642
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z4/backpack_3
  rotate: true
  xy: 662, 642
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z4/backpack_4
  rotate: true
  xy: 332, 642
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z4/body
  rotate: false
  xy: 2, 22
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z4/body_1
  rotate: false
  xy: 180, 22
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z4/body_2
  rotate: false
  xy: 390, 364
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z4/body_3
  rotate: false
  xy: 358, 37
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z4/body_4
  rotate: false
  xy: 212, 364
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z4/body_die
  rotate: true
  xy: 832, 642
  size: 176, 187
  orig: 176, 187
  offset: 0, 0
  index: -1
z4/body_die_1
  rotate: false
  xy: 674, 129
  size: 176, 186
  orig: 176, 186
  offset: 0, 0
  index: -1
z4/body_die_2
  rotate: false
  xy: 852, 266
  size: 176, 186
  orig: 176, 186
  offset: 0, 0
  index: -1
z4/body_die_3
  rotate: false
  xy: 852, 78
  size: 176, 186
  orig: 176, 186
  offset: 0, 0
  index: -1
z4/body_die_4
  rotate: false
  xy: 852, 454
  size: 176, 186
  orig: 176, 186
  offset: 0, 0
  index: -1
z4/hat_1
  rotate: true
  xy: 568, 435
  size: 205, 131
  orig: 205, 131
  offset: 0, 0
  index: -1
z4/hat_2
  rotate: true
  xy: 832, 820
  size: 210, 197
  orig: 210, 197
  offset: 0, 0
  index: -1
z4/hat_3
  rotate: true
  xy: 536, 114
  size: 199, 136
  orig: 199, 136
  offset: 0, 0
  index: -1
z4/hat_4
  rotate: true
  xy: 2, 300
  size: 340, 208
  orig: 340, 208
  offset: 0, 0
  index: -1
