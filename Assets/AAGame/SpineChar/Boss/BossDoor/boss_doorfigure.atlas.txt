
boss_doorfigure.png
size: 880,880
format: RGBA8888
filter: Linear,Linear
repeat: none
L_arm_1
  rotate: true
  xy: 357, 763
  size: 115, 487
  orig: 115, 490
  offset: 0, 2
  index: -1
L_arm_2
  rotate: true
  xy: 357, 333
  size: 103, 331
  orig: 103, 333
  offset: 0, 2
  index: -1
L_arm_3
  rotate: false
  xy: 357, 438
  size: 137, 323
  orig: 137, 323
  offset: 0, 0
  index: -1
L_foot
  rotate: false
  xy: 690, 275
  size: 181, 166
  orig: 181, 168
  offset: 0, 2
  index: -1
L_hand
  rotate: false
  xy: 496, 441
  size: 176, 154
  orig: 176, 156
  offset: 0, 1
  index: -1
L_leg_1
  rotate: false
  xy: 496, 118
  size: 167, 213
  orig: 167, 216
  offset: 0, 2
  index: -1
L_leg_2
  rotate: true
  xy: 496, 597
  size: 164, 335
  orig: 165, 335
  offset: 0, 0
  index: -1
L_leg_3
  rotate: false
  xy: 665, 37
  size: 134, 236
  orig: 134, 236
  offset: 0, 0
  index: -1
R_hand
  rotate: false
  xy: 674, 443
  size: 145, 152
  orig: 145, 152
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 326
  size: 353, 552
  orig: 353, 552
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 2, 27
  size: 297, 271
  orig: 297, 271
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 275, 82
  size: 219, 242
  orig: 219, 243
  offset: 0, 1
  index: -1
shadow
  rotate: false
  xy: 2, 2
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
