
boss_toilet.png
size: 1128,1128
format: RGBA8888
filter: Linear,Linear
repeat: none
body_1
  rotate: false
  xy: 2, 593
  size: 482, 533
  orig: 483, 533
  offset: 0, 0
  index: -1
body_2
  rotate: false
  xy: 2, 154
  size: 360, 437
  orig: 361, 437
  offset: 0, 0
  index: -1
bomb_fx0
  rotate: true
  xy: 2, 13
  size: 139, 130
  orig: 141, 135
  offset: 2, 1
  index: -1
bomb_fx1
  rotate: false
  xy: 255, 22
  size: 116, 130
  orig: 116, 132
  offset: 0, 1
  index: -1
bomb_fx2
  rotate: true
  xy: 134, 18
  size: 134, 119
  orig: 136, 123
  offset: 0, 4
  index: -1
eyebrow
  rotate: true
  xy: 822, 5
  size: 195, 75
  orig: 195, 75
  offset: 0, 0
  index: -1
head_1
  rotate: false
  xy: 725, 348
  size: 391, 379
  orig: 391, 379
  offset: 0, 0
  index: -1
head_2
  rotate: false
  xy: 725, 729
  size: 401, 397
  orig: 403, 398
  offset: 1, 0
  index: -1
head_3
  rotate: true
  xy: 364, 176
  size: 415, 359
  orig: 416, 406
  offset: 0, 0
  index: -1
mouth_1
  rotate: false
  xy: 486, 53
  size: 109, 121
  orig: 109, 121
  offset: 0, 0
  index: -1
mouth_2
  rotate: false
  xy: 373, 53
  size: 111, 121
  orig: 111, 121
  offset: 0, 0
  index: -1
neck_1
  rotate: true
  xy: 725, 202
  size: 65, 366
  orig: 65, 366
  offset: 0, 0
  index: -1
neck_2
  rotate: true
  xy: 725, 269
  size: 77, 370
  orig: 77, 370
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 1097, 236
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
tnt
  rotate: false
  xy: 725, 3
  size: 95, 197
  orig: 95, 197
  offset: 0, 0
  index: -1
