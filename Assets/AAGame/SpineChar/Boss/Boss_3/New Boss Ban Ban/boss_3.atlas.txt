
boss_3.png
size: 844,844
format: RGBA8888
filter: Linear,Linear
repeat: none
1/L_arm_1
  rotate: false
  xy: 412, 236
  size: 95, 150
  orig: 95, 151
  offset: 0, 1
  index: -1
1/L_arm_1_fake
  rotate: true
  xy: 255, 108
  size: 115, 170
  orig: 115, 171
  offset: 0, 1
  index: -1
1/L_arm_2
  rotate: false
  xy: 255, 225
  size: 155, 161
  orig: 155, 170
  offset: 0, 8
  index: -1
1/L_eye
  rotate: false
  xy: 787, 280
  size: 54, 60
  orig: 54, 68
  offset: 0, 4
  index: -1
1/L_hand
  rotate: false
  xy: 2, 169
  size: 251, 217
  orig: 251, 217
  offset: 0, 0
  index: -1
1/L_leg_1
  rotate: false
  xy: 525, 432
  size: 73, 216
  orig: 73, 216
  offset: 0, 0
  index: -1
1/L_leg_1_fake
  rotate: true
  xy: 2, 74
  size: 93, 236
  orig: 93, 236
  offset: 0, 0
  index: -1
1/R_arm_1
  rotate: true
  xy: 525, 335
  size: 95, 150
  orig: 95, 151
  offset: 0, 1
  index: -1
1/R_arm_1_fake
  rotate: true
  xy: 427, 119
  size: 115, 171
  orig: 115, 171
  offset: 0, 0
  index: -1
1/R_arm_2
  rotate: false
  xy: 735, 672
  size: 103, 170
  orig: 103, 171
  offset: 0, 1
  index: -1
1/R_arm_2_fake
  rotate: false
  xy: 610, 651
  size: 123, 191
  orig: 123, 191
  offset: 0, 0
  index: -1
1/R_eye
  rotate: false
  xy: 787, 234
  size: 49, 44
  orig: 58, 48
  offset: 9, 1
  index: -1
1/R_hand
  rotate: false
  xy: 430, 650
  size: 178, 192
  orig: 178, 193
  offset: 0, 1
  index: -1
1/R_leg_1
  rotate: false
  xy: 675, 433
  size: 73, 216
  orig: 73, 216
  offset: 0, 0
  index: -1
1/R_leg_1_fake
  rotate: false
  xy: 430, 412
  size: 93, 236
  orig: 93, 236
  offset: 0, 0
  index: -1
1/R_leg_2
  rotate: true
  xy: 528, 9
  size: 108, 197
  orig: 108, 198
  offset: 0, 1
  index: -1
1/armor
  rotate: false
  xy: 668, 142
  size: 99, 90
  orig: 114, 104
  offset: 7, 7
  index: -1
1/armor_fake
  rotate: false
  xy: 240, 2
  size: 114, 104
  orig: 114, 104
  offset: 0, 0
  index: -1
1/body
  rotate: false
  xy: 2, 388
  size: 212, 454
  orig: 212, 454
  offset: 0, 0
  index: -1
1/eye
  rotate: false
  xy: 735, 653
  size: 13, 17
  orig: 15, 18
  offset: 1, 1
  index: -1
1/mouth
  rotate: false
  xy: 2, 9
  size: 125, 63
  orig: 125, 63
  offset: 0, 0
  index: -1
1/tongue
  rotate: true
  xy: 129, 6
  size: 66, 107
  orig: 70, 107
  offset: 0, 0
  index: -1
1/tongue_fake
  rotate: false
  xy: 600, 129
  size: 66, 107
  orig: 70, 107
  offset: 0, 0
  index: -1
2/L_leg_1
  rotate: false
  xy: 600, 432
  size: 73, 216
  orig: 73, 216
  offset: 0, 0
  index: -1
2/R_arm_1
  rotate: true
  xy: 509, 238
  size: 95, 150
  orig: 95, 151
  offset: 0, 1
  index: -1
2/R_arm_2
  rotate: true
  xy: 356, 3
  size: 103, 170
  orig: 103, 171
  offset: 0, 1
  index: -1
2/R_leg_1
  rotate: false
  xy: 750, 454
  size: 73, 216
  orig: 73, 216
  offset: 0, 0
  index: -1
2/R_leg_2
  rotate: false
  xy: 677, 234
  size: 108, 197
  orig: 108, 198
  offset: 0, 1
  index: -1
2/body
  rotate: false
  xy: 216, 388
  size: 212, 454
  orig: 212, 454
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 787, 342
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
