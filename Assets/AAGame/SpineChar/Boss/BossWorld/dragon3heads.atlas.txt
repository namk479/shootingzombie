
dragon3heads.png
size: 2744,2744
format: RGBA8888
filter: Linear,Linear
repeat: none
2/L_arm_1
  rotate: false
  xy: 2, 1252
  size: 364, 446
  orig: 364, 446
  offset: 0, 0
  index: -1
2/L_arm_2
  rotate: true
  xy: 2482, 2392
  size: 350, 260
  orig: 350, 260
  offset: 0, 0
  index: -1
2/L_arm_3
  rotate: true
  xy: 1324, 330
  size: 252, 197
  orig: 252, 197
  offset: 0, 0
  index: -1
2/L_wing_1
  rotate: false
  xy: 557, 1049
  size: 308, 304
  orig: 308, 313
  offset: 0, 9
  index: -1
2/L_wing_2
  rotate: false
  xy: 2, 14
  size: 397, 306
  orig: 397, 306
  offset: 0, 0
  index: -1
2/R_arm_1
  rotate: false
  xy: 389, 2222
  size: 384, 520
  orig: 384, 520
  offset: 0, 0
  index: -1
2/R_arm_2
  rotate: false
  xy: 922, 584
  size: 255, 268
  orig: 255, 268
  offset: 0, 0
  index: -1
2/R_leg_1
  rotate: false
  xy: 2623, 1390
  size: 75, 107
  orig: 75, 107
  offset: 0, 0
  index: -1
2/R_leg_3
  rotate: true
  xy: 1530, 9
  size: 119, 156
  orig: 119, 156
  offset: 0, 0
  index: -1
2/R_wing_1
  rotate: false
  xy: 557, 1355
  size: 299, 417
  orig: 299, 417
  offset: 0, 0
  index: -1
2/R_wing_2
  rotate: true
  xy: 1983, 1604
  size: 163, 273
  orig: 163, 273
  offset: 0, 0
  index: -1
2/R_wing_3
  rotate: false
  xy: 1237, 1842
  size: 481, 308
  orig: 481, 308
  offset: 0, 0
  index: -1
2/body
  rotate: false
  xy: 775, 2341
  size: 418, 401
  orig: 418, 401
  offset: 0, 0
  index: -1
2/head_1
  rotate: true
  xy: 534, 825
  size: 222, 279
  orig: 222, 279
  offset: 0, 0
  index: -1
2/head_2
  rotate: true
  xy: 683, 58
  size: 367, 353
  orig: 367, 353
  offset: 0, 0
  index: -1
2/head_3
  rotate: true
  xy: 1295, 55
  size: 273, 233
  orig: 273, 233
  offset: 0, 0
  index: -1
2/jaw_1
  rotate: true
  xy: 2101, 68
  size: 212, 153
  orig: 234, 182
  offset: 15, 7
  index: -1
2/jaw_1_2
  rotate: false
  xy: 1713, 966
  size: 234, 182
  orig: 234, 182
  offset: 0, 0
  index: -1
2/jaw_2
  rotate: false
  xy: 1222, 985
  size: 244, 170
  orig: 255, 202
  offset: 7, 4
  index: -1
2/jaw_2_2
  rotate: false
  xy: 1222, 1157
  size: 255, 202
  orig: 255, 202
  offset: 0, 0
  index: -1
2/jaw_3
  rotate: false
  xy: 1901, 450
  size: 240, 168
  orig: 240, 168
  offset: 0, 0
  index: -1
2/neck_1_1
  rotate: false
  xy: 2165, 976
  size: 207, 205
  orig: 207, 205
  offset: 0, 0
  index: -1
2/neck_1_2
  rotate: false
  xy: 2374, 984
  size: 164, 197
  orig: 164, 197
  offset: 0, 0
  index: -1
2/neck_2_1
  rotate: true
  xy: 1917, 788
  size: 165, 241
  orig: 165, 241
  offset: 0, 0
  index: -1
2/neck_2_2
  rotate: false
  xy: 1482, 1356
  size: 225, 214
  orig: 225, 214
  offset: 0, 0
  index: -1
2/neck_3_1
  rotate: false
  xy: 2158, 510
  size: 207, 154
  orig: 207, 154
  offset: 0, 0
  index: -1
2/neck_3_2
  rotate: false
  xy: 1906, 75
  size: 193, 205
  orig: 193, 205
  offset: 0, 0
  index: -1
2/tail
  rotate: false
  xy: 1341, 2152
  size: 563, 187
  orig: 563, 187
  offset: 0, 0
  index: -1
3/L_arm_1
  rotate: false
  xy: 389, 1774
  size: 363, 446
  orig: 363, 446
  offset: 0, 0
  index: -1
3/L_arm_2
  rotate: true
  xy: 2482, 2040
  size: 350, 260
  orig: 350, 260
  offset: 0, 0
  index: -1
3/L_arm_3
  rotate: false
  xy: 1228, 1361
  size: 252, 197
  orig: 252, 197
  offset: 0, 0
  index: -1
3/L_wing_1
  rotate: true
  xy: 1720, 1842
  size: 308, 304
  orig: 308, 313
  offset: 0, 9
  index: -1
3/L_wing_2
  rotate: false
  xy: 2026, 1769
  size: 396, 306
  orig: 396, 306
  offset: 0, 0
  index: -1
3/R_arm_1
  rotate: false
  xy: 2, 2222
  size: 385, 520
  orig: 385, 520
  offset: 0, 0
  index: -1
3/R_arm_2
  rotate: false
  xy: 1179, 584
  size: 255, 268
  orig: 255, 268
  offset: 0, 0
  index: -1
3/R_leg_1
  rotate: true
  xy: 1906, 2167
  size: 75, 108
  orig: 75, 108
  offset: 0, 0
  index: -1
3/R_leg_3
  rotate: false
  xy: 922, 427
  size: 119, 155
  orig: 119, 155
  offset: 0, 0
  index: -1
3/R_wing_1
  rotate: true
  xy: 2035, 2077
  size: 299, 416
  orig: 299, 416
  offset: 0, 0
  index: -1
3/R_wing_2
  rotate: false
  xy: 2258, 1495
  size: 164, 272
  orig: 164, 272
  offset: 0, 0
  index: -1
3/R_wing_3
  rotate: true
  xy: 2, 322
  size: 481, 309
  orig: 481, 309
  offset: 0, 0
  index: -1
3/body
  rotate: false
  xy: 1195, 2341
  size: 418, 401
  orig: 418, 401
  offset: 0, 0
  index: -1
3/head_1
  rotate: true
  xy: 1043, 360
  size: 222, 279
  orig: 222, 279
  offset: 0, 0
  index: -1
3/head_2
  rotate: false
  xy: 858, 1486
  size: 368, 353
  orig: 368, 353
  offset: 0, 0
  index: -1
3/head_3
  rotate: false
  xy: 1709, 1607
  size: 272, 233
  orig: 272, 233
  offset: 0, 0
  index: -1
3/jaw_1
  rotate: false
  xy: 2165, 821
  size: 213, 153
  orig: 234, 182
  offset: 14, 7
  index: -1
3/jaw_1_2
  rotate: false
  xy: 1436, 800
  size: 234, 182
  orig: 234, 182
  offset: 0, 0
  index: -1
3/jaw_2
  rotate: false
  xy: 1468, 984
  size: 243, 170
  orig: 255, 202
  offset: 8, 4
  index: -1
3/jaw_2_2
  rotate: false
  xy: 1709, 1403
  size: 255, 202
  orig: 255, 202
  offset: 0, 0
  index: -1
3/jaw_3
  rotate: false
  xy: 1672, 624
  size: 241, 168
  orig: 241, 168
  offset: 0, 0
  index: -1
3/neck_1_1
  rotate: false
  xy: 1693, 417
  size: 206, 205
  orig: 206, 205
  offset: 0, 0
  index: -1
3/neck_1_2
  rotate: false
  xy: 2380, 785
  size: 164, 197
  orig: 164, 197
  offset: 0, 0
  index: -1
3/neck_2_1
  rotate: true
  xy: 1915, 620
  size: 166, 241
  orig: 166, 241
  offset: 0, 0
  index: -1
3/neck_2_2
  rotate: true
  xy: 1949, 955
  size: 226, 214
  orig: 226, 214
  offset: 0, 0
  index: -1
3/neck_3_1
  rotate: false
  xy: 2367, 510
  size: 206, 154
  orig: 206, 154
  offset: 0, 0
  index: -1
3/neck_3_2
  rotate: false
  xy: 2148, 303
  size: 192, 205
  orig: 192, 205
  offset: 0, 0
  index: -1
3/tail
  rotate: true
  xy: 368, 1135
  size: 563, 187
  orig: 563, 187
  offset: 0, 0
  index: -1
L_arm_1
  rotate: true
  xy: 2035, 2378
  size: 364, 445
  orig: 364, 445
  offset: 0, 0
  index: -1
L_arm_1_fake
  rotate: false
  xy: 2, 805
  size: 364, 445
  orig: 364, 445
  offset: 0, 0
  index: -1
L_arm_2
  rotate: false
  xy: 867, 854
  size: 350, 260
  orig: 350, 260
  offset: 0, 0
  index: -1
L_arm_2_fake
  rotate: true
  xy: 401, 14
  size: 370, 280
  orig: 370, 280
  offset: 0, 0
  index: -1
L_arm_3
  rotate: false
  xy: 1479, 1156
  size: 251, 198
  orig: 251, 198
  offset: 0, 0
  index: -1
L_arm_3_fake
  rotate: true
  xy: 1732, 1150
  size: 251, 198
  orig: 251, 198
  offset: 0, 0
  index: -1
L_wing_1
  rotate: false
  xy: 2424, 1734
  size: 309, 304
  orig: 309, 313
  offset: 0, 9
  index: -1
L_wing_2
  rotate: true
  xy: 614, 427
  size: 396, 306
  orig: 396, 306
  offset: 0, 0
  index: -1
R_arm_1
  rotate: false
  xy: 2, 1700
  size: 385, 520
  orig: 385, 520
  offset: 0, 0
  index: -1
R_arm_2
  rotate: false
  xy: 1038, 90
  size: 255, 268
  orig: 255, 268
  offset: 0, 0
  index: -1
R_arm_2_fake
  rotate: false
  xy: 1452, 1572
  size: 255, 268
  orig: 255, 268
  offset: 0, 0
  index: -1
R_leg_1
  rotate: true
  xy: 2222, 1418
  size: 75, 107
  orig: 75, 107
  offset: 0, 0
  index: -1
R_leg_1_fake
  rotate: true
  xy: 1906, 2244
  size: 95, 127
  orig: 95, 127
  offset: 0, 0
  index: -1
R_leg_1_fake2
  rotate: false
  xy: 2375, 666
  size: 91, 117
  orig: 96, 127
  offset: 5, 0
  index: -1
R_leg_3
  rotate: true
  xy: 1219, 863
  size: 120, 156
  orig: 120, 156
  offset: 0, 0
  index: -1
R_wing_1
  rotate: false
  xy: 313, 386
  size: 299, 417
  orig: 299, 417
  offset: 0, 0
  index: -1
R_wing_2
  rotate: false
  xy: 368, 860
  size: 164, 273
  orig: 164, 273
  offset: 0, 0
  index: -1
R_wing_3
  rotate: false
  xy: 754, 1841
  size: 481, 309
  orig: 481, 309
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 1615, 2341
  size: 418, 401
  orig: 418, 401
  offset: 0, 0
  index: -1
head_1
  rotate: false
  xy: 1228, 1560
  size: 222, 279
  orig: 222, 279
  offset: 0, 0
  index: -1
head_2
  rotate: true
  xy: 867, 1116
  size: 368, 353
  orig: 368, 353
  offset: 0, 0
  index: -1
head_3
  rotate: false
  xy: 2424, 1499
  size: 272, 233
  orig: 272, 233
  offset: 0, 0
  index: -1
jaw_1
  rotate: false
  xy: 2160, 666
  size: 213, 153
  orig: 234, 182
  offset: 14, 7
  index: -1
jaw_1_2
  rotate: false
  xy: 1436, 616
  size: 234, 182
  orig: 234, 182
  offset: 0, 0
  index: -1
jaw_2
  rotate: false
  xy: 1672, 794
  size: 243, 170
  orig: 254, 202
  offset: 7, 4
  index: -1
jaw_2_2
  rotate: false
  xy: 1966, 1400
  size: 254, 202
  orig: 254, 202
  offset: 0, 0
  index: -1
jaw_3
  rotate: true
  xy: 1523, 373
  size: 241, 168
  orig: 241, 168
  offset: 0, 0
  index: -1
neck_1_1
  rotate: true
  xy: 1698, 209
  size: 206, 205
  orig: 206, 205
  offset: 0, 0
  index: -1
neck_1_1_fake
  rotate: false
  xy: 1698, 2
  size: 206, 205
  orig: 206, 205
  offset: 0, 0
  index: -1
neck_1_2
  rotate: true
  xy: 2424, 1333
  size: 164, 197
  orig: 164, 197
  offset: 0, 0
  index: -1
neck_1_2_fake
  rotate: false
  xy: 2540, 1134
  size: 164, 197
  orig: 164, 197
  offset: 0, 0
  index: -1
neck_2_1
  rotate: false
  xy: 1530, 130
  size: 166, 241
  orig: 166, 241
  offset: 0, 0
  index: -1
neck_2_1_fake
  rotate: true
  xy: 1905, 282
  size: 166, 241
  orig: 166, 241
  offset: 0, 0
  index: -1
neck_2_2
  rotate: false
  xy: 1932, 1183
  size: 226, 215
  orig: 226, 215
  offset: 0, 0
  index: -1
neck_2_2_fake
  rotate: false
  xy: 2160, 1183
  size: 226, 215
  orig: 226, 215
  offset: 0, 0
  index: -1
neck_3_1
  rotate: true
  xy: 2256, 94
  size: 207, 154
  orig: 207, 154
  offset: 0, 0
  index: -1
neck_3_1_fake
  rotate: false
  xy: 2536, 354
  size: 206, 154
  orig: 206, 154
  offset: 0, 0
  index: -1
neck_3_2
  rotate: false
  xy: 2342, 303
  size: 192, 205
  orig: 192, 205
  offset: 0, 0
  index: -1
neck_3_2_fake
  rotate: false
  xy: 2412, 96
  size: 192, 205
  orig: 192, 205
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 389, 1749
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
tail
  rotate: false
  xy: 775, 2152
  size: 564, 187
  orig: 564, 187
  offset: 0, 0
  index: -1
tongue
  rotate: false
  xy: 2468, 673
  size: 101, 110
  orig: 101, 110
  offset: 0, 0
  index: -1
