
Treasure.png
size: 800,800
format: RGBA8888
filter: Linear,Linear
repeat: none
Ef_Neon
  rotate: false
  xy: 2, 403
  size: 398, 188
  orig: 461, 213
  offset: 35, 1
  index: -1
Ef_Ray
  rotate: false
  xy: 2, 593
  size: 506, 202
  orig: 541, 210
  offset: 12, 1
  index: -1
Ef_Star
  rotate: false
  xy: 268, 45
  size: 56, 59
  orig: 60, 62
  offset: 2, 1
  index: -1
Hand
  rotate: true
  xy: 396, 84
  size: 317, 81
  orig: 317, 81
  offset: 0, 0
  index: -1
Hand_Gold
  rotate: false
  xy: 479, 456
  size: 316, 57
  orig: 316, 57
  offset: 0, 0
  index: -1
box_a1
  rotate: false
  xy: 479, 200
  size: 263, 126
  orig: 263, 126
  offset: 0, 0
  index: -1
box_a2
  rotate: false
  xy: 510, 610
  size: 265, 185
  orig: 265, 185
  offset: 0, 0
  index: -1
box_a3
  rotate: false
  xy: 510, 515
  size: 281, 93
  orig: 281, 93
  offset: 0, 0
  index: -1
box_b1
  rotate: true
  xy: 268, 106
  size: 295, 126
  orig: 295, 126
  offset: 0, 0
  index: -1
box_b2
  rotate: false
  xy: 2, 216
  size: 264, 185
  orig: 264, 185
  offset: 0, 0
  index: -1
box_b3
  rotate: false
  xy: 479, 105
  size: 280, 93
  orig: 280, 93
  offset: 0, 0
  index: -1
box_c1
  rotate: false
  xy: 479, 328
  size: 295, 126
  orig: 295, 126
  offset: 0, 0
  index: -1
box_c2
  rotate: false
  xy: 2, 29
  size: 264, 185
  orig: 264, 185
  offset: 0, 0
  index: -1
box_c3
  rotate: false
  xy: 479, 10
  size: 280, 93
  orig: 280, 93
  offset: 0, 0
  index: -1
