
AK47.png
size: 972,972
format: RGBA8888
filter: Linear,Linear
repeat: none
AK_1
  rotate: false
  xy: 583, 377
  size: 257, 104
  orig: 257, 104
  offset: 0, 0
  index: -1
AK_2
  rotate: false
  xy: 280, 383
  size: 275, 95
  orig: 275, 95
  offset: 0, 0
  index: -1
AK_3
  rotate: false
  xy: 2, 743
  size: 293, 94
  orig: 293, 94
  offset: 0, 0
  index: -1
AK_4
  rotate: false
  xy: 308, 836
  size: 293, 134
  orig: 293, 134
  offset: 0, 0
  index: -1
AK_5
  rotate: false
  xy: 603, 839
  size: 291, 131
  orig: 291, 131
  offset: 0, 0
  index: -1
AK_6
  rotate: false
  xy: 2, 839
  size: 304, 131
  orig: 304, 131
  offset: 0, 0
  index: -1
R_foot_fake
  rotate: false
  xy: 896, 814
  size: 70, 44
  orig: 70, 44
  offset: 0, 0
  index: -1
R_leg1_fake
  rotate: true
  xy: 842, 405
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
R_leg2_fake
  rotate: true
  xy: 842, 336
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
char_deadpool/L_hand
  rotate: false
  xy: 879, 474
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_flash/L_hand
  rotate: false
  xy: 879, 474
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_ironman/L_hand
  rotate: false
  xy: 879, 474
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_red/L_hand
  rotate: false
  xy: 879, 474
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_spiderman/L_hand
  rotate: false
  xy: 879, 474
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_deadpool/R_foot
  rotate: true
  xy: 575, 677
  size: 51, 24
  orig: 51, 24
  offset: 0, 0
  index: -1
char_deadpool/R_hand
  rotate: false
  xy: 842, 207
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_flash/R_hand
  rotate: false
  xy: 842, 207
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_ironman/R_hand
  rotate: false
  xy: 842, 207
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_red/R_hand
  rotate: false
  xy: 842, 207
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_spiderman/R_hand
  rotate: false
  xy: 842, 207
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_deadpool/R_leg1
  rotate: true
  xy: 648, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_flash/R_leg1
  rotate: true
  xy: 648, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_ironman/R_leg1
  rotate: true
  xy: 648, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_red/R_leg1
  rotate: true
  xy: 648, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_deadpool/R_leg2
  rotate: true
  xy: 742, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_flash/R_leg2
  rotate: true
  xy: 742, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_ironman/R_leg2
  rotate: true
  xy: 742, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_red/R_leg2
  rotate: true
  xy: 742, 14
  size: 46, 92
  orig: 48, 94
  offset: 1, 1
  index: -1
char_deadpool/backpack
  rotate: true
  xy: 649, 280
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
char_flash/backpack
  rotate: true
  xy: 649, 280
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
char_spiderman/backpack
  rotate: true
  xy: 649, 280
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
char_deadpool/body
  rotate: true
  xy: 603, 661
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_deadpool/body_die
  rotate: true
  xy: 2, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
char_fireman/body_die
  rotate: true
  xy: 2, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
char_flash/body_die
  rotate: true
  xy: 2, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
char_ironman/body_die
  rotate: true
  xy: 2, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
char_red/body_die
  rotate: true
  xy: 2, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
char_fireman/L_hand
  rotate: false
  xy: 842, 127
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_fireman/R_foot
  rotate: true
  xy: 575, 783
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
char_fireman/R_hand
  rotate: false
  xy: 842, 47
  size: 78, 78
  orig: 78, 78
  offset: 0, 0
  index: -1
char_fireman/R_leg1
  rotate: false
  xy: 522, 288
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
char_fireman/R_leg2
  rotate: true
  xy: 649, 231
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
char_fireman/backpack
  rotate: false
  xy: 648, 62
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
char_fireman/body
  rotate: true
  xy: 297, 658
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_fireman/hat_1
  rotate: false
  xy: 2, 211
  size: 268, 174
  orig: 268, 174
  offset: 0, 0
  index: -1
char_fireman/hat_2
  rotate: true
  xy: 881, 554
  size: 258, 89
  orig: 258, 89
  offset: 0, 0
  index: -1
char_flash/R_foot
  rotate: true
  xy: 575, 624
  size: 51, 24
  orig: 51, 24
  offset: 0, 0
  index: -1
char_flash/body
  rotate: true
  xy: 601, 483
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_flash/hat
  rotate: true
  xy: 571, 227
  size: 107, 76
  orig: 107, 76
  offset: 0, 0
  index: -1
char_ironman/R_foot
  rotate: true
  xy: 558, 571
  size: 51, 24
  orig: 51, 24
  offset: 0, 0
  index: -1
char_ironman/backpack
  rotate: false
  xy: 745, 66
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
char_ironman/body
  rotate: true
  xy: 2, 565
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_red/R_foot
  rotate: true
  xy: 575, 730
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
char_red/backpack
  rotate: false
  xy: 499, 40
  size: 147, 175
  orig: 147, 175
  offset: 0, 0
  index: -1
char_red/body
  rotate: true
  xy: 280, 480
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_red/hair
  rotate: true
  xy: 382, 33
  size: 182, 115
  orig: 182, 115
  offset: 0, 0
  index: -1
char_spiderman/R_foot
  rotate: true
  xy: 558, 519
  size: 50, 24
  orig: 50, 24
  offset: 0, 0
  index: -1
char_spiderman/R_leg1
  rotate: true
  xy: 744, 231
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
char_spiderman/R_leg2
  rotate: true
  xy: 814, 287
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
char_spiderman/body
  rotate: true
  xy: 2, 387
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
char_spiderman/body_die
  rotate: true
  xy: 192, 8
  size: 201, 188
  orig: 201, 188
  offset: 0, 0
  index: -1
glass_2
  rotate: true
  xy: 896, 860
  size: 110, 72
  orig: 110, 72
  offset: 0, 0
  index: -1
s9_3
  rotate: false
  xy: 558, 501
  size: 24, 16
  orig: 24, 16
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 558, 389
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
vent
  rotate: false
  xy: 272, 217
  size: 248, 164
  orig: 248, 164
  offset: 0, 0
  index: -1
