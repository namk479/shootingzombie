
z_tank.png
size: 1300,1300
format: RGBA8888
filter: Linear,Linear
repeat: none
crack_1
  rotate: false
  xy: 2, 1124
  size: 828, 173
  orig: 828, 173
  offset: 0, 0
  index: -1
crack_2
  rotate: false
  xy: 2, 1006
  size: 828, 116
  orig: 828, 173
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 829, 9
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
z_tank/backpack_1
  rotate: true
  xy: 832, 1041
  size: 256, 325
  orig: 256, 325
  offset: 0, 0
  index: -1
z_tank/backpack_4
  rotate: false
  xy: 340, 62
  size: 217, 186
  orig: 217, 186
  offset: 0, 0
  index: -1
z_tank/body
  rotate: false
  xy: 2, 743
  size: 312, 261
  orig: 312, 261
  offset: 0, 0
  index: -1
z_tank/body_1
  rotate: false
  xy: 2, 480
  size: 312, 261
  orig: 312, 261
  offset: 0, 0
  index: -1
z_tank/body_2
  rotate: false
  xy: 316, 743
  size: 312, 261
  orig: 312, 261
  offset: 0, 0
  index: -1
z_tank/body_3
  rotate: false
  xy: 316, 480
  size: 311, 261
  orig: 311, 261
  offset: 0, 0
  index: -1
z_tank/body_4
  rotate: false
  xy: 2, 217
  size: 311, 261
  orig: 311, 261
  offset: 0, 0
  index: -1
z_tank/hat_1
  rotate: true
  xy: 1159, 1096
  size: 201, 129
  orig: 201, 129
  offset: 0, 0
  index: -1
z_tank/hat_3
  rotate: true
  xy: 829, 106
  size: 178, 142
  orig: 178, 142
  offset: 0, 0
  index: -1
z_tank/hat_4
  rotate: true
  xy: 630, 773
  size: 231, 215
  orig: 231, 215
  offset: 0, 0
  index: -1
z_tank/head
  rotate: false
  xy: 315, 250
  size: 223, 228
  orig: 223, 228
  offset: 0, 0
  index: -1
z_tank/l_arm1
  rotate: false
  xy: 540, 283
  size: 107, 195
  orig: 107, 195
  offset: 0, 0
  index: -1
z_tank/l_arm1_2
  rotate: true
  xy: 649, 286
  size: 106, 195
  orig: 106, 195
  offset: 0, 0
  index: -1
z_tank/l_arm1_3
  rotate: true
  xy: 931, 305
  size: 106, 195
  orig: 106, 195
  offset: 0, 0
  index: -1
z_tank/l_arm1_4
  rotate: true
  xy: 630, 665
  size: 106, 195
  orig: 106, 195
  offset: 0, 0
  index: -1
z_tank/l_arm2
  rotate: false
  xy: 1107, 206
  size: 103, 93
  orig: 103, 93
  offset: 0, 0
  index: -1
z_tank/l_arm2_2
  rotate: false
  xy: 1093, 17
  size: 103, 93
  orig: 103, 93
  offset: 0, 0
  index: -1
z_tank/l_arm2_3
  rotate: true
  xy: 1128, 301
  size: 110, 92
  orig: 110, 92
  offset: 0, 0
  index: -1
z_tank/l_arm2_4
  rotate: false
  xy: 1107, 112
  size: 103, 92
  orig: 103, 92
  offset: 0, 0
  index: -1
z_tank/l_foot
  rotate: true
  xy: 973, 78
  size: 88, 58
  orig: 88, 58
  offset: 0, 0
  index: -1
z_tank/l_foot_2
  rotate: true
  xy: 1033, 76
  size: 88, 58
  orig: 88, 58
  offset: 0, 0
  index: -1
z_tank/l_hand
  rotate: false
  xy: 846, 299
  size: 83, 93
  orig: 83, 93
  offset: 0, 0
  index: -1
z_tank/l_hand_1
  rotate: false
  xy: 1213, 453
  size: 83, 93
  orig: 83, 93
  offset: 0, 0
  index: -1
z_tank/l_hand_2
  rotate: false
  xy: 1212, 67
  size: 82, 93
  orig: 82, 93
  offset: 0, 0
  index: -1
z_tank/l_leg
  rotate: true
  xy: 139, 7
  size: 70, 135
  orig: 70, 135
  offset: 0, 0
  index: -1
z_tank/l_leg_1
  rotate: true
  xy: 829, 34
  size: 70, 135
  orig: 70, 135
  offset: 0, 0
  index: -1
z_tank/l_leg_2
  rotate: true
  xy: 2, 6
  size: 71, 135
  orig: 71, 135
  offset: 0, 0
  index: -1
z_tank/l_leg_fake
  rotate: false
  xy: 973, 168
  size: 70, 135
  orig: 70, 135
  offset: 0, 0
  index: -1
z_tank/r_arm
  rotate: false
  xy: 559, 2
  size: 133, 279
  orig: 133, 279
  offset: 0, 0
  index: -1
z_tank/r_arm_1
  rotate: true
  xy: 629, 529
  size: 134, 279
  orig: 134, 279
  offset: 0, 0
  index: -1
z_tank/r_arm_2
  rotate: false
  xy: 694, 5
  size: 133, 279
  orig: 133, 279
  offset: 0, 0
  index: -1
z_tank/r_arm_3
  rotate: true
  xy: 931, 413
  size: 133, 280
  orig: 133, 280
  offset: 0, 0
  index: -1
z_tank/r_arm_4
  rotate: true
  xy: 649, 394
  size: 133, 280
  orig: 133, 280
  offset: 0, 0
  index: -1
z_tank/r_leg
  rotate: false
  xy: 1045, 166
  size: 60, 137
  orig: 60, 137
  offset: 0, 0
  index: -1
z_tank/r_leg_1
  rotate: false
  xy: 1183, 957
  size: 60, 137
  orig: 60, 137
  offset: 0, 0
  index: -1
z_tank/r_leg_2
  rotate: false
  xy: 1212, 162
  size: 60, 137
  orig: 60, 137
  offset: 0, 0
  index: -1
z_tank/weapon
  rotate: false
  xy: 847, 785
  size: 366, 118
  orig: 366, 118
  offset: 0, 0
  index: -1
z_tank/weapon_1
  rotate: false
  xy: 847, 905
  size: 334, 134
  orig: 334, 134
  offset: 0, 0
  index: -1
z_tank/weapon_2
  rotate: false
  xy: 2, 79
  size: 336, 136
  orig: 336, 136
  offset: 0, 0
  index: -1
z_tank/weapon_3
  rotate: false
  xy: 910, 548
  size: 323, 118
  orig: 323, 118
  offset: 0, 0
  index: -1
z_tank/weapon_4
  rotate: false
  xy: 847, 668
  size: 362, 115
  orig: 362, 115
  offset: 0, 0
  index: -1
