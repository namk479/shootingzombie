
mud_zombie_1.png
size: 748,748
format: RGBA8888
filter: Linear,Linear
repeat: none
1/body
  rotate: false
  xy: 2, 176
  size: 564, 567
  orig: 564, 567
  offset: 0, 0
  index: -1
1/glass
  rotate: false
  xy: 2, 26
  size: 202, 148
  orig: 202, 148
  offset: 0, 0
  index: -1
1/mouth
  rotate: true
  xy: 449, 2
  size: 172, 144
  orig: 172, 144
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 206, 53
  size: 241, 121
  orig: 241, 121
  offset: 0, 0
  index: -1
