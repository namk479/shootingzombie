
z1.png
size: 844,844
format: RGBA8888
filter: Linear,Linear
repeat: none
R_foot_fake
  rotate: true
  xy: 579, 288
  size: 71, 47
  orig: 71, 47
  offset: 0, 0
  index: -1
R_leg1_fake
  rotate: true
  xy: 714, 480
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
R_leg2_fake
  rotate: true
  xy: 714, 411
  size: 67, 113
  orig: 67, 113
  offset: 0, 0
  index: -1
crack_1
  rotate: false
  xy: 2, 667
  size: 828, 173
  orig: 828, 173
  offset: 0, 0
  index: -1
crack_2
  rotate: false
  xy: 2, 549
  size: 828, 116
  orig: 828, 173
  offset: 0, 0
  index: -1
eye
  rotate: false
  xy: 467, 286
  size: 110, 72
  orig: 110, 72
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 240, 9
  size: 110, 23
  orig: 113, 25
  offset: 1, 0
  index: -1
z1/L_hand
  rotate: true
  xy: 358, 278
  size: 80, 107
  orig: 80, 107
  offset: 0, 0
  index: -1
z1/R_foot
  rotate: false
  xy: 187, 7
  size: 51, 25
  orig: 51, 25
  offset: 0, 0
  index: -1
z1/R_hand
  rotate: false
  xy: 714, 314
  size: 96, 95
  orig: 96, 95
  offset: 0, 0
  index: -1
z1/R_leg1
  rotate: true
  xy: 352, 13
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z1/R_leg1_4
  rotate: true
  xy: 447, 13
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z1/R_leg2
  rotate: true
  xy: 707, 265
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z1/R_leg2_4
  rotate: true
  xy: 707, 216
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
z1/backpack
  rotate: true
  xy: 542, 2
  size: 95, 163
  orig: 95, 163
  offset: 0, 0
  index: -1
z1/body
  rotate: false
  xy: 2, 271
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z1/body_4
  rotate: false
  xy: 180, 271
  size: 176, 276
  orig: 176, 276
  offset: 0, 0
  index: -1
z1/body_die
  rotate: false
  xy: 358, 360
  size: 176, 187
  orig: 176, 187
  offset: 0, 0
  index: -1
z1/body_die_4
  rotate: false
  xy: 536, 361
  size: 176, 186
  orig: 176, 186
  offset: 0, 0
  index: -1
z1/hat_1
  rotate: true
  xy: 2, 21
  size: 248, 183
  orig: 248, 183
  offset: 0, 0
  index: -1
z1/hat_2
  rotate: false
  xy: 518, 99
  size: 152, 185
  orig: 152, 185
  offset: 0, 0
  index: -1
z1/hat_3
  rotate: false
  xy: 312, 62
  size: 204, 207
  orig: 204, 207
  offset: 0, 0
  index: -1
z1/hat_4
  rotate: true
  xy: 187, 34
  size: 235, 123
  orig: 235, 123
  offset: 0, 0
  index: -1
