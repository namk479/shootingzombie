using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellSelectMode : MonoBehaviour
{
    public Material gray;
    public SkeletonGraphic btnIcon;
    public GameObject imgLock;
    public Mode mode;
    public void OnEnable()
    {
        SetData();
    }
    public void SetData()
    {
     //   this.mode = mode;
        if(User.Instance[ItemID.PlayingLevel] < 5 && mode.nameMode != NameMode.CampaignMode && mode.nameMode != NameMode.CollectionMode) 
        {
            imgLock.gameObject.SetActive(true);
        }
        /*else if(User.Instance[ItemID.PlayingLevel] > 5 )
        {
            imgLock.gameObject.SetActive(false);
        }*/
        if(mode.nameMode == NameMode.EndlessMode && User.Instance[ItemID.PlayingLevel] >=  5 || User.Instance[ItemID.AdsEndLessMode] == 1)
        {
                imgLock.gameObject.SetActive(false);
        }
        else if(mode.nameMode == NameMode.BossMode && User.Instance[ItemID.PlayingLevel] >=  5 || User.Instance[ItemID.AdsBossMode] == 1)
        {
                imgLock.gameObject.SetActive(false);
        }

    }
}
