using AA_Game;
using TMPro;
using UnityEngine;

public class FloatingHealth : Item
{
    public TextMeshPro healthAmount;
    public Animator anim;
    public Transform parent;

    private void Update()
    {
        if(parent != null)
        {
            this.transform.position = parent.transform.position;
        }
    }

    public void SetData(float damage,Transform pos,bool isCrit)
    {
        parent = pos;
        healthAmount.text = "-" + ((int)damage).ToString();
        anim.CrossFade("Fly", 0f);
        anim.Play("Fly", 0, 0f);
        if (isCrit)
        {
            healthAmount.color = Color.red;
            transform.localScale = new Vector3(2,2,2);
        }
        else
        {
            healthAmount.color = Color.white;
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
