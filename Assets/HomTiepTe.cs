using System.Collections.Generic;
using UnityEngine;
using Yurowm.GameCore;

public class HomTiepTe : MonoBehaviour
{
    private int amountToUnlock = 3;
    public List<TiepTe> tiepTes = new List<TiepTe>();

    private void OnEnable()
    {
        transform.localScale = Vector3.zero;
    }

    public void TiepTeOut()
    {
        transform.localScale = new Vector3(2, 2, 2);
        GetComponent<Animator>().CrossFade("TiepTe", 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (collision.gameObject.GetComponent<BulletBase>().isBulletOfPlayer)
            {
                amountToUnlock -= 1;
                FxItem hitFx = ContentPoolable.Emit(ItemID.enemy_hit_1) as FxItem;
                hitFx.transform.position = collision.gameObject.transform.position;
                if (amountToUnlock <= 0)
                {
                    // mo hop qua
                    TiepTe tiepTe = tiepTes.GetRandom();
                    GameManager.Instance.trainManager.playerGun.tiepTe = tiepTe;
                    GameManager.Instance.trainManager.NhanTiepTe(tiepTe);
                    GameScene.main.popupPlaying.tiepTeUI.Show(GameManager.Instance.trainManager.playerGun.tiepTe);
                    Destroy(collision.gameObject);
                    gameObject.SetActive(false);
                    FxItem collect = ContentPoolable.Emit(ItemID.fxCollectTiepTe) as FxItem;
                    collect.transform.position = this.transform.position;
                    FXCollectTiepTe collectFollow = ContentPoolable.Emit(ItemID.fxCollectTiepTeFollow) as FXCollectTiepTe;
                    collectFollow.transform.position = this.transform.position;
                    collectFollow.transform.localScale = new Vector3(3, 3, 3);
                }
            }
        }
    }
}

[System.Serializable]
public class TiepTe
{
    public MaTiepTe maTiepTe;
    public string nameTiepTe;
    public float timeActive;
}

public enum MaTiepTe
{
    None,
    Slowdown,
    Crit,
    AddPlayer,
}
